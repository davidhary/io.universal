# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [2.1.8053] - 2022-01-18
* Convert project to cs.

## [2.0.7095] - 2019-06-05
* Imported to Visual Studio 2019.
* Simplifies the assembly information.

## [1.2.4232] - 2011-08-03
Standardize code elements and documentation.

## [1.2.4213] - 2011-07-15
Simplifies the assembly information.

## [1.2.3412] - 2011-05-06
* Apps: Uses Foundation, Controls, and Windows Forms libraries.

## [1.2.2961] - 2008-02-09
Update to .NET 3.5.

## [1.1.2302] - 2006-04-21
Upgrade to VS 2005.

## [1.0.2219] - 2006-01-29
Remove VisualBasic import and replace related functions.

## [1.0.2206] - 2006-01-15
* New support and exceptions libraries. 
* Use Int32, Int64, and Int16 instead of Integer, Long, and Short.

## [1.0.2034] - 2005-07-27
* Demo: Uses Drawing library.

## [1.0.1937] - 2005-04-21
* Rename and restructure. Needs to fix reference to modified Core.

## [1.0.1615] - 2004-06-03
* Demo: Update installer to use UL merge module and add CBDIREC to handle the path business.

## [1.0.1592] - 2004-05-11
* Demo: Use tool bar.

## [1.0.1558] - 2004-04-07
* New dispose methods.

## [1.0.1496] - 2004-02-05
* Use Core VB Library
* Demo: simplify startup using Exception Display

## [1.0.1477] - 2004-01-17
* Demo: Apply Win Form upgrades.

## [1.0.1350] - 2003-09-12
Add Exceptions module and exception classes.

## [1.0.1345] - 2003-09-07
Recompile and test.

## [1.0.1315] - 2003-08-08
Add unit tests. Add method to adjust full range.

## [1.0.1314] - 2003-08-07
Initiate error handling. Add single bit and byte
digital I/O.

## [1.0.1313] - 2003-08-06
Rename from ULdaqOne and enforce guidlines

## [1.0.1145] - 2003-05-30
New library

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
[8053] - 2022-01-18 - merged branch vb2cs onto main
```
[2.1.8053]: https://www.bitbucket.org/davidhary/io.universal
