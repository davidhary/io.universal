# Universal Libraries

Wrapper for Measurement Computing Universal Library

## Getting Started

See the unit test library for code examples.

## Built, Tested and Facilitated By

* [Universal Library](https://www.mccdaq.com/productsearch.aspx?q=universal%20library) - Universal Library
* [Visual Studio](https://www.visualstudIO.com/) - Visual Studio
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - Wix Toolset
* [Fish Code Library](https://www.fishcodelib.com/) - Database tools
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker

## Authors

* **David Hary** - *Initial Workarounds* - [ATE Coder](https://www.IntegratedScientificResources.com)

## License

This repository is licensed under the [MIT License](https://www.bitbucket.org/davidhary/vs.universal/src/master/LICENSE.md)

## Acknowledgments

* Hat tip to all my mentors
* [Its all a remix](www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [John Simmons](https://www.codeproject.com/script/Membership/View.aspx?mid=7741) - outlaw programmer
* [Stack overflow](https://www.stackoveflow.com)

## Revision Changes

* Version 0.1.xxxx
	off we go
	

	