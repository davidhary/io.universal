Partial Friend Class SingleIo

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public tipsToolTip As System.Windows.Forms.ToolTip
    Public WithEvents ledCheckBox As System.Windows.Forms.CheckBox
    Public WithEvents channelCheckBoxD As System.Windows.Forms.ComboBox
    Public WithEvents channelCheckBoxC As System.Windows.Forms.ComboBox
    Public WithEvents channelCheckBoxB As System.Windows.Forms.ComboBox
    Public WithEvents channelCheckBoxA As System.Windows.Forms.ComboBox
    Public WithEvents analogInputErrorTextBox As System.Windows.Forms.TextBox
    Public WithEvents voltsTextBoxA As System.Windows.Forms.TextBox
    Public WithEvents voltsTextBoxB As System.Windows.Forms.TextBox
    Public WithEvents voltsTextBoxC As System.Windows.Forms.TextBox
    Public WithEvents voltsTextBoxD As System.Windows.Forms.TextBox
    Public WithEvents setCheckBox2 As System.Windows.Forms.CheckBox
    Public WithEvents setCheckBox3 As System.Windows.Forms.CheckBox
    Public WithEvents triggerCheckBox2 As System.Windows.Forms.CheckBox
    Public WithEvents triggerCheckBox3 As System.Windows.Forms.CheckBox
    Public WithEvents readCheckBox2 As System.Windows.Forms.CheckBox
    Public WithEvents readCheckBox3 As System.Windows.Forms.CheckBox
    Public WithEvents resetCountCheckBox As System.Windows.Forms.CheckBox
    Public WithEvents countTextBox As System.Windows.Forms.TextBox
    Public WithEvents analogOutOneTextBox As System.Windows.Forms.TextBox
    Public WithEvents analogOutErrorTextBox As System.Windows.Forms.TextBox
    Public WithEvents actionTimer As System.Windows.Forms.Timer
    Public WithEvents analogOutZeroTextBox As System.Windows.Forms.TextBox
    Public WithEvents channelLabel As System.Windows.Forms.Label
    Public WithEvents analogInputErrorLabel As System.Windows.Forms.Label
    Public WithEvents analogInputLabelA As System.Windows.Forms.Label
    Public WithEvents analogOutputOneLabel As System.Windows.Forms.Label
    Public WithEvents analogOutputErrorLabel As System.Windows.Forms.Label
    Public WithEvents analogOutputZeroLabel As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    Friend WithEvents openDeviceCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents counterGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents analogOutputGroupBox As System.Windows.Forms.GroupBox
    Public WithEvents countCaptionLabel As System.Windows.Forms.Label
    Friend WithEvents digitalIoGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents analogInputGroupBox As System.Windows.Forms.GroupBox
    Public WithEvents messagesTextBox As System.Windows.Forms.TextBox
    Public WithEvents rangeCheckBoxD As System.Windows.Forms.ComboBox
    Public WithEvents rangeCheckBoxC As System.Windows.Forms.ComboBox
    Public WithEvents rangeCheckBoxB As System.Windows.Forms.ComboBox
    Public WithEvents rangeCheckBoxA As System.Windows.Forms.ComboBox
    Public WithEvents rangeLabel As System.Windows.Forms.Label
    Friend WithEvents portZeroNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents portOneNumericUpDown As System.Windows.Forms.NumericUpDown
    Public WithEvents portOneReadCheckBox As System.Windows.Forms.CheckBox
    Public WithEvents portZeroReadCheckBox As System.Windows.Forms.CheckBox
    Public WithEvents portOneInputCheckBox As System.Windows.Forms.CheckBox
    Public WithEvents portZeroInputCheckBox As System.Windows.Forms.CheckBox
    Public WithEvents portOneSetCheckBox As System.Windows.Forms.CheckBox
    Public WithEvents portZeroSetCheckBox As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.tipsToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.ledCheckBox = New System.Windows.Forms.CheckBox
        Me.rangeCheckBoxD = New System.Windows.Forms.ComboBox
        Me.rangeCheckBoxC = New System.Windows.Forms.ComboBox
        Me.rangeCheckBoxB = New System.Windows.Forms.ComboBox
        Me.rangeCheckBoxA = New System.Windows.Forms.ComboBox
        Me.channelCheckBoxD = New System.Windows.Forms.ComboBox
        Me.channelCheckBoxC = New System.Windows.Forms.ComboBox
        Me.channelCheckBoxB = New System.Windows.Forms.ComboBox
        Me.channelCheckBoxA = New System.Windows.Forms.ComboBox
        Me.analogInputErrorTextBox = New System.Windows.Forms.TextBox
        Me.voltsTextBoxA = New System.Windows.Forms.TextBox
        Me.voltsTextBoxB = New System.Windows.Forms.TextBox
        Me.voltsTextBoxC = New System.Windows.Forms.TextBox
        Me.voltsTextBoxD = New System.Windows.Forms.TextBox
        Me.setCheckBox2 = New System.Windows.Forms.CheckBox
        Me.setCheckBox3 = New System.Windows.Forms.CheckBox
        Me.triggerCheckBox2 = New System.Windows.Forms.CheckBox
        Me.triggerCheckBox3 = New System.Windows.Forms.CheckBox
        Me.readCheckBox2 = New System.Windows.Forms.CheckBox
        Me.readCheckBox3 = New System.Windows.Forms.CheckBox
        Me.portOneReadCheckBox = New System.Windows.Forms.CheckBox
        Me.portZeroReadCheckBox = New System.Windows.Forms.CheckBox
        Me.portOneInputCheckBox = New System.Windows.Forms.CheckBox
        Me.portZeroInputCheckBox = New System.Windows.Forms.CheckBox
        Me.resetCountCheckBox = New System.Windows.Forms.CheckBox
        Me.portOneSetCheckBox = New System.Windows.Forms.CheckBox
        Me.portZeroSetCheckBox = New System.Windows.Forms.CheckBox
        Me.countTextBox = New System.Windows.Forms.TextBox
        Me.analogOutOneTextBox = New System.Windows.Forms.TextBox
        Me.analogOutErrorTextBox = New System.Windows.Forms.TextBox
        Me.actionTimer = New System.Windows.Forms.Timer(Me.components)
        Me.analogOutZeroTextBox = New System.Windows.Forms.TextBox
        Me.rangeLabel = New System.Windows.Forms.Label
        Me.channelLabel = New System.Windows.Forms.Label
        Me.analogInputErrorLabel = New System.Windows.Forms.Label
        Me.analogInputLabelA = New System.Windows.Forms.Label
        Me.countCaptionLabel = New System.Windows.Forms.Label
        Me.analogOutputOneLabel = New System.Windows.Forms.Label
        Me.analogOutputErrorLabel = New System.Windows.Forms.Label
        Me.analogOutputZeroLabel = New System.Windows.Forms.Label
        Me.openDeviceCheckBox = New System.Windows.Forms.CheckBox
        Me.counterGroupBox = New System.Windows.Forms.GroupBox
        Me.analogOutputGroupBox = New System.Windows.Forms.GroupBox
        Me.digitalIoGroupBox = New System.Windows.Forms.GroupBox
        Me.portOneNumericUpDown = New System.Windows.Forms.NumericUpDown
        Me.portZeroNumericUpDown = New System.Windows.Forms.NumericUpDown
        Me.analogInputGroupBox = New System.Windows.Forms.GroupBox
        Me.messagesTextBox = New System.Windows.Forms.TextBox
        Me.counterGroupBox.SuspendLayout()
        Me.analogOutputGroupBox.SuspendLayout()
        Me.digitalIoGroupBox.SuspendLayout()
        CType(Me.portOneNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.portZeroNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.analogInputGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'ledCheckBox
        '
        Me.ledCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me.ledCheckBox.Checked = True
        Me.ledCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ledCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.ledCheckBox.Enabled = False
        Me.ledCheckBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ledCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ledCheckBox.Location = New System.Drawing.Point(320, 24)
        Me.ledCheckBox.Name = "ledCheckBox"
        Me.ledCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ledCheckBox.Size = New System.Drawing.Size(97, 14)
        Me.ledCheckBox.TabIndex = 41
        Me.ledCheckBox.Text = "Status LED"
        '
        'rangeCheckBoxD
        '
        Me.rangeCheckBoxD.BackColor = System.Drawing.SystemColors.Window
        Me.rangeCheckBoxD.Cursor = System.Windows.Forms.Cursors.Default
        Me.rangeCheckBoxD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.rangeCheckBoxD.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rangeCheckBoxD.ForeColor = System.Drawing.SystemColors.WindowText
        Me.rangeCheckBoxD.Items.AddRange(New Object() {"1", "2", "4", "5", "8", "10", "16", "20"})
        Me.rangeCheckBoxD.Location = New System.Drawing.Point(90, 121)
        Me.rangeCheckBoxD.Name = "rangeCheckBoxD"
        Me.rangeCheckBoxD.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.rangeCheckBoxD.Size = New System.Drawing.Size(55, 22)
        Me.rangeCheckBoxD.TabIndex = 40
        '
        'rangeCheckBoxC
        '
        Me.rangeCheckBoxC.BackColor = System.Drawing.SystemColors.Window
        Me.rangeCheckBoxC.Cursor = System.Windows.Forms.Cursors.Default
        Me.rangeCheckBoxC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.rangeCheckBoxC.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rangeCheckBoxC.ForeColor = System.Drawing.SystemColors.WindowText
        Me.rangeCheckBoxC.Items.AddRange(New Object() {"1", "2", "4", "5", "8", "10", "16", "20"})
        Me.rangeCheckBoxC.Location = New System.Drawing.Point(90, 92)
        Me.rangeCheckBoxC.Name = "rangeCheckBoxC"
        Me.rangeCheckBoxC.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.rangeCheckBoxC.Size = New System.Drawing.Size(55, 22)
        Me.rangeCheckBoxC.TabIndex = 39
        '
        'rangeCheckBoxB
        '
        Me.rangeCheckBoxB.BackColor = System.Drawing.SystemColors.Window
        Me.rangeCheckBoxB.Cursor = System.Windows.Forms.Cursors.Default
        Me.rangeCheckBoxB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.rangeCheckBoxB.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rangeCheckBoxB.ForeColor = System.Drawing.SystemColors.WindowText
        Me.rangeCheckBoxB.Items.AddRange(New Object() {"1", "2", "4", "5", "8", "10", "16", "20"})
        Me.rangeCheckBoxB.Location = New System.Drawing.Point(90, 63)
        Me.rangeCheckBoxB.Name = "rangeCheckBoxB"
        Me.rangeCheckBoxB.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.rangeCheckBoxB.Size = New System.Drawing.Size(55, 22)
        Me.rangeCheckBoxB.TabIndex = 38
        '
        'rangeCheckBoxA
        '
        Me.rangeCheckBoxA.BackColor = System.Drawing.SystemColors.Window
        Me.rangeCheckBoxA.Cursor = System.Windows.Forms.Cursors.Default
        Me.rangeCheckBoxA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.rangeCheckBoxA.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rangeCheckBoxA.ForeColor = System.Drawing.SystemColors.WindowText
        Me.rangeCheckBoxA.Items.AddRange(New Object() {"±20.00V", "±10.0.V", "  ±5.00V", "  ±4.00V", "  ±2.50V", "  ±2.00V", "  ±1.25V", "  ±1.00V"})
        Me.rangeCheckBoxA.Location = New System.Drawing.Point(90, 34)
        Me.rangeCheckBoxA.Name = "rangeCheckBoxA"
        Me.rangeCheckBoxA.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.rangeCheckBoxA.Size = New System.Drawing.Size(55, 22)
        Me.rangeCheckBoxA.TabIndex = 35
        '
        'channelCheckBoxD
        '
        Me.channelCheckBoxD.BackColor = System.Drawing.SystemColors.Window
        Me.channelCheckBoxD.Cursor = System.Windows.Forms.Cursors.Default
        Me.channelCheckBoxD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.channelCheckBoxD.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.channelCheckBoxD.ForeColor = System.Drawing.SystemColors.WindowText
        Me.channelCheckBoxD.Items.AddRange(New Object() {"0", "1", "2", "3"})
        Me.channelCheckBoxD.Location = New System.Drawing.Point(16, 121)
        Me.channelCheckBoxD.Name = "channelCheckBoxD"
        Me.channelCheckBoxD.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.channelCheckBoxD.Size = New System.Drawing.Size(58, 22)
        Me.channelCheckBoxD.TabIndex = 34
        '
        'channelCheckBoxC
        '
        Me.channelCheckBoxC.BackColor = System.Drawing.SystemColors.Window
        Me.channelCheckBoxC.Cursor = System.Windows.Forms.Cursors.Default
        Me.channelCheckBoxC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.channelCheckBoxC.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.channelCheckBoxC.ForeColor = System.Drawing.SystemColors.WindowText
        Me.channelCheckBoxC.Items.AddRange(New Object() {"0", "1", "2", "3"})
        Me.channelCheckBoxC.Location = New System.Drawing.Point(16, 92)
        Me.channelCheckBoxC.Name = "channelCheckBoxC"
        Me.channelCheckBoxC.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.channelCheckBoxC.Size = New System.Drawing.Size(58, 22)
        Me.channelCheckBoxC.TabIndex = 33
        '
        'channelCheckBoxB
        '
        Me.channelCheckBoxB.BackColor = System.Drawing.SystemColors.Window
        Me.channelCheckBoxB.Cursor = System.Windows.Forms.Cursors.Default
        Me.channelCheckBoxB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.channelCheckBoxB.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.channelCheckBoxB.ForeColor = System.Drawing.SystemColors.WindowText
        Me.channelCheckBoxB.Items.AddRange(New Object() {"0", "1", "2", "3"})
        Me.channelCheckBoxB.Location = New System.Drawing.Point(16, 63)
        Me.channelCheckBoxB.Name = "channelCheckBoxB"
        Me.channelCheckBoxB.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.channelCheckBoxB.Size = New System.Drawing.Size(58, 22)
        Me.channelCheckBoxB.TabIndex = 32
        '
        'channelCheckBoxA
        '
        Me.channelCheckBoxA.BackColor = System.Drawing.SystemColors.Window
        Me.channelCheckBoxA.Cursor = System.Windows.Forms.Cursors.Default
        Me.channelCheckBoxA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.channelCheckBoxA.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.channelCheckBoxA.ForeColor = System.Drawing.SystemColors.WindowText
        Me.channelCheckBoxA.Items.AddRange(New Object() {"0", "1", "2", "3"})
        Me.channelCheckBoxA.Location = New System.Drawing.Point(16, 34)
        Me.channelCheckBoxA.Name = "channelCheckBoxA"
        Me.channelCheckBoxA.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.channelCheckBoxA.Size = New System.Drawing.Size(58, 22)
        Me.channelCheckBoxA.TabIndex = 31
        '
        'AnalogInputErrorTextBox
        '
        Me.analogInputErrorTextBox.AcceptsReturn = True
        Me.analogInputErrorTextBox.AutoSize = False
        Me.analogInputErrorTextBox.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.analogInputErrorTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.analogInputErrorTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.analogInputErrorTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.analogInputErrorTextBox.Location = New System.Drawing.Point(286, 239)
        Me.analogInputErrorTextBox.MaxLength = 0
        Me.analogInputErrorTextBox.Name = "AnalogInputErrorTextBox"
        Me.analogInputErrorTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.analogInputErrorTextBox.Size = New System.Drawing.Size(273, 33)
        Me.analogInputErrorTextBox.TabIndex = 29
        Me.analogInputErrorTextBox.Text = String.Empty
        '
        'VoltsTextBoxA
        '
        Me.voltsTextBoxA.AcceptsReturn = True
        Me.voltsTextBoxA.AutoSize = False
        Me.voltsTextBoxA.BackColor = System.Drawing.SystemColors.Window
        Me.voltsTextBoxA.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.voltsTextBoxA.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.voltsTextBoxA.ForeColor = System.Drawing.SystemColors.WindowText
        Me.voltsTextBoxA.Location = New System.Drawing.Point(160, 36)
        Me.voltsTextBoxA.MaxLength = 0
        Me.voltsTextBoxA.Name = "VoltsTextBoxA"
        Me.voltsTextBoxA.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.voltsTextBoxA.Size = New System.Drawing.Size(65, 19)
        Me.voltsTextBoxA.TabIndex = 24
        Me.voltsTextBoxA.Text = String.Empty
        '
        'VoltsTextBoxB
        '
        Me.voltsTextBoxB.AcceptsReturn = True
        Me.voltsTextBoxB.AutoSize = False
        Me.voltsTextBoxB.BackColor = System.Drawing.SystemColors.Window
        Me.voltsTextBoxB.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.voltsTextBoxB.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.voltsTextBoxB.ForeColor = System.Drawing.SystemColors.WindowText
        Me.voltsTextBoxB.Location = New System.Drawing.Point(160, 65)
        Me.voltsTextBoxB.MaxLength = 0
        Me.voltsTextBoxB.Name = "VoltsTextBoxB"
        Me.voltsTextBoxB.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.voltsTextBoxB.Size = New System.Drawing.Size(65, 19)
        Me.voltsTextBoxB.TabIndex = 23
        Me.voltsTextBoxB.Text = String.Empty
        '
        'VoltsTextBoxC
        '
        Me.voltsTextBoxC.AcceptsReturn = True
        Me.voltsTextBoxC.AutoSize = False
        Me.voltsTextBoxC.BackColor = System.Drawing.SystemColors.Window
        Me.voltsTextBoxC.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.voltsTextBoxC.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.voltsTextBoxC.ForeColor = System.Drawing.SystemColors.WindowText
        Me.voltsTextBoxC.Location = New System.Drawing.Point(160, 94)
        Me.voltsTextBoxC.MaxLength = 0
        Me.voltsTextBoxC.Name = "VoltsTextBoxC"
        Me.voltsTextBoxC.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.voltsTextBoxC.Size = New System.Drawing.Size(65, 19)
        Me.voltsTextBoxC.TabIndex = 22
        Me.voltsTextBoxC.Text = String.Empty
        '
        'VoltsTextBoxD
        '
        Me.voltsTextBoxD.AcceptsReturn = True
        Me.voltsTextBoxD.AutoSize = False
        Me.voltsTextBoxD.BackColor = System.Drawing.SystemColors.Window
        Me.voltsTextBoxD.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.voltsTextBoxD.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.voltsTextBoxD.ForeColor = System.Drawing.SystemColors.WindowText
        Me.voltsTextBoxD.Location = New System.Drawing.Point(160, 123)
        Me.voltsTextBoxD.MaxLength = 0
        Me.voltsTextBoxD.Name = "VoltsTextBoxD"
        Me.voltsTextBoxD.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.voltsTextBoxD.Size = New System.Drawing.Size(65, 19)
        Me.voltsTextBoxD.TabIndex = 21
        Me.voltsTextBoxD.Text = String.Empty
        '
        'setCheckBox2
        '
        Me.setCheckBox2.BackColor = System.Drawing.SystemColors.Control
        Me.setCheckBox2.Cursor = System.Windows.Forms.Cursors.Default
        Me.setCheckBox2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.setCheckBox2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.setCheckBox2.Location = New System.Drawing.Point(142, 68)
        Me.setCheckBox2.Name = "setCheckBox2"
        Me.setCheckBox2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.setCheckBox2.Size = New System.Drawing.Size(40, 16)
        Me.setCheckBox2.TabIndex = 20
        Me.setCheckBox2.Text = "Set"
        '
        'setCheckBox3
        '
        Me.setCheckBox3.BackColor = System.Drawing.SystemColors.Control
        Me.setCheckBox3.Cursor = System.Windows.Forms.Cursors.Default
        Me.setCheckBox3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.setCheckBox3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.setCheckBox3.Location = New System.Drawing.Point(142, 92)
        Me.setCheckBox3.Name = "setCheckBox3"
        Me.setCheckBox3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.setCheckBox3.Size = New System.Drawing.Size(40, 16)
        Me.setCheckBox3.TabIndex = 19
        Me.setCheckBox3.Text = "Set"
        '
        'triggerCheckBox2
        '
        Me.triggerCheckBox2.BackColor = System.Drawing.SystemColors.Control
        Me.triggerCheckBox2.Cursor = System.Windows.Forms.Cursors.Default
        Me.triggerCheckBox2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.triggerCheckBox2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.triggerCheckBox2.Location = New System.Drawing.Point(17, 68)
        Me.triggerCheckBox2.Name = "triggerCheckBox2"
        Me.triggerCheckBox2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.triggerCheckBox2.Size = New System.Drawing.Size(121, 16)
        Me.triggerCheckBox2.TabIndex = 18
        Me.triggerCheckBox2.Text = "IO2 Output/(Input)"
        '
        'triggerCheckBox3
        '
        Me.triggerCheckBox3.BackColor = System.Drawing.SystemColors.Control
        Me.triggerCheckBox3.Cursor = System.Windows.Forms.Cursors.Default
        Me.triggerCheckBox3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.triggerCheckBox3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.triggerCheckBox3.Location = New System.Drawing.Point(17, 92)
        Me.triggerCheckBox3.Name = "triggerCheckBox3"
        Me.triggerCheckBox3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.triggerCheckBox3.Size = New System.Drawing.Size(121, 16)
        Me.triggerCheckBox3.TabIndex = 17
        Me.triggerCheckBox3.Text = "IO3 Output/(Input)"
        '
        'readCheckBox2
        '
        Me.readCheckBox2.BackColor = System.Drawing.SystemColors.Control
        Me.readCheckBox2.Cursor = System.Windows.Forms.Cursors.Default
        Me.readCheckBox2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.readCheckBox2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.readCheckBox2.Location = New System.Drawing.Point(199, 68)
        Me.readCheckBox2.Name = "readCheckBox2"
        Me.readCheckBox2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.readCheckBox2.Size = New System.Drawing.Size(50, 16)
        Me.readCheckBox2.TabIndex = 16
        Me.readCheckBox2.Text = "Read"
        '
        'readCheckBox3
        '
        Me.readCheckBox3.BackColor = System.Drawing.SystemColors.Control
        Me.readCheckBox3.Cursor = System.Windows.Forms.Cursors.Default
        Me.readCheckBox3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.readCheckBox3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.readCheckBox3.Location = New System.Drawing.Point(199, 92)
        Me.readCheckBox3.Name = "readCheckBox3"
        Me.readCheckBox3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.readCheckBox3.Size = New System.Drawing.Size(50, 16)
        Me.readCheckBox3.TabIndex = 15
        Me.readCheckBox3.Text = "Read"
        '
        'portOneReadCheckBox
        '
        Me.portOneReadCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me.portOneReadCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.portOneReadCheckBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.portOneReadCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me.portOneReadCheckBox.Location = New System.Drawing.Point(167, 44)
        Me.portOneReadCheckBox.Name = "portOneReadCheckBox"
        Me.portOneReadCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.portOneReadCheckBox.Size = New System.Drawing.Size(50, 16)
        Me.portOneReadCheckBox.TabIndex = 14
        Me.portOneReadCheckBox.Text = "Read"
        '
        'portZeroReadCheckBox
        '
        Me.portZeroReadCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me.portZeroReadCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.portZeroReadCheckBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.portZeroReadCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me.portZeroReadCheckBox.Location = New System.Drawing.Point(168, 20)
        Me.portZeroReadCheckBox.Name = "portZeroReadCheckBox"
        Me.portZeroReadCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.portZeroReadCheckBox.Size = New System.Drawing.Size(56, 16)
        Me.portZeroReadCheckBox.TabIndex = 13
        Me.portZeroReadCheckBox.Text = "Read"
        '
        'portOneInputCheckBox
        '
        Me.portOneInputCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me.portOneInputCheckBox.Checked = True
        Me.portOneInputCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.portOneInputCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.portOneInputCheckBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.portOneInputCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me.portOneInputCheckBox.Location = New System.Drawing.Point(17, 44)
        Me.portOneInputCheckBox.Name = "portOneInputCheckBox"
        Me.portOneInputCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.portOneInputCheckBox.Size = New System.Drawing.Size(82, 16)
        Me.portOneInputCheckBox.TabIndex = 12
        Me.portOneInputCheckBox.Text = "Port B Input"
        '
        'portZeroInputCheckBox
        '
        Me.portZeroInputCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me.portZeroInputCheckBox.Checked = True
        Me.portZeroInputCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.portZeroInputCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.portZeroInputCheckBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.portZeroInputCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me.portZeroInputCheckBox.Location = New System.Drawing.Point(17, 20)
        Me.portZeroInputCheckBox.Name = "portZeroInputCheckBox"
        Me.portZeroInputCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.portZeroInputCheckBox.Size = New System.Drawing.Size(82, 16)
        Me.portZeroInputCheckBox.TabIndex = 11
        Me.portZeroInputCheckBox.Text = "Port A Input"
        '
        'resetCountCheckBox
        '
        Me.resetCountCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me.resetCountCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.resetCountCheckBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.resetCountCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me.resetCountCheckBox.Location = New System.Drawing.Point(16, 47)
        Me.resetCountCheckBox.Name = "resetCountCheckBox"
        Me.resetCountCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.resetCountCheckBox.Size = New System.Drawing.Size(105, 15)
        Me.resetCountCheckBox.TabIndex = 10
        Me.resetCountCheckBox.Text = "Reset Counter"
        '
        'portOneSetCheckBox
        '
        Me.portOneSetCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me.portOneSetCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.portOneSetCheckBox.Enabled = False
        Me.portOneSetCheckBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.portOneSetCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me.portOneSetCheckBox.Location = New System.Drawing.Point(110, 44)
        Me.portOneSetCheckBox.Name = "portOneSetCheckBox"
        Me.portOneSetCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.portOneSetCheckBox.Size = New System.Drawing.Size(40, 16)
        Me.portOneSetCheckBox.TabIndex = 9
        Me.portOneSetCheckBox.Text = "Set"
        '
        'portZeroSetCheckBox
        '
        Me.portZeroSetCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me.portZeroSetCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.portZeroSetCheckBox.Enabled = False
        Me.portZeroSetCheckBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.portZeroSetCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me.portZeroSetCheckBox.Location = New System.Drawing.Point(110, 20)
        Me.portZeroSetCheckBox.Name = "portZeroSetCheckBox"
        Me.portZeroSetCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.portZeroSetCheckBox.Size = New System.Drawing.Size(40, 16)
        Me.portZeroSetCheckBox.TabIndex = 8
        Me.portZeroSetCheckBox.Text = "Set"
        '
        'countTextBox
        '
        Me.countTextBox.AcceptsReturn = True
        Me.countTextBox.AutoSize = False
        Me.countTextBox.BackColor = System.Drawing.SystemColors.Window
        Me.countTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.countTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.countTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.countTextBox.Location = New System.Drawing.Point(49, 18)
        Me.countTextBox.MaxLength = 0
        Me.countTextBox.Name = "countTextBox"
        Me.countTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.countTextBox.Size = New System.Drawing.Size(80, 19)
        Me.countTextBox.TabIndex = 6
        Me.countTextBox.Text = String.Empty
        '
        'analogOutOneTextBox
        '
        Me.analogOutOneTextBox.AcceptsReturn = True
        Me.analogOutOneTextBox.AutoSize = False
        Me.analogOutOneTextBox.BackColor = System.Drawing.SystemColors.Window
        Me.analogOutOneTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.analogOutOneTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.analogOutOneTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.analogOutOneTextBox.Location = New System.Drawing.Point(44, 46)
        Me.analogOutOneTextBox.MaxLength = 0
        Me.analogOutOneTextBox.Name = "analogOutOneTextBox"
        Me.analogOutOneTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.analogOutOneTextBox.Size = New System.Drawing.Size(65, 19)
        Me.analogOutOneTextBox.TabIndex = 4
        Me.analogOutOneTextBox.Text = "0.00"
        '
        'analogOutErrorTextBox
        '
        Me.analogOutErrorTextBox.AcceptsReturn = True
        Me.analogOutErrorTextBox.AutoSize = False
        Me.analogOutErrorTextBox.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.analogOutErrorTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.analogOutErrorTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.analogOutErrorTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.analogOutErrorTextBox.Location = New System.Drawing.Point(8, 239)
        Me.analogOutErrorTextBox.MaxLength = 0
        Me.analogOutErrorTextBox.Name = "analogOutErrorTextBox"
        Me.analogOutErrorTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.analogOutErrorTextBox.Size = New System.Drawing.Size(273, 33)
        Me.analogOutErrorTextBox.TabIndex = 1
        Me.analogOutErrorTextBox.Text = String.Empty
        '
        'actionTimer
        '
        Me.actionTimer.Interval = 200
        '
        'analogOutZeroTextBox
        '
        Me.analogOutZeroTextBox.AcceptsReturn = True
        Me.analogOutZeroTextBox.AutoSize = False
        Me.analogOutZeroTextBox.BackColor = System.Drawing.SystemColors.Window
        Me.analogOutZeroTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.analogOutZeroTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.analogOutZeroTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.analogOutZeroTextBox.Location = New System.Drawing.Point(44, 22)
        Me.analogOutZeroTextBox.MaxLength = 0
        Me.analogOutZeroTextBox.Name = "analogOutZeroTextBox"
        Me.analogOutZeroTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.analogOutZeroTextBox.Size = New System.Drawing.Size(65, 19)
        Me.analogOutZeroTextBox.TabIndex = 0
        Me.analogOutZeroTextBox.Text = "0.00"
        '
        'rangeLabel
        '
        Me.rangeLabel.BackColor = System.Drawing.SystemColors.Control
        Me.rangeLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.rangeLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rangeLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rangeLabel.Location = New System.Drawing.Point(90, 18)
        Me.rangeLabel.Name = "rangeLabel"
        Me.rangeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.rangeLabel.Size = New System.Drawing.Size(49, 16)
        Me.rangeLabel.TabIndex = 37
        Me.rangeLabel.Text = "Range"
        '
        'channelLabel
        '
        Me.channelLabel.BackColor = System.Drawing.SystemColors.Control
        Me.channelLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.channelLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.channelLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.channelLabel.Location = New System.Drawing.Point(16, 18)
        Me.channelLabel.Name = "channelLabel"
        Me.channelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.channelLabel.Size = New System.Drawing.Size(73, 16)
        Me.channelLabel.TabIndex = 36
        Me.channelLabel.Text = "Channel"
        '
        'analogInputErrorLabel
        '
        Me.analogInputErrorLabel.BackColor = System.Drawing.SystemColors.Control
        Me.analogInputErrorLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.analogInputErrorLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.analogInputErrorLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.analogInputErrorLabel.Location = New System.Drawing.Point(285, 223)
        Me.analogInputErrorLabel.Name = "analogInputErrorLabel"
        Me.analogInputErrorLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.analogInputErrorLabel.Size = New System.Drawing.Size(143, 16)
        Me.analogInputErrorLabel.TabIndex = 30
        Me.analogInputErrorLabel.Text = "Analog Input Error Message"
        '
        'analogInputLabelA
        '
        Me.analogInputLabelA.BackColor = System.Drawing.SystemColors.Control
        Me.analogInputLabelA.Cursor = System.Windows.Forms.Cursors.Default
        Me.analogInputLabelA.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.analogInputLabelA.ForeColor = System.Drawing.SystemColors.ControlText
        Me.analogInputLabelA.Location = New System.Drawing.Point(160, 18)
        Me.analogInputLabelA.Name = "analogInputLabelA"
        Me.analogInputLabelA.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.analogInputLabelA.Size = New System.Drawing.Size(81, 16)
        Me.analogInputLabelA.TabIndex = 28
        Me.analogInputLabelA.Text = "Voltage"
        '
        'countCaptionLabel
        '
        Me.countCaptionLabel.BackColor = System.Drawing.SystemColors.Control
        Me.countCaptionLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.countCaptionLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.countCaptionLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.countCaptionLabel.Location = New System.Drawing.Point(9, 19)
        Me.countCaptionLabel.Name = "countCaptionLabel"
        Me.countCaptionLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.countCaptionLabel.Size = New System.Drawing.Size(36, 16)
        Me.countCaptionLabel.TabIndex = 7
        Me.countCaptionLabel.Text = "Count: "
        Me.countCaptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'analogOutputOneLabel
        '
        Me.analogOutputOneLabel.BackColor = System.Drawing.SystemColors.Control
        Me.analogOutputOneLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.analogOutputOneLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.analogOutputOneLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.analogOutputOneLabel.Location = New System.Drawing.Point(20, 47)
        Me.analogOutputOneLabel.Name = "analogOutputOneLabel"
        Me.analogOutputOneLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.analogOutputOneLabel.Size = New System.Drawing.Size(19, 16)
        Me.analogOutputOneLabel.TabIndex = 5
        Me.analogOutputOneLabel.Text = "1: "
        Me.analogOutputOneLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'analogOutputErrorLabel
        '
        Me.analogOutputErrorLabel.BackColor = System.Drawing.SystemColors.Control
        Me.analogOutputErrorLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.analogOutputErrorLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.analogOutputErrorLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.analogOutputErrorLabel.Location = New System.Drawing.Point(8, 223)
        Me.analogOutputErrorLabel.Name = "analogOutputErrorLabel"
        Me.analogOutputErrorLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.analogOutputErrorLabel.Size = New System.Drawing.Size(130, 16)
        Me.analogOutputErrorLabel.TabIndex = 3
        Me.analogOutputErrorLabel.Text = "Analog Output Error Message"
        '
        'analogOutputZeroLabel
        '
        Me.analogOutputZeroLabel.BackColor = System.Drawing.SystemColors.Control
        Me.analogOutputZeroLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.analogOutputZeroLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.analogOutputZeroLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.analogOutputZeroLabel.Location = New System.Drawing.Point(20, 23)
        Me.analogOutputZeroLabel.Name = "analogOutputZeroLabel"
        Me.analogOutputZeroLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.analogOutputZeroLabel.Size = New System.Drawing.Size(19, 16)
        Me.analogOutputZeroLabel.TabIndex = 2
        Me.analogOutputZeroLabel.Text = "0: "
        Me.analogOutputZeroLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'openDeviceCheckBox
        '
        Me.openDeviceCheckBox.Appearance = System.Windows.Forms.Appearance.Button
        Me.openDeviceCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.openDeviceCheckBox.Location = New System.Drawing.Point(435, 12)
        Me.openDeviceCheckBox.Name = "openDeviceCheckBox"
        Me.openDeviceCheckBox.Size = New System.Drawing.Size(120, 32)
        Me.openDeviceCheckBox.TabIndex = 42
        Me.openDeviceCheckBox.Text = "&Open Device"
        Me.openDeviceCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'counterGroupBox
        '
        Me.counterGroupBox.Controls.Add(Me.countTextBox)
        Me.counterGroupBox.Controls.Add(Me.countCaptionLabel)
        Me.counterGroupBox.Controls.Add(Me.resetCountCheckBox)
        Me.counterGroupBox.Enabled = False
        Me.counterGroupBox.Location = New System.Drawing.Point(160, 9)
        Me.counterGroupBox.Name = "counterGroupBox"
        Me.counterGroupBox.Size = New System.Drawing.Size(144, 70)
        Me.counterGroupBox.TabIndex = 43
        Me.counterGroupBox.TabStop = False
        Me.counterGroupBox.Text = "Counter"
        '
        'analogOutputGroupBox
        '
        Me.analogOutputGroupBox.Controls.Add(Me.analogOutOneTextBox)
        Me.analogOutputGroupBox.Controls.Add(Me.analogOutputZeroLabel)
        Me.analogOutputGroupBox.Controls.Add(Me.analogOutputOneLabel)
        Me.analogOutputGroupBox.Controls.Add(Me.analogOutZeroTextBox)
        Me.analogOutputGroupBox.Enabled = False
        Me.analogOutputGroupBox.Location = New System.Drawing.Point(8, 8)
        Me.analogOutputGroupBox.Name = "analogOutputGroupBox"
        Me.analogOutputGroupBox.Size = New System.Drawing.Size(144, 72)
        Me.analogOutputGroupBox.TabIndex = 44
        Me.analogOutputGroupBox.TabStop = False
        Me.analogOutputGroupBox.Text = "Analog Output Voltages"
        '
        'digitalIoGroupBox
        '
        Me.digitalIoGroupBox.Controls.Add(Me.portOneNumericUpDown)
        Me.digitalIoGroupBox.Controls.Add(Me.portZeroNumericUpDown)
        Me.digitalIoGroupBox.Controls.Add(Me.portZeroInputCheckBox)
        Me.digitalIoGroupBox.Controls.Add(Me.triggerCheckBox2)
        Me.digitalIoGroupBox.Controls.Add(Me.triggerCheckBox3)
        Me.digitalIoGroupBox.Controls.Add(Me.portOneInputCheckBox)
        Me.digitalIoGroupBox.Controls.Add(Me.portOneSetCheckBox)
        Me.digitalIoGroupBox.Controls.Add(Me.portZeroSetCheckBox)
        Me.digitalIoGroupBox.Controls.Add(Me.setCheckBox2)
        Me.digitalIoGroupBox.Controls.Add(Me.setCheckBox3)
        Me.digitalIoGroupBox.Controls.Add(Me.readCheckBox2)
        Me.digitalIoGroupBox.Controls.Add(Me.readCheckBox3)
        Me.digitalIoGroupBox.Controls.Add(Me.portOneReadCheckBox)
        Me.digitalIoGroupBox.Controls.Add(Me.portZeroReadCheckBox)
        Me.digitalIoGroupBox.Enabled = False
        Me.digitalIoGroupBox.Location = New System.Drawing.Point(8, 96)
        Me.digitalIoGroupBox.Name = "digitalIoGroupBox"
        Me.digitalIoGroupBox.Size = New System.Drawing.Size(296, 120)
        Me.digitalIoGroupBox.TabIndex = 45
        Me.digitalIoGroupBox.TabStop = False
        Me.digitalIoGroupBox.Text = "Digital I/O"
        '
        'portOneNumericUpDown
        '
        Me.portOneNumericUpDown.Hexadecimal = True
        Me.portOneNumericUpDown.Location = New System.Drawing.Point(240, 42)
        Me.portOneNumericUpDown.Maximum = New Decimal(New Int32() {255, 0, 0, 0})
        Me.portOneNumericUpDown.Name = "portOneNumericUpDown"
        Me.portOneNumericUpDown.Size = New System.Drawing.Size(48, 20)
        Me.portOneNumericUpDown.TabIndex = 22
        '
        'portZeroNumericUpDown
        '
        Me.portZeroNumericUpDown.Hexadecimal = True
        Me.portZeroNumericUpDown.Location = New System.Drawing.Point(240, 18)
        Me.portZeroNumericUpDown.Maximum = New Decimal(New Int32() {255, 0, 0, 0})
        Me.portZeroNumericUpDown.Name = "portZeroNumericUpDown"
        Me.portZeroNumericUpDown.Size = New System.Drawing.Size(48, 20)
        Me.portZeroNumericUpDown.TabIndex = 21
        '
        'analogInputGroupBox
        '
        Me.analogInputGroupBox.Controls.Add(Me.voltsTextBoxD)
        Me.analogInputGroupBox.Controls.Add(Me.rangeCheckBoxC)
        Me.analogInputGroupBox.Controls.Add(Me.analogInputLabelA)
        Me.analogInputGroupBox.Controls.Add(Me.rangeLabel)
        Me.analogInputGroupBox.Controls.Add(Me.channelLabel)
        Me.analogInputGroupBox.Controls.Add(Me.rangeCheckBoxD)
        Me.analogInputGroupBox.Controls.Add(Me.rangeCheckBoxB)
        Me.analogInputGroupBox.Controls.Add(Me.rangeCheckBoxA)
        Me.analogInputGroupBox.Controls.Add(Me.channelCheckBoxD)
        Me.analogInputGroupBox.Controls.Add(Me.channelCheckBoxC)
        Me.analogInputGroupBox.Controls.Add(Me.channelCheckBoxB)
        Me.analogInputGroupBox.Controls.Add(Me.channelCheckBoxA)
        Me.analogInputGroupBox.Controls.Add(Me.voltsTextBoxA)
        Me.analogInputGroupBox.Controls.Add(Me.voltsTextBoxB)
        Me.analogInputGroupBox.Controls.Add(Me.voltsTextBoxC)
        Me.analogInputGroupBox.Enabled = False
        Me.analogInputGroupBox.Location = New System.Drawing.Point(311, 55)
        Me.analogInputGroupBox.Name = "analogInputGroupBox"
        Me.analogInputGroupBox.Size = New System.Drawing.Size(248, 160)
        Me.analogInputGroupBox.TabIndex = 46
        Me.analogInputGroupBox.TabStop = False
        Me.analogInputGroupBox.Text = "Analog Inputs"
        '
        'messagesTextBox
        '
        Me.messagesTextBox.AcceptsReturn = True
        Me.messagesTextBox.AutoSize = False
        Me.messagesTextBox.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.messagesTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.messagesTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.messagesTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.messagesTextBox.Location = New System.Drawing.Point(8, 280)
        Me.messagesTextBox.MaxLength = 0
        Me.messagesTextBox.Name = "messagesTextBox"
        Me.messagesTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.messagesTextBox.Size = New System.Drawing.Size(552, 33)
        Me.messagesTextBox.TabIndex = 47
        Me.messagesTextBox.Text = String.Empty
        '
        'SingleIo
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(568, 318)
        Me.Controls.Add(Me.messagesTextBox)
        Me.Controls.Add(Me.analogInputErrorTextBox)
        Me.Controls.Add(Me.analogOutErrorTextBox)
        Me.Controls.Add(Me.analogInputGroupBox)
        Me.Controls.Add(Me.digitalIoGroupBox)
        Me.Controls.Add(Me.analogOutputGroupBox)
        Me.Controls.Add(Me.counterGroupBox)
        Me.Controls.Add(Me.openDeviceCheckBox)
        Me.Controls.Add(Me.ledCheckBox)
        Me.Controls.Add(Me.analogInputErrorLabel)
        Me.Controls.Add(Me.analogOutputErrorLabel)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Location = New System.Drawing.Point(218, 167)
        Me.Name = "SingleIo"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "PMD-1208LS Single I/O Testing Module"
        Me.counterGroupBox.ResumeLayout(False)
        Me.analogOutputGroupBox.ResumeLayout(False)
        Me.digitalIoGroupBox.ResumeLayout(False)
        CType(Me.portOneNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.portZeroNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.analogInputGroupBox.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

End Class
