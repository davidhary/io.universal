Imports System.Reflection

Imports Microsoft.VisualBasic.ApplicationServices
''' <summary>Includes code for form SingleIo, which provide test methods for single I/O.</summary>
''' <remarks>Use this module to test single I/O operations of the board.(c) 2002 Integrated Scientific Resources, Inc.
''' Licensed under The MIT License.<para>
''' David, 12/05/02, 1.0.1069. Created </para><para>
''' David, 05/30/03, 1.0.1245. Adapt to PMD-1208 </para></remarks>
Partial Friend Class SingleIo
    Inherits System.Windows.Forms.Form

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
    ''' </summary>
    ''' <remarks> David, 2022-01-17. </remarks>
    Public Sub New()
        MyBase.New()
        ' This method is required by the Windows Form Designer.
        Me.InitializeComponent()
    End Sub

    ' Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                components?.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)

        End Try
    End Sub

#End Region

#Region " METHODS "

    ''' <summary>Displays this module.</summary>
    ''' <returns>Returns dialog result.</returns>
    ''' <remarks>Use this method to display this form.</remarks>
    Friend Overloads Function ShowDialog() As System.Windows.Forms.DialogResult

        ' show the form
        Return MyBase.ShowDialog()

    End Function

    ''' <summary>Initializes the user interface and tool tips.</summary>
    Private Sub initializeUserInterface()
        tipsToolTip.SetToolTip(Me.openDeviceCheckBox, "Depress to open the device or release to close.")
    End Sub

    ''' <summary>opens access to SignalLink.</summary>
    ''' <remarks>Use this method to open the driver in real or demo modes</remarks>
    Private Sub [open]()

        ' the board number is a constant.  We should later get it from 
        ' a configuration file.
        Const boardNumber As Int32 = 1

        Try

            ' instantiate the class
            daqDevice = New isr.io.Universal.Device("Single I/O")

            ' Single I/O is only relevant for real mode
            daqDevice.IsDemo = False

            ' open the driver
            If Me.OpenDriver(boardNumber) Then

                Me.analogInputGroupBox.Enabled = True
                Me.analogOutputGroupBox.Enabled = True
                Me.counterGroupBox.Enabled = True
                Me.digitalIoGroupBox.Enabled = True
                Me.ledCheckBox.Enabled = True

                ' enable the timer
                actionTimer.Enabled = True

            Else
                ' if failed opening, disable user interface 
                Me.analogInputGroupBox.Enabled = False
                Me.analogOutputGroupBox.Enabled = False
                Me.counterGroupBox.Enabled = False
                Me.digitalIoGroupBox.Enabled = False
                Me.ledCheckBox.Enabled = False

                ' disable the timer
                actionTimer.Enabled = False
            End If

            ' enable controls
        Catch exn As System.ApplicationException

            ' add the message to the exception
            statusMessage = "failed instantiating the MCC device driver"
            Throw New ApplicationException(statusMessage, exn)

        End Try

    End Sub

    ''' <summary>opens the SignalLink driver.</summary>
    ''' <remarks>Use this method to open the driver</remarks>
    Private Function OpenDriver(ByVal boardNumber As Int32) As Boolean

        ' use in finally to determine if we should complete the open process.
        Dim failed As Boolean = False

        Try

            ' open the driver trapping hardware not found exception
            daqDevice.Open(boardNumber)

        Catch hardwareExn As isr.io.Universal.HardwareNotFoundException

            ' if failed opening try to open in demo mode
            If daqDevice.IsDemo Then

                ' add the message to the exception
                statusMessage = "Failed opening the data acquisition device."
                failed = True

            Else

                ' if not demo, set to demo mode.
                daqDevice.IsDemo = True

                Try

                    ' try to open the driver
                    daqDevice.Open(boardNumber)

                    ' add the message to message list
                    statusMessage = "Failed opening device for data acquisition.  Reverting to demo mode.  Check if the device is connected."
                    Me.messagesTextBox.Text = statusMessage

                Catch exn As System.ApplicationException

                    ' add the message to the exception
                    statusMessage = "Failed opening the data acquisition device"
                    failed = True

                End Try

            End If

        Finally

            If failed Then

                ' if failed, do nothing.  we shall abort this one.

            Else

                ' return the firmware version
                Me.messagesTextBox.Text = "Open.  Software revision: " & isr.io.Universal.Device.SoftwareRevision.ToString(Globalization.CultureInfo.CurrentCulture)

                ' instantiate two digital input channels
                portZero = New isr.io.Universal.DigitalPort("Port A", daqDevice)
                With portZero
                    .IsInput = True
                    .PortNumber = 0
                    .ConfigurePort()
                End With
                portOne = New isr.io.Universal.DigitalPort("Port B", daqDevice)
                With portOne
                    .IsInput = True
                    .PortNumber = 1
                    .ConfigurePort()
                End With

                ' instantiate four analog input channels
                inputZero = New isr.io.Universal.AnalogInput("Channel A", daqDevice)
                inputOne = New isr.io.Universal.AnalogInput("Channel B", daqDevice)
                inputTwo = New isr.io.Universal.AnalogInput("Channel C", daqDevice)
                inputThree = New isr.io.Universal.AnalogInput("Channel D", daqDevice)

            End If

        End Try

        Return Not failed

    End Function

    ''' <summary>Closes and releases the data acquisition device driver.</summary>
    ''' <remarks>Use this method to close and release the driver</remarks>
    Private Sub closeSignalIo()

        Try

            ' disable the timer
            actionTimer.Enabled = False
            System.Windows.Forms.Application.DoEvents()

            ' check if we have instantiated the class
            If daqDevice Is Nothing Then
                messagesTextBox.Text = "Warning.  Driver already closed."
            Else
                ' close the driver.
                daqDevice.Close()
            End If

        Catch exn As System.ApplicationException

            ' add the message to the exception
            Dim usrMessage As String
            usrMessage = "Failed closing the data acquisition device driver"
            Throw New ApplicationException(usrMessage, exn)

        Finally

            ' disable all group boxes
            Me.analogInputGroupBox.Enabled = False
            Me.analogOutputGroupBox.Enabled = False
            Me.counterGroupBox.Enabled = False
            Me.digitalIoGroupBox.Enabled = False
            Me.ledCheckBox.Enabled = False

        End Try

    End Sub

    ''' <summary>Initializes the class objects.</summary>
    ''' <remarks>Called from the form load method to instantiate 
    '''   module-level objects.</remarks>
    Private Sub instantiateObjects()

        ' instantiate the board
        daqDevice = New isr.io.Universal.Device("Single I/O")

        ' disable all group boxes
        Me.analogInputGroupBox.Enabled = False
        Me.analogOutputGroupBox.Enabled = False
        Me.counterGroupBox.Enabled = False
        Me.digitalIoGroupBox.Enabled = False
        Me.ledCheckBox.Enabled = False

        ' set the channel and gains
        With channelCheckBoxA
            .Items.Clear()
            .DataSource = isr.io.Universal.AnalogInput.Channels
            .SelectedIndex = 0
        End With
        With channelCheckBoxB
            .Items.Clear()
            .DataSource = isr.io.Universal.AnalogInput.Channels
            .SelectedIndex = 1
        End With
        With channelCheckBoxC
            .Items.Clear()
            .DataSource = isr.io.Universal.AnalogInput.Channels
            .SelectedIndex = 2
        End With
        With channelCheckBoxD
            .Items.Clear()
            .DataSource = isr.io.Universal.AnalogInput.Channels
            .SelectedIndex = 3
        End With

        With rangeCheckBoxA
            .Items.Clear()
            .DataSource = isr.io.Universal.AnalogInput.Ranges
            .SelectedIndex = 0
        End With
        With rangeCheckBoxB
            .Items.Clear()
            .DataSource = isr.io.Universal.AnalogInput.Ranges
            .SelectedIndex = 0
        End With
        With rangeCheckBoxC
            .Items.Clear()
            .DataSource = isr.io.Universal.AnalogInput.Ranges
            .SelectedIndex = 0
        End With
        With rangeCheckBoxD
            .Items.Clear()
            .DataSource = isr.io.Universal.AnalogInput.Ranges
            .SelectedIndex = 0
        End With

    End Sub

#End Region

#Region " PROPERTIES "

    ' module level SignalLink PMD-1208 board
    Private daqDevice As isr.io.Universal.Device

    ' module level digital ports
    Private portOne As isr.io.Universal.DigitalPort
    Private portZero As isr.io.Universal.DigitalPort

    ' module level analog input channel
    Private inputZero As isr.io.Universal.AnalogInput
    Private inputOne As isr.io.Universal.AnalogInput
    Private inputTwo As isr.io.Universal.AnalogInput
    Private inputThree As isr.io.Universal.AnalogInput
    Private statusMessage As String = String.Empty

    ''' <summary>Returns true if an instance of the class was created and not disposed.</summary>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        ' Returns true if an instance of the class was created and not disposed
        Get
            Return My.Application.OpenForms.Count > 0 AndAlso
          My.Application.OpenForms.Item(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name) IsNot Nothing
        End Get
    End Property

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs after the form is closed.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>This event is a notification that the form has already gone away before
    ''' control is returned to the calling method (in case of a modal form).  Use this
    ''' method to delete any temporary files that were created or dispose of any objects
    ''' not disposed with the closing event.
    ''' </remarks>
    Private Sub form_Closed(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles MyBase.Closed

    End Sub

    ''' <summary>Occurs before the form is closed</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
    ''' <remarks>Use this method to optionally cancel the closing of the form.
    ''' Because the form is not yet closed at this point, this is also the best 
    ''' place to serialize a form's visible properties, such as size and 
    ''' location. Finally, dispose of any form level objects especially those that
    ''' might needs access to the form and thus should not be terminated after the
    ''' form closed.
    ''' </remarks>
    Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) _
    Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        System.Windows.Forms.Application.DoEvents()

        ' set module objects that reference other objects to Nothing

        ' terminate form objects
        Me.terminateObjects()

    End Sub

    ''' <summary>Terminates and disposes of class-level objects.</summary>
    ''' <remarks>Called from the form Closing method.</remarks>
    Private Sub terminateObjects()

        ' disable all group boxes
        Me.analogInputGroupBox.Enabled = False
        Me.analogOutputGroupBox.Enabled = False
        Me.counterGroupBox.Enabled = False
        Me.digitalIoGroupBox.Enabled = False
        Me.ledCheckBox.Enabled = False

        ' disable the timer
        actionTimer.Enabled = False
        System.Windows.Forms.Application.DoEvents()

        ' terminate module objects 
        If portOne IsNot Nothing Then
            portOne.Dispose()
        End If

        If portZero IsNot Nothing Then
            portZero.Dispose()
        End If

        If inputZero IsNot Nothing Then
            inputZero.Dispose()
        End If
        If inputOne IsNot Nothing Then
            inputOne.Dispose()
        End If
        If inputTwo IsNot Nothing Then
            inputTwo.Dispose()
        End If
        If inputThree IsNot Nothing Then
            inputThree.Dispose()
        End If

        If daqDevice IsNot Nothing Then

            ' close the board
            Me.closeSignalIo()

            daqDevice.Dispose()
            daqDevice = Nothing
        End If

    End Sub

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            Me.instantiateObjects()

            ' set the form caption
            Me.Text = isr.Core.AssemblyExtensions.AssemblyExtensionMethods.BuildProductTimeCaption(New AssemblyInfo(Assembly.GetExecutingAssembly())) & ": SINGLE I/O PANEL"

            ' set tool tips
            initializeUserInterface()

            ' center the form
            Me.CenterToScreen()

            ' turn on the loaded flag
            'loaded = True

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    Private Sub openDeviceCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles openDeviceCheckBox.CheckedChanged
        If openDeviceCheckBox.Checked Then
            ' open the daq board
            Me.open()
            ' set the caption to close
            openDeviceCheckBox.Text = "Cl&ose Device"
        Else
            ' close the daq board
            Me.closeSignalIo()
            ' set the caption to open
            openDeviceCheckBox.Text = "&Open Device"
        End If
    End Sub

    ''' <summary>Occurs upon timer events.</summary>
    ''' <remarks>Use this method to execute all timer actions.</remarks>
    Private Sub actionTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles actionTimer.Tick

        Try
            With inputZero
                .ChannelNumber = Int32.Parse(Me.channelCheckBoxA.Text, Globalization.CultureInfo.CurrentCulture)
                .Gain = CType(isr.io.Universal.AnalogInput.Gains(Convert.ToInt16(Me.rangeCheckBoxA.SelectedIndex, Globalization.CultureInfo.CurrentCulture)), Int32)
                .Acquire()
                Me.voltsTextBoxA.Text = .Voltage.ToString("F4", Globalization.CultureInfo.CurrentCulture)
            End With

            With inputOne
                .ChannelNumber = Int32.Parse(Me.channelCheckBoxB.Text, Globalization.CultureInfo.CurrentCulture)
                .Gain = CType(isr.io.Universal.AnalogInput.Gains(Convert.ToInt16(Me.rangeCheckBoxB.SelectedIndex, Globalization.CultureInfo.CurrentCulture)), Int32)
                .Acquire()
                Me.voltsTextBoxB.Text = .Voltage.ToString("F4", Globalization.CultureInfo.CurrentCulture)
            End With

            With inputTwo
                .ChannelNumber = Int32.Parse(Me.channelCheckBoxC.Text, Globalization.CultureInfo.CurrentCulture)
                .Gain = CType(isr.io.Universal.AnalogInput.Gains(Convert.ToInt16(Me.rangeCheckBoxC.SelectedIndex, Globalization.CultureInfo.CurrentCulture)), Int32)
                .Acquire()
                Me.voltsTextBoxC.Text = .Voltage.ToString("F4", Globalization.CultureInfo.CurrentCulture)
            End With

            With inputThree
                .ChannelNumber = Int32.Parse(Me.channelCheckBoxD.Text, Globalization.CultureInfo.CurrentCulture)
                .Gain = CType(isr.io.Universal.AnalogInput.Gains(Convert.ToInt16(Me.rangeCheckBoxD.SelectedIndex, Globalization.CultureInfo.CurrentCulture)), Int32)
                .Acquire()
                Me.voltsTextBoxD.Text = .Voltage.ToString("F4", Globalization.CultureInfo.CurrentCulture)
            End With

            If Me.ledCheckBox.Checked Then
                Me.ledCheckBox.Checked = False
                Me.messagesTextBox.Text = daqDevice.DoIdentify()
            End If

            If Me.portZeroInputCheckBox.Checked Xor portZero.IsInput Then
                With portZero
                    .IsInput = Me.portZeroInputCheckBox.Checked
                    .ConfigurePort()
                End With
                With Me.portZeroSetCheckBox
                    .Checked = False
                    .Enabled = Not Me.portZeroInputCheckBox.Checked
                End With
                With Me.portZeroReadCheckBox
                    .Checked = False
                    .Enabled = Me.portZeroInputCheckBox.Checked
                End With
            End If

            If Me.portZeroSetCheckBox.Checked Then
                portZero.PortValue = CShort(Me.portZeroNumericUpDown.Value)
            End If
            If Me.portZeroReadCheckBox.Checked Then
                Me.portZeroNumericUpDown.Value = Convert.ToDecimal(portZero.PortValue, Globalization.CultureInfo.CurrentCulture)
            End If

            If Me.portOneInputCheckBox.Checked Xor portOne.IsInput Then
                With portOne
                    .IsInput = Me.portOneInputCheckBox.Checked
                    .ConfigurePort()
                End With
                With Me.portOneSetCheckBox
                    .Checked = False
                    .Enabled = Not Me.portOneInputCheckBox.Checked
                End With
                With Me.portOneReadCheckBox
                    .Checked = False
                    .Enabled = Me.portOneInputCheckBox.Checked
                    .Refresh()
                End With
            End If

            If Me.portOneSetCheckBox.Checked Then
                portOne.PortValue = CShort(Me.portOneNumericUpDown.Value)
            End If
            If Me.portOneReadCheckBox.Checked Then
                Me.portOneNumericUpDown.Value = Convert.ToDecimal(portOne.PortValue, Globalization.CultureInfo.CurrentCulture)
            End If

        Catch ex As System.Exception
            actionTimer.Enabled = False
            Me.messagesTextBox.Text = daqDevice.DeviceErrorMessage
            Me.openDeviceCheckBox.Checked = False
        End Try

    End Sub

#End Region

End Class
