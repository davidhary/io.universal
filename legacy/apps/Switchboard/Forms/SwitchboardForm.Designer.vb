﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SwitchboardForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._OpenButton = New System.Windows.Forms.Button()
        Me._ExitButton = New System.Windows.Forms.Button()
        Me._DialogsComboBox = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        '_OpenButton
        '
        Me._OpenButton.Location = New System.Drawing.Point(385, 12)
        Me._OpenButton.Name = "_OpenButton"
        Me._OpenButton.Size = New System.Drawing.Size(58, 24)
        Me._OpenButton.TabIndex = 11
        Me._OpenButton.Text = "&Open..."
        Me._ToolTip.SetToolTip(Me._OpenButton, "Opens the selected dialog.")
        '
        '_ExitButton
        '
        Me._ExitButton.Location = New System.Drawing.Point(381, 52)
        Me._ExitButton.Name = "_ExitButton"
        Me._ExitButton.Size = New System.Drawing.Size(62, 23)
        Me._ExitButton.TabIndex = 10
        Me._ExitButton.Text = "E&xit"
        '
        '_DialogsComboBox
        '
        Me._DialogsComboBox.BackColor = System.Drawing.SystemColors.Window
        Me._DialogsComboBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._DialogsComboBox.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._DialogsComboBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._DialogsComboBox.Location = New System.Drawing.Point(17, 12)
        Me._DialogsComboBox.Name = "_DialogsComboBox"
        Me._DialogsComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._DialogsComboBox.Size = New System.Drawing.Size(361, 27)
        Me._DialogsComboBox.TabIndex = 9
        Me._DialogsComboBox.Text = "Select Dialog from the list"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(458, 91)
        Me.Controls.Add(Me._OpenButton)
        Me.Controls.Add(Me._ExitButton)
        Me.Controls.Add(Me._DialogsComboBox)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _ToolTip As ToolTip
    Private WithEvents _OpenButton As Button
    Private WithEvents _ExitButton As Button
    Private WithEvents _DialogsComboBox As ComboBox
End Class
