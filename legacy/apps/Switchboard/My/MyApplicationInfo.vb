﻿Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.ProjectTraceEventId.ApplicationNamespace

        Public Const AssemblyTitle As String = "Universal Library Switchboard"
        Public Const AssemblyDescription As String = "Universal Library Switchboard"
        Public Const AssemblyProduct As String = "Universal.Library.Switchboard"

    End Class

End Namespace

