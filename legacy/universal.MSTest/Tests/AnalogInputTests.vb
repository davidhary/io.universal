Imports System.Text

Imports Microsoft.VisualStudio.TestTools.UnitTesting

''' <summary> An analog input tests. </summary>
''' <remarks> David, 2022-01-17. </remarks>
<TestClass()>
Public Class AnalogInputTests

#Region " TEST CONSTRUCTION "

    ''' <summary> My class initialize. </summary>
    ''' <remarks> David, 2022-01-17. </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <ClassInitialize()>
    <CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        AnalogInputTests.TestContext = testContext
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> David, 2022-01-17. </remarks>
    <ClassCleanup()> Public Shared Sub MyClassCleanup()
    End Sub

    ''' <summary> My test initialize. </summary>
    ''' <remarks> David, 2022-01-17. </remarks>
    <TestInitialize()>
    Public Sub MyTestInitialize()

        Try

            ' instantiate the prover bank
            ' instantiate the class
            _device = New isr.io.Universal.Device("Single I/O")

            ' open the driver trapping hardware not found exception
            _device.Open(_boardNumber)

            ' instantiate an analog input channel
            _analogInput = New isr.io.Universal.AnalogInput("Analog Input", _device)

        Catch ex As Exception

            ' close to meet strong guarantees
            Try
                MyTestCleanup()
            Finally
            End Try

            ' throw an exception
            Throw New isr.io.Universal.OperationOpenException($"{NameOf(AnalogInputTests)} failed opening", ex)

        End Try

    End Sub

    ''' <summary>Closes the instance.</summary>
    ''' <remarks>Use this method to close the instance.  The method is class as the 
    '''   TearDown method of the tester.</remarks>
    <TestCleanup()>
    Public Sub MyTestCleanup()

        Try

            ' check if we have instantiated the class
            If _device IsNot Nothing Then
                ' close the driver.
                _device.Close()
            End If

        Catch ex As Exception

            ' throw an exception
            Throw New isr.io.Universal.OperationException($"{NameOf(AnalogInputTests)} failed closing", ex)

        End Try

    End Sub

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    <CLSCompliant(False)>
    Public Shared Property TestContext() As TestContext


#End Region

    ''' <summary> (Unit Test Method) tests analog gain. </summary>
    ''' <remarks> David, 2022-01-17. </remarks>
    <TestMethod()>
    Public Sub AnalogGainTest()
        Const gainRangeTwo As Single = 2
        Const gainRangeTen As Single = 10
        Const gainRangeTwenty As Single = 20
        Const deltaRange As Single = 0.2
        Dim maxVoltage As Single
        maxVoltage = gainRangeTwo
        Me._analogInput.AdjustRange(maxVoltage - 0.1F)
        Assert.AreEqual(maxVoltage, Me._analogInput.AnalogInputRange.Max, deltaRange, "2 Volts Range")

        maxVoltage = gainRangeTwenty
        Me._analogInput.AdjustRange(maxVoltage)
        Assert.AreEqual(maxVoltage, Me._analogInput.AnalogInputRange.Max, deltaRange, "10 Volts Range")

        maxVoltage = gainRangeTen
        Me._analogInput.AdjustRange(maxVoltage)
        Assert.AreEqual(maxVoltage, Me._analogInput.AnalogInputRange.Max, deltaRange, "20 Volts Range")
    End Sub

    ''' <summary> (Unit Test Method) tests measure voltage. </summary>
    ''' <remarks> David, 2022-01-17. </remarks>
    <TestMethod()>
    Public Sub MeasureVoltageTest()
        Const expectedVoltage As Single = 5.0
        Const deltaVoltage As Single = 0.5
        Me._analogInput.AdjustOptimalRange(2 * expectedVoltage)
        Me._analogInput.Acquire()
        Assert.AreEqual(expectedVoltage, Me._analogInput.Voltage, deltaVoltage, "Input Voltage")
    End Sub

    Private _device As isr.io.Universal.Device
    Private _analogInput As isr.io.Universal.AnalogInput
    Private _boardNumber As Int32 = 1

End Class
