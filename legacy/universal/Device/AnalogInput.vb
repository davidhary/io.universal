Imports System.Collections
''' <summary>Defines an analog input channel.</summary>
''' <remarks> (c) 2003 Integrated Scientific Resources, Inc.
''' Licensed under The MIT License.</para><para>
''' David, 08/07/03, 1.0.1314. Created </para></remarks>
Public Class AnalogInput

    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="instanceName">Specifies the name of the instance.</param>
    ''' <remarks>Use this constructor to instantiate this class
    '''   and set the instance name, which is useful in tracing</remarks>
    Public Sub New(
        ByVal instanceName As String)

        ' instantiate the base class
        MyBase.New()
        _instanceName = instanceName

    End Sub

    ''' <summary>Constructs this class.</summary>
    ''' <param name="instanceName">Specifies the name of the instance.</param>
    ''' <param name="deviceLink">
    '''   is an Object expression that specifies the signal link driver for accessing
    '''   the device.</param>
    ''' <remarks>Use this constructor to instantiate this class
    '''   and set the instance name, which is useful in tracing</remarks>
    Public Sub New(
        ByVal instanceName As String,
        ByVal deviceLink As Device)

        Me.New(instanceName)

        Me._analogDevice = deviceLink

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>Gets or sets the dispose status sentinel.</summary>
    Private _disposed As Boolean
    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me._disposed Then

            If disposing Then

                ' Free managed resources when explicitly called
                _statusMessage = String.Empty
                _instanceName = String.Empty

                If Me._analogDevice IsNot Nothing Then
                    Me._analogDevice.Dispose()
                End If

            End If

            ' Free shared unmanaged resources

        End If

        ' set the sentinel indicating that the class was disposed.
        Me._disposed = True
    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>Overrides ToString returning the instance name if not empty.</summary>
    ''' <remarks>Use this method to return the instance name. If instance name is not set, 
    '''   returns the base class ToString value.</remarks>
    Public Overrides Function ToString() As String
        If String.IsNullOrEmpty(Me._instanceName) Then
            Return MyBase.ToString
        Else
            Return Me._instanceName
        End If

        If String.IsNullOrEmpty(Me._instanceName) Then
            Return MyBase.ToString
        Else
            Return Me._instanceName
        End If
    End Function

    Private _instanceName As String = String.Empty
    ''' <summary>Gets or sets the name given to an instance of this class.</summary>
    ''' <value><c>InstanceName</c> is a String property.</value>
    ''' <remarks> David, 11/23/04, 1.0.1788.
    '''   Correct code no to get instance name from .ToString but from MyBase.ToString
    '''   so that calling this method from the child class will not break the rule of
    '''   calling overridable methods from the constructor.
    ''' </para></remarks>
    Public Property InstanceName() As String
        Get
            If Not String.IsNullOrEmpty(_instanceName) Then
                Return _instanceName
            Else
                Return MyBase.ToString
            End If
        End Get
        Set(ByVal value As String)
            _instanceName = value
        End Set
    End Property

    Private _statusMessage As String = String.Empty
    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    Public Property StatusMessage() As String
        Get
            Return _statusMessage
        End Get
        Set(ByVal value As String)
            _statusMessage = value
        End Set
    End Property

#End Region

#Region " METHODS "

    ''' <summary>Configures the analog input.</summary>
    ''' <exception cref="isr.IO.Universal.OperationException" guarantee="strong"></exception>
    Public Sub ConfigureInput()

        If _analogDevice.IsDemo Then
            _analogDevice.DeviceErrorInfo = New MccDaq.ErrorInfo
        Else
            ' configure the port.
            '      _analogDevice.DeviceErrorInfo = _
            '        _analogDevice.DaqDevice.DConfigPort(_portType.FirstPortA, _portDirection)
        End If

        If _analogDevice.DeviceErrorInfo.Value = MccDaq.ErrorInfo.ErrorCode.NoErrors Then
        Else
            ' throw an exception
            Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
          "{0} failed to configure analog input",
          Me.InstanceName)
            Throw New isr.IO.Universal.IOException(Me.StatusMessage, _analogDevice.DeviceErrorInfo.Value, _analogDevice.DeviceErrorInfo.Message)
        End If

    End Sub

    ''' <summary>Increments the analog input gain.</summary>
    ''' <remarks>Use this method to increment the analog input gains.</remarks>
    Public Function IncrementGain() As Single
        ' select a new gain
        Select Case _gain
            Case Is >= 20
                _gain = 20
            Case Is >= 16
                _gain = 20
            Case Is >= 10
                _gain = 16
            Case Is >= 8
                _gain = 10
            Case Is >= 5
                _gain = 8
            Case Is >= 4
                _gain = 8
            Case Is >= 2
                _gain = 4
            Case Is >= 1
                _gain = 2
            Case Else
                _gain = 1
        End Select
        ' set the new gain
        Me.Gain = _gain

    End Function

    ''' <summary>Decrements the analog input gain.</summary>
    ''' <remarks>Use this method to decrement the analog input gains.</remarks>
    Public Function DecrementGain() As Single
        ' select a new gain
        Select Case _gain
            Case Is >= 20
                _gain = 16
            Case Is >= 16
                _gain = 10
            Case Is >= 10
                _gain = 8
            Case Is >= 8
                _gain = 5
            Case Is >= 5
                _gain = 4
            Case Is >= 4
                _gain = 2
            Case Is >= 2
                _gain = 1
            Case Is >= 1
                _gain = 1
            Case Else
                _gain = 1
        End Select
        ' set the new gain
        Me.Gain = _gain

    End Function

    Private randomAnalogInput As Random = New Random
    ''' <summary>Samples a single channel at a specific gain.</summary>
    ''' <exception cref="isr.IO.Universal.IOException" guarantee="strong"></exception>
    ''' <remarks>Use this method to get a single sample from the input channel.</remarks>
    Public Sub Acquire()

        If _analogDevice.IsDemo Then
            _voltage = 0.9F * _voltage +
        0.01F * Convert.ToSingle(randomAnalogInput.NextDouble - 0.5) * Me.AnalogInputRange.Max
            _analogDevice.DeviceErrorInfo = New MccDaq.ErrorInfo
        Else
            ' save previous data value for overflow test
            _dataValuePrev = _dataValue
            ' read an analog input
            _analogDevice.DeviceErrorInfo = _analogDevice.DaqDevice.AIn(Me._channelNumber, Me._rangeCode, _dataValue)
        End If

        If _analogDevice.DeviceErrorInfo.Value = MccDaq.ErrorInfo.ErrorCode.NoErrors Then
            ' if no error convert
            _analogDevice.DeviceErrorInfo = _analogDevice.DaqDevice.ToEngUnits(_rangeCode, _dataValue, _voltage)
        Else
            ' throw an exception
            Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
          "{0} failed getting analog input",
          Me.InstanceName)
            Throw New isr.IO.Universal.IOException(Me.StatusMessage,
          _analogDevice.DeviceErrorInfo.Value, _analogDevice.DeviceErrorInfo.Message)
        End If

    End Sub

    ''' <summary>Sets the offset voltage correction to the current voltage.</summary>
    ''' <remarks>Use this method to set the internal voltage offset value used to correct
    '''   the analog input voltage for offset.</remarks>
    Public Sub UpdateVoltageOffset()

        _voltageOffset = _voltage

    End Sub

    ''' <summary>Adjust the gain to fit the voltage within the device range.</summary>
    ''' <returns>A Boolean data type</returns>
    ''' <param name="Voltage">
    '''   is a Single expression that specifies the input voltage to check for range.</param>
    Public Function AdjustRange(ByVal voltage As Single) As Boolean

        Dim rangeChanged As Boolean = False
        ' set gain to maximum
        Me.Gain = Me._gainMax
        Do Until Me.inRange(voltage) Or (Me.Gain = Me._gainMin)
            ' if not in range, 
            Me.DecrementGain()
            rangeChanged = True
        Loop
        Return rangeChanged

    End Function

    ''' <summary>updates the range to fit within the optimal ranges.</summary>
    ''' <returns>A Boolean data type</returns>
    ''' <param name="Voltage">
    '''   is a Single expression that specifies the input voltage to check for range.</param>
    ''' <remarks>Call this method to adjust the analog input gain to fit the specified 
    '''   voltage within the optimal gain.</remarks>
    Public Function AdjustOptimalRange(ByVal voltage As Single) As Boolean

        Dim rangeChanged As Boolean = False
        Do Until Me.inOptimalRange(voltage)

            If Me.inHigherGainRange(voltage) Then
                ' if in higher gain, increment the gain
                Me.IncrementGain()
                rangeChanged = True
            ElseIf Me.inLowerGainRange(voltage) Then
                ' if in lower gain range, decrement the gain
                Me.DecrementGain()
                rangeChanged = True
            End If

        Loop
        Return rangeChanged

    End Function

#End Region

#Region " SHARED PROPERTIES "

    ''' <summary>Gets or sets the list Analog Input Channels.</summary>
    ''' <value><c>Channels</c> is an ArrayList property.</value>
    Public Shared ReadOnly Property Channels() As ArrayList
        Get
            Dim channelList As ArrayList = New ArrayList
            channelList.AddRange(New Object() {0, 1, 2, 3})
            Return channelList
        End Get
    End Property

    ''' <summary>Lists the Analog Input Gains.</summary>
    ''' <value><c>Gains</c> is an ArrayList property.</value>
    Public Shared ReadOnly Property Gains() As ArrayList
        Get
            Dim gainList As New ArrayList
            gainList.AddRange(New Object() {1, 2, 4, 5, 8, 10, 16, 20})
            Return gainList
        End Get
    End Property

    ''' <summary>Lists the Analog Input Ranges.</summary>
    ''' <value><c>Ranges</c> is an ArrayList property.</value>
    Public Shared ReadOnly Property Ranges() As ArrayList
        Get
            Dim rangeList As New ArrayList
            rangeList.AddRange(New Object() {"±20", "±10", "±5", "±4", "±2.5", "±2", "±1.25", "±1"})
            Return rangeList
        End Get
    End Property

#End Region

#Region " PROPERTIES "

    Private relativeGainRange As SignalRange = New SignalRange(-9.5, 9.5)
    Private higherGainRange As SignalRange = New SignalRange(-2.5, 2.5)

    Private _analogDevice As Device

    Private _channelNumber As Int32
    ''' <summary>Gets or sets the analog input channel code.</summary>
    ''' <value><c>ChannelNumber</c> is an Int32 property.</value>
    ''' <remarks>Use this property to get or set the analog input channel.</remarks>
    Public Property ChannelNumber() As Int32
        Get
            Return Me._channelNumber
        End Get
        Set(ByVal value As Int32)
            Me._channelNumber = value
        End Set
    End Property

    Private _gainMax As Int32 = 20
    Private _gainMin As Int32 = 1
    Private _gain As Int32 = 1
    Private _gainNextHigher As Int32 = 2
    Private _rangeCode As MccDaq.Range = MccDaq.Range.Bip10Volts
    ''' <summary>Gets or sets the analog input gain.</summary>
    ''' <value><c>Gain</c> is an Int32 property.</value>
    ''' <remarks>Use this property to get or set the analog input gain which to use for single 
    '''   analog inputs when calling the Read method.  This property also sets the
    '''   analog input gain code (GainCode).</remarks>
    Public Property Gain() As Int32
        Get
            Return _gain
        End Get
        Set(ByVal value As Int32)
            ' set the gain code based on the gain value
            Select Case value
                Case Is >= 20
                    _rangeCode = MccDaq.Range.Bip1Volts
                    _gain = 20
                    _gainNextHigher = 20
                Case Is >= 16
                    _rangeCode = MccDaq.Range.Bip1Pt25Volts
                    _gain = 16
                    _gainNextHigher = 20
                Case Is >= 10
                    _rangeCode = MccDaq.Range.Bip2Volts
                    _gain = 10
                    _gainNextHigher = 16
                Case Is >= 8
                    _rangeCode = MccDaq.Range.Bip2Pt5Volts
                    _gain = 8
                    _gainNextHigher = 10
                Case Is >= 5
                    _rangeCode = MccDaq.Range.Bip4Volts
                    _gain = 5
                    _gainNextHigher = 8
                Case Is >= 4
                    _rangeCode = MccDaq.Range.Bip5Volts
                    _gain = 4
                    _gainNextHigher = 8
                Case Is >= 2
                    _rangeCode = MccDaq.Range.Bip10Volts
                    _gain = 2
                    _gainNextHigher = 4
                Case Is >= 1
                    _rangeCode = MccDaq.Range.Bip20Volts
                    _gain = 1
                    _gainNextHigher = 2
                Case Else
                    _rangeCode = MccDaq.Range.Bip20Volts
                    _gain = 1
                    _gainNextHigher = 2
            End Select
            ' set the analog input range based on the new range code
            setRanges()
        End Set
    End Property

    Private _dataValueMin As Int32
    Private _dataValueMax As Int32 = 4095
    Private _dataValuePrev As Int32 = 1
    Private _dataValue As Int16
    ''' <summary>Gets or sets the analog input data value.</summary>
    ''' <value><c>DataValue</c> is a Unsigned 16 Bit Int32 property that can be read from (read only).</value>
    ''' <remarks>Use this property to get the analog input data value read using GetAnalogInput.</remarks>
    Public ReadOnly Property DataValue() As Int32
        Get
            Return _dataValue
        End Get
    End Property

    Private _voltage As Single
    ''' <summary>Gets or sets the analog input voltage.</summary>
    ''' <value><c>Voltage</c> is a Single-Precision property that can be read from (read only).</value>
    ''' <remarks>Use this property to get analog input voltage read using GetAnalogInput.</remarks>
    Public ReadOnly Property Voltage() As Single
        Get
            Return _voltage
        End Get
    End Property

    Private _voltageOffset As Single
    ''' <summary>Gets or sets the analog input voltage corrected for offset.</summary>
    ''' <value><c>VoltageCorrected</c> is a Single-Precision property that can be read from (read only).</value>
    ''' <remarks>Use this property to get analog input voltage read using Read 
    '''   and corrected for offset as set in UpdateVoltageOffset.</remarks>
    Public ReadOnly Property VoltageCorrected() As Single
        Get
            If _Invert Then
                Return _voltageOffset - _voltage
            Else
                Return _voltage - _voltageOffset
            End If
        End Get
    End Property

    Private _analogInputRange As SignalRange = New SignalRange(-10, 10)
    ''' <summary>Gets or sets the analog input range for the prescribed gain.</summary>
    ''' <value><c>AnalogInputRange</c> is a SignalRange property that can be read from (read only).</value>
    ''' <remarks>Use this property to get the maximum, minimum, or range values of the
    '''   analog input voltage for the range settings for the specified channel and gain.</remarks>
    Public ReadOnly Property AnalogInputRange() As SignalRange
        Get
            Return _analogInputRange
        End Get
    End Property

    Private _analogInputAutoRangeLimen As Single = 0.95
    ''' <summary>Gets or sets the analog input relative range.</summary>
    ''' <value><c>AnalogInputAutoRangeLimen</c> is a Single property.</value>
    ''' <remarks>Use this property to get or set the range within the analog input 
    '''   range outside of which auto ranging kicks in.</remarks>
    Public Property AnalogInputAutoRangeLimen() As Single
        Get
            Return _analogInputAutoRangeLimen
        End Get
        Set(ByVal value As Single)
            _analogInputAutoRangeLimen = value
        End Set
    End Property

    Private _Invert As Boolean
    ''' <summary>Gets or sets the invert status of the analog input.</summary>
    ''' <value><c>IsInvert</c> is a Boolean property.</value>
    ''' <remarks>Use this property to turn on or off inversion of the input voltage when calculating the corrected
    '''   input voltage</remarks>
    Public Property IsInvert() As Boolean
        Get
            Return _Invert
        End Get
        Set(ByVal value As Boolean)
            _Invert = value
        End Set
    End Property

    ''' <summary>Gets or sets the over voltage outcome value.</summary>
    ''' <value><c>IsOverVoltage</c> is a <see cref="Boolean"/> property that can be read from (read only).</value>
    ''' <remarks>Use this property to get over voltage outcome of the last reading.</remarks>
    ''' <remarks> David, 05/30/03, 1.0.1245. Adapt for PMD-1208. </para></remarks>
    ''' <remarks> David, 05/30/03, 1.0.1123. Use range limits </para></remarks>
    Public ReadOnly Property IsOverVoltage() As Boolean
        Get
            Return (_voltage >= _analogInputRange.High) Or (_voltage <= _analogInputRange.Low)
        End Get
    End Property

    ''' <summary>Gets or sets the overflow outcome value.</summary>
    ''' <value><c>Overflowed</c> is a <see cref="Boolean"/> property that can be read from (read only).</value>
    ''' <remarks>Use this property to test if the last two data values hit the A/D range limits.</remarks>
    Public ReadOnly Property Overflowed() As Boolean
        Get
            Return Me.dataValueOverflowed(Me._dataValue) And Me.dataValueOverflowed(Me._dataValuePrev)
        End Get
    End Property

    Private _singleEnded As Boolean
    ''' <summary>Gets or sets the single-ended attribute of the input channel.</summary>
    ''' <value><c>SingleEnded</c> is a <see cref="Boolean"/> property that can be read from (read only).</value>
    ''' <remarks>Use this property to determine if the specified input channel is single ended.</remarks>
    Public ReadOnly Property SingleEnded() As Boolean
        Get
            Return _singleEnded
        End Get
    End Property

#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary>Sets the analog input ranges.</summary>
    ''' <exception cref="isr.IO.Universal.OperationException" guarantee="strong">
    '''   Failed to set ranges.</exception>
    ''' <remarks>Call this method from the class properties setting gain or polarity
    '''   to set the ranges for such values.</remarks>
    Private Function setRanges() As Boolean

        Try

            If Me.SingleEnded Then
                _analogInputRange = New SignalRange(20, 1, True)
            Else
                _analogInputRange = New SignalRange(40, _gain, True)
            End If

            ' these settings are needed for auto range
            ' if unipolar, set ranges for in auto range, set the high and low gain ranges.
            With Me.relativeGainRange
                .Max = _analogInputRange.Max - 0.5F * (1.0F - _analogInputAutoRangeLimen) * _analogInputRange.Range
                .Min = _analogInputRange.Min + 0.5F * (1.0F - _analogInputAutoRangeLimen) * _analogInputRange.Range
            End With
            ' set the relative range for higher gain making sure that upon using the 
            ' higher gain, the signal will be within the AnalogInputAutoRangeLimen
            Dim higherGainAutoRangeLimen As Single =
          (_analogInputAutoRangeLimen - 0.01F) * _gain / _gainNextHigher
            With Me.higherGainRange
                .Max = _analogInputRange.Midrange + 0.5F * higherGainAutoRangeLimen * _analogInputRange.Range
                .Min = _analogInputRange.Midrange - 0.5F * higherGainAutoRangeLimen * _analogInputRange.Range
            End With

        Catch exn As System.Exception

            ' add the message to the exception
            Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
          "{0} failed to set ranges",
          Me.InstanceName)
            Throw New isr.IO.Universal.OperationException(Me.StatusMessage, exn)

        End Try

    End Function

    Private Function dataValueOverflowed(ByVal dataValue As Int32) As Boolean
        Return (dataValue <= _dataValueMin) Or (dataValue >= _dataValueMax)
    End Function

    ''' <summary>Check the analog input ranges.</summary>
    ''' <param name="voltage">
    '''   is a Single expression that specifies the input voltage to check for range.</param>
    ''' <remarks>Call this method to check if the signal is in full range.</remarks>
    Private Function inRange(ByVal voltage As Single) As Boolean
        With Me._analogInputRange
            Return ((voltage <= .Max) And (voltage >= .Min))
        End With
    End Function

    ''' <summary>Checks the analog input ranges.</summary>
    ''' <returns><c>isAinOptimalRange</c> returns true if the input signal is in optional range.</returns>
    ''' <param name="voltage">
    '''   is a Single expression that specifies the input voltage to check for range.</param>
    ''' <remarks>Call this method to check if the signal is in optimal range. The 
    '''   signal is in optimal range if it is within the analog input relative 
    '''   range and outside the range of higher gain.</remarks>
    Private Function inOptimalRange(ByVal voltage As Single) As Boolean
        Return Not (inHigherGainRange(voltage) Or inLowerGainRange(voltage))
    End Function

    Private Function inHigherGainRange(ByVal voltage As Single) As Boolean
        With Me.higherGainRange
            Return ((voltage < .Max) And (voltage > .Min)) _
                And (Not Me.inMaxAnalogInputGainRange)
        End With
    End Function

    Private Function inMaxAnalogInputGainRange() As Boolean
        Return (_gain >= _gainMax)
    End Function

    Private Function inLowerGainRange(ByVal voltage As Single) As Boolean
        With Me.relativeGainRange
            Return ((voltage > .Max) Or (voltage < .Min)) _
              And (Not Me.inMinAnalogInputGainRange)
        End With
    End Function

    Private Function inMinAnalogInputGainRange() As Boolean
        Return (_gain <= _gainMin)
    End Function

#End Region

End Class

