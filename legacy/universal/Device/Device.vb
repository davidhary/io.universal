''' <summary>Links to the PMD-1208LS USB data acquisition module.</summary>
''' <remarks> (c) 2002 Integrated Scientific Resources, Inc.
''' Licensed under The MIT License.</para><para>
''' David, 12/05/02, 1.0.997. Created 
''' David, 05/30/03, 1.0.1245.x. Adapt for PMD-1208
''' David, 96/03/04, 1.0.1615.x. Adapt for installing without the CBUL system files </para></remarks>
Public Class Device

    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    Public Sub New()

        Me.New(String.Empty)

    End Sub

    ''' <summary>Constructs this class.</summary>
    ''' <param name="instanceName">Specifies the name of the instance.</param>
    Public Sub New(ByVal instanceName As String)

        MyBase.New()
        _instanceName = instanceName

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>Gets or sets the dispose status sentinel.</summary>
    Private _disposed As Boolean
    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me._disposed Then

            If disposing Then

                ' Free managed resources when explicitly called
                _statusMessage = String.Empty
                _instanceName = String.Empty

            End If

            ' Free shared unmanaged resources

        End If

        ' set the sentinel indicating that the class was disposed.
        Me._disposed = True
    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#Region " SHARED "
#If False Then

    ''' <summary>Returns a Boolean selected value</summary>
    ''' <param name="condition">The condition for selecting the true or false parts.</param>
    ''' <param name="truePart">The part selected if the condition is True.</param>
    ''' <param name="falsePart">The part selected if the condition is false.</param>
    Public Shared Function ImmediateIf(ByVal condition As Boolean, ByVal truePart As Boolean, ByVal falsePart As Boolean) As Boolean
      If condition Then
        Return truePart
      Else
        Return falsePart
      End If
    End Function

    ''' <summary>Returns a string selected value</summary>
    ''' <param name="condition">The condition for selecting the true or false parts.</param>
    ''' <param name="truePart">The part selected if the condition is True.</param>
    ''' <param name="falsePart">The part selected if the condition is false.</param>
    Public Shared Function ImmediateIf(ByVal condition As Boolean, ByVal truePart As String, ByVal falsePart As String) As String
      If condition Then
        Return truePart
      Else
        Return falsePart
      End If
    End Function

    ''' <summary>Returns a Byte selected value</summary>
    ''' <param name="condition">The condition for selecting the true or false parts.</param>
    ''' <param name="truePart">The part selected if the condition is True.</param>
    ''' <param name="falsePart">The part selected if the condition is false.</param>
    Public Shared Function ImmediateIf(ByVal condition As Boolean, ByVal truePart As Byte, ByVal falsePart As Byte) As Byte
      If condition Then
        Return truePart
      Else
        Return falsePart
      End If
    End Function

    ''' <summary>Returns a double selected value</summary>
    ''' <param name="condition">The condition for selecting the true or false parts.</param>
    ''' <param name="truePart">The part selected if the condition is True.</param>
    ''' <param name="falsePart">The part selected if the condition is false.</param>
    Public Shared Function ImmediateIf(ByVal condition As Boolean, ByVal truePart As Double, ByVal falsePart As Double) As Double
      If condition Then
        Return truePart
      Else
        Return falsePart
      End If
    End Function

#End If
#End Region

#Region " BASE METHODS AND PROPERTIES "

    ''' <summary>Overrides ToString returning the instance name if not empty.</summary>
    ''' <remarks>Use this method to return the instance name. If instance name is not set, 
    '''   returns the base class ToString value.</remarks>
    Public Overrides Function ToString() As String
        If String.IsNullOrEmpty(Me._instanceName) Then
            Return MyBase.ToString
        Else
            Return Me._instanceName
        End If
    End Function

    Private _isOpen As Boolean
    ''' <summary>Gets or sets the opened status of the instance.</summary>
    ''' <value><c>IsOpen</c> is a <see cref="Boolean"/> property that is True if the 
    '''   instance is open.</value>
    Public Property IsOpen() As Boolean
        Get
            Return _isOpen
        End Get
        Set(ByVal value As Boolean)
            _isOpen = value
        End Set
    End Property

    Private _instanceName As String = String.Empty
    ''' <summary>Gets or sets the name given to an instance of this class.</summary>
    ''' <value><c>InstanceName</c> is a String property.</value>
    ''' <remarks>If instance name is not set, returns .ToString.</remarks>
    ''' <remarks> David, 11/23/04", 1.0.1788.x">
    '''   Correct code no to get instance name from .ToString but from MyBase.ToString
    '''   so that calling this method from the child class will not break the rule of
    '''   calling overridable methods from the constructor.
    ''' </para></remarks>
    Public Property InstanceName() As String
        Get
            If Not String.IsNullOrEmpty(_instanceName) Then
                Return _instanceName
            Else
                Return MyBase.ToString
            End If
        End Get
        Set(ByVal value As String)
            _instanceName = value
        End Set
    End Property

    Private _statusMessage As String = String.Empty
    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    ''' <remarks>Use this property to get the status message generated by the object.</remarks>
    Public Property StatusMessage() As String
        Get
            Return _statusMessage
        End Get
        Set(ByVal value As String)
            _statusMessage = value
        End Set
    End Property

#End Region

#Region " METHODS "

    ''' <summary>opens this instance.</summary>
    ''' <exception cref="isr.IO.Universal.OperationException" guarantee="strong">
    '''   Failed I/O initialize error handling or declaring version level.
    ''' </exception>
    ''' <remarks>Use this method to open the instance.</remarks>
    ''' <remarks> David, 05/30/03", 1.0.1245.x">
    ''' Adapt for PMD-1208 </para>
    ''' David, 08/07/03, 1.0.1314.
    '''   Declare revision level of library and initialize error reporting.
    ''' </para>
    ''' David, 06/03/04, 1.0.1615
    '''   Ignore first fail so as not to use CBUL system drivers.
    ''' </para></remarks>
    Public Overridable Function [Open](ByVal deviceNumber As Int32) As Boolean

        Dim revisionNumber As Single = 0

        Try

            ' Declare a private MccBoard _daqDevice object for Device number 
            _daqDevice = New MccDaq.MccBoard(deviceNumber)

            ' declare revision level of Universal Library
            If MccDaq.MccService.DeclareRevision(MccDaq.MccService.CurrentRevNum).Value = MccDaq.ErrorInfo.ErrorCode.NoErrors Then

                ' initiate error handling
                If Me.InitiateErrorHandling Then

                    ' get the software revision
                    revisionNumber = Device.SoftwareRevision

                    ' trap error on first call to device per instructions from MCC VP of S/w Eng. 
                    ' because the first time it tries to access the CBUL32.sys driver
                    Try
                        Me.DoIdentify()
                    Catch ex As ApplicationException
                    End Try

                    ' identify the device
                    Me.DoIdentify()

                    Me.IsOpen = True
                    Return Me.IsOpen

                Else

                    Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
          "{0} failed to initiate error handling",
              Me.InstanceName)
                    Throw New isr.IO.Universal.OperationException(Me.StatusMessage)

                End If

            Else

                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
          "{0} failed to declare revision level of Universal Library",
            Me.InstanceName)
                Throw New isr.IO.Universal.OperationException(Me.StatusMessage)

            End If

        Catch exn As isr.IO.Universal.HardwareNotFoundException

            ' if hardware not found, just throw the same exception
            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Catch exn As System.ApplicationException

            ' close device to meet strong guarantees
            Try
                Me.Close()
            Finally
            End Try

            ' throw an exception
            Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
          "{0} operation failed opening", Me.InstanceName)
            Throw New isr.IO.Universal.OperationOpenException(StatusMessage, exn)

        End Try

        If _deviceErrorInfo.Value <> MccDaq.ErrorInfo.ErrorCode.NoErrors Then

            ' if error, 

            ' close device to meet strong guarantees
            Try
                Me.Close()
            Catch
            Finally
            End Try
            ' throw an exception
            Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
        "{0} operation failed opening. PMD-1208 returned error {1}:: {2}",
            Me.InstanceName, Me.DeviceErrorCode, Me.DeviceErrorMessage)
            Throw New isr.IO.Universal.OperationOpenException(Me.StatusMessage)
        ElseIf revisionNumber <= 0 Then
            ' throw an exception
            Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
          "{0} operation failed opening. PMD-1208 returned an invalid firmware version.",
          Me.InstanceName)
            Throw New isr.IO.Universal.HardwareNotFoundException(Me.StatusMessage)
        End If

    End Function

    ''' <summary>Closes the instance.</summary>
    ''' <exception cref="isr.IO.Universal.OperationInitializeException" guarantee="strong"></exception>
    ''' <remarks>Use this method to close the instance.</remarks>
    ''' <remarks> David, 05/30/03", 1.0.1245.x">
    '''   Adapt for PMD-1208
    ''' </para></remarks>
    Public Overridable Function [Close]() As Boolean

        Try

            ' clear the error code
            _deviceErrorInfo = New MccDaq.ErrorInfo

            Me.IsOpen = False
            Return Not Me.IsOpen

        Catch exn As System.ApplicationException
            ' throw an exception
            Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
          "{0} operation failed closing", Me.InstanceName)
            Throw New isr.IO.Universal.OperationInitializeException(Me.StatusMessage, exn)
        End Try

    End Function

    ''' <summary>Identifies the device.</summary>
    ''' <exception cref="isr.IO.Universal.OperationException" guarantee="strong"></exception>
    ''' <remarks>Use this method to flash the LED and return the device name.</remarks>
    Public Function DoIdentify() As String

        ' flash the led
        If Me.IsDemo Then
            _deviceErrorInfo = New MccDaq.ErrorInfo
        Else
            _deviceErrorInfo = _daqDevice.FlashLED()
        End If

        If _deviceErrorInfo.Value = MccDaq.ErrorInfo.ErrorCode.NoErrors Then
            ' if no error return the device name
            If Me.IsDemo Then
                Return "demo device"
            Else
                ' if no error return the device name
                Return _daqDevice.BoardName()
            End If
        ElseIf _deviceErrorInfo.Value = MccDaq.ErrorInfo.ErrorCode.BadBoard Then
            ' throw an exception
            Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
          "{0} failed to locate the device.",
          Me.InstanceName)
            Throw New isr.IO.Universal.HardwareNotFoundException(Me.StatusMessage,
          _deviceErrorInfo.Value, _deviceErrorInfo.Message)
        Else
            ' throw an exception
            Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
          "{0} failed to identify the device.",
          Me.InstanceName)
            Throw New isr.IO.Universal.IOException(Me.StatusMessage, _deviceErrorInfo.Value, _deviceErrorInfo.Message)
        End If

    End Function

    ''' <summary>Initiates error handling.</summary>
    ''' <returns>A Boolean data type that is true if success</returns>
    ''' <remarks>Use this method to initiate error handling so that errors will not 
    '''   generate a message to the screen -- this program checks the returned error 
    '''   code after each library call to determine if an error occurred. The program 
    '''   will always continue executing when an error occurs.
    ''' </remarks>
    Public Function InitiateErrorHandling() As Boolean

        ' Initiate error handling
        '  activating error handling will trap errors like
        '  bad channel numbers and non-configured conditions.
        '  Parameters:
        '    MccDaq.ErrorReporting.PrintAll :all warnings and errors encountered will be printed
        '    MccDaq.ErrorHandling.StopAll   :if any error is encountered, the program will stop
        _deviceErrorInfo = MccDaq.MccService.ErrHandling(MccDaq.ErrorReporting.DontPrint,
            MccDaq.ErrorHandling.DontStop)
        Return (_deviceErrorInfo.Value = MccDaq.ErrorInfo.ErrorCode.NoErrors)

    End Function

#End Region

#Region " Device Properties "

    Private _deviceNumber As Int32
    ''' <summary>Gets or sets the Device serial number..</summary>
    ''' <value><c>SerialNumber</c> is an Int32 property.</value>
    ''' <remarks>Use this property to get the Device number as it is identified by the CB.CFG 
    '''   configuration of the Universal Library.</remarks>
    Public ReadOnly Property DeviceNumber() As Int32
        Get
            Return _deviceNumber
        End Get
    End Property

    ' Declare a private MccBoard _daqDevice object 
    Private _daqDevice As MccDaq.MccBoard
    ''' <summary>Gets or sets the Device board.</summary>
    ''' <value><c>DaqDevice</c> is an Object MccDaq.MccBoard property.</value>
    ''' <remarks>Use this property to get project-level reference to the MccDaq board.</remarks>
    Friend ReadOnly Property DaqDevice() As MccDaq.MccBoard
        Get
            Return _daqDevice
        End Get
    End Property

    Private _deviceErrorInfo As MccDaq.ErrorInfo
    Friend Property DeviceErrorInfo() As MccDaq.ErrorInfo
        Get
            Return _deviceErrorInfo
        End Get
        Set(ByVal value As MccDaq.ErrorInfo)
            _deviceErrorInfo = value
        End Set
    End Property

    ''' <summary>Returns the current Device error.</summary>
    ''' <value><c>DeviceErrorMessage</c> is a String property.</value>
    Public ReadOnly Property DeviceErrorMessage() As String
        Get
            Return _deviceErrorInfo.Message()
        End Get
    End Property

    ''' <summary>Returns the current Device error code.</summary>
    ''' <value><c>DeviceErrorCode</c> is a String property.</value>
    Public ReadOnly Property DeviceErrorCode() As Int32
        Get
            Return _deviceErrorInfo.Value
        End Get
    End Property

    Private _isDemo As Boolean
    ''' <summary>Gets or sets the demo mode.</summary>
    ''' <value><c>IsDemo</c> is a Boolean property.</value>
    ''' <remarks>Use this property to get or set the demo mode of the Device.
    '''   When in demo mode, Device returns demo values.</remarks>
    Public Property IsDemo() As Boolean
        Get
            Return _isDemo
        End Get
        Set(ByVal value As Boolean)
            _isDemo = value
        End Set
    End Property

    ''' <summary>Gets or sets the Device Firmware Version.</summary>
    ''' <value><c>SoftwareRevision</c> is a Single Precision property.</value>
    ''' <remarks>Use this property to get the firmware version of the Device.</remarks>
    Public Shared Property SoftwareRevision() As Single
        Get
            Return MccDaq.MccService.CurrentRevNum
        End Get
        Set(ByVal value As Single)
            MccDaq.MccService.DeclareRevision(value)
        End Set
    End Property

#End Region

End Class

''' <summary>This structure defines a scalar range for A/D converters.</summary>
''' <remarks>The SignalRange provides the basic structure for handling the range of a scalar.(c) 2003 Integrated Scientific Resources, Inc.
''' Licensed under The MIT License.</para><para>
''' David, 01/13/03", 1.0.1122.x">
''' Created
''' </para></remarks>
Public Structure SignalRange

    Private _min As Single
    Public Property Min() As Single
        Get
            Return _min
        End Get
        Set(ByVal value As Single)
            _min = value
        End Set
    End Property

    Private _max As Single
    Public Property Max() As Single
        Get
            Return _max
        End Get
        Set(ByVal value As Single)
            _max = value
        End Set
    End Property

    Private _resolution As Single
    Public Property Resolution() As Single
        Get
            Return _resolution
        End Get
        Set(ByVal value As Single)
            _resolution = value
        End Set
    End Property

    Private _bits As Int32
    Public Property Bits() As Int32
        Get
            Return _bits
        End Get
        Set(ByVal value As Int32)
            _bits = value
        End Set
    End Property

    Private _low As Single
    Public Property Low() As Single
        Get
            Return _low
        End Get
        Set(ByVal value As Single)
            _low = value
        End Set
    End Property

    Private _high As Single
    Public Property High() As Single
        Get
            Return _high
        End Get
        Set(ByVal value As Single)
            _high = value
        End Set
    End Property

    Public Sub New(ByVal minValue As Single, ByVal maxValue As Single)
        SetRange(minValue, maxValue)
    End Sub

    Public Sub New(ByVal rangeValue As Single, ByVal gainValue As Single,
        ByVal bipolarValue As Boolean)
        Dim minValue, maxValue As Single
        If bipolarValue Then
            maxValue = 0.5F * rangeValue / gainValue
            minValue = -maxValue
        Else
            maxValue = rangeValue / gainValue
            minValue = 0
        End If
        SetRange(minValue, maxValue)
    End Sub

    Public Overloads Function Equals(ByVal rangeCompared As SignalRange) As Boolean
        Return (Math.Ceiling(_min / _resolution) = Math.Ceiling(rangeCompared.Min / rangeCompared.Resolution)) _
          And (Math.Ceiling(_max / _resolution) = Math.Ceiling(rangeCompared.Max / rangeCompared.Resolution)) _
          And (_resolution = rangeCompared.Resolution)
    End Function

    Public Overloads Overrides Function GetHashCode() As Int32
        Return Convert.ToInt32(Math.Ceiling(_min / _resolution), Globalization.CultureInfo.CurrentCulture) _
          Xor Convert.ToInt32(Math.Ceiling(_max / _resolution), Globalization.CultureInfo.CurrentCulture)
    End Function

    Private Sub SetRange(ByVal minValue As Single, ByVal maxValue As Single)
        _min = minValue
        _max = maxValue
        _bits = 12
        _resolution = Convert.ToSingle(Me.Range / (2 ^ _bits), Globalization.CultureInfo.CurrentCulture)
        _low = _min + _resolution
        _high = _max - _resolution - _resolution
    End Sub

    Public Overrides Function ToString() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture,
              "({0},{1})", _min.ToString(Globalization.CultureInfo.CurrentCulture), _max.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

    Public ReadOnly Property Range() As Single
        Get
            Return (_max - _min)
        End Get
    End Property

    Public ReadOnly Property Midrange() As Single
        Get
            Return 0.5F * (_max + _min)
        End Get
    End Property

End Structure

