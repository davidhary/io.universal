''' <summary>Defines a digital port for I/O..</summary>
''' <remarks> (c) 2003 Integrated Scientific Resources, Inc.
''' Licensed under The MIT License.</para><para>
''' David, 08/07/03, 1.0.1314. Created. </para></remarks>
Public Class DigitalPort

    ' based on the BaseClass inheritable class
    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="instanceName">Specifies the name of the instance.</param>
    ''' <param name="deviceLink">
    '''   is an Object expression that specifies the signal link driver for accessing
    '''   the device.</param>
    ''' <remarks>Use this constructor to instantiate this class
    '''   and set the instance name, which is useful in tracing</remarks>
    Public Sub New(ByVal instanceName As String,
        ByVal deviceLink As Device)

        MyBase.New()
        _instanceName = instanceName
        Me._digitalDevice = deviceLink

    End Sub

    ''' <summary>Constructs this class.</summary>
    ''' <param name="deviceLink">
    '''   is an Object expression that specifies the signal link driver for accessing
    '''   the device.</param>
    ''' <remarks>Use this constructor to instantiate this class
    '''   and set the instance name, which is useful in tracing</remarks>
    Public Sub New(ByVal deviceLink As Device)

        Me.New(deviceLink.ToString, deviceLink)

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>Gets or sets the dispose status sentinel.</summary>
    Private _disposed As Boolean
    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me._disposed Then

            If disposing Then

                ' Free managed resources when explicitly called
                _statusMessage = String.Empty
                _instanceName = String.Empty

                If Me._digitalDevice IsNot Nothing Then
                    Me._digitalDevice.Dispose()
                End If

            End If

            ' Free shared unmanaged resources

        End If

        ' set the sentinel indicating that the class was disposed.
        Me._disposed = True
    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>Overrides ToString returning the instance name if not empty.</summary>
    ''' <remarks>Use this method to return the instance name. If instance name is not set, 
    '''   returns the base class ToString value.</remarks>
    Public Overrides Function ToString() As String
        If String.IsNullOrEmpty(Me._instanceName) Then
            Return MyBase.ToString
        Else
            Return Me._instanceName
        End If
    End Function

    Private _instanceName As String = String.Empty
    ''' <summary>Gets or sets the name given to an instance of this class.</summary>
    ''' <value><c>InstanceName</c> is a String property.</value>
    ''' <remarks> David, 11/23/04, 1.0.1788.
    '''   Correct code no to get instance name from .ToString but from MyBase.ToString
    '''   so that calling this method from the child class will not break the rule of
    '''   calling overridable methods from the constructor.
    ''' </para></remarks>
    Public Property InstanceName() As String
        Get
            If Not String.IsNullOrEmpty(_instanceName) Then
                Return _instanceName
            Else
                Return MyBase.ToString
            End If
        End Get
        Set(ByVal value As String)
            _instanceName = value
        End Set
    End Property

    Private _statusMessage As String = String.Empty
    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    Public Property StatusMessage() As String
        Get
            Return _statusMessage
        End Get
        Set(ByVal value As String)
            _statusMessage = value
        End Set
    End Property

#End Region

#Region " METHODS "

    ''' <summary>Configures the digital port.</summary>
    ''' <exception cref="isr.IO.Universal.OperationException" guarantee="strong"></exception>
    Public Sub ConfigurePort()

        If _digitalDevice.IsDemo Then
            _digitalDevice.DeviceErrorInfo = New MccDaq.ErrorInfo
        Else
            ' configure the port.
            _digitalDevice.DeviceErrorInfo =
        _digitalDevice.DaqDevice.DConfigPort(_portType, _portDirection)
        End If

        If _digitalDevice.DeviceErrorInfo.Value = MccDaq.ErrorInfo.ErrorCode.NoErrors Then
        Else
            ' throw an exception
            Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
          "{0} failed to configure digital port.",
          Me.InstanceName)
            Throw New isr.IO.Universal.IOException(Me.StatusMessage, _digitalDevice.DeviceErrorInfo.Value, _digitalDevice.DeviceErrorInfo.Message)
        End If

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>Gets or sets the reference to the parent device.</summary>
    Private _digitalDevice As Device

    Private _input As Boolean = True
    Private _portDirection As MccDaq.DigitalPortDirection = MccDaq.DigitalPortDirection.DigitalIn
    ''' <summary>Gets or sets the condition for this is an input port.</summary>
    Public Property IsInput() As Boolean
        Get
            Return _input
        End Get
        Set(ByVal value As Boolean)
            _input = value
            If _input Then
                _portDirection = MccDaq.DigitalPortDirection.DigitalIn
            Else
                _portDirection = MccDaq.DigitalPortDirection.DigitalOut
            End If
        End Set
    End Property

    Private _portNumber As Int32
    Private _portType As MccDaq.DigitalPortType = MccDaq.DigitalPortType.FirstPortA
    ''' <summary>Specifies the port number.</summary>
    ''' <value></value>
    ''' <remarks>Port numbers can be either 0 or 1 for ports A and B respectively.</remarks>
    Public Property PortNumber() As Int32
        Get
            Return _portNumber
        End Get
        Set(ByVal value As Int32)
            _portNumber = value
            Select Case _portNumber
                Case 1
                    Me._portType = MccDaq.DigitalPortType.FirstPortB
                Case Else
                    Me._portType = MccDaq.DigitalPortType.FirstPortA
            End Select
        End Set
    End Property

    Private _portValue As UInt16
    ''' <summary>Reads or writes a digital port value.</summary>
    ''' <value><c>PortValue</c> is an unsigned Int32 (16 bits) property</value>
    ''' <exception cref="isr.IO.Universal.IOException" guarantee="strong">
    '''   Failed attempting to output to an input port or input from and output port
    '''   or to set or get digital port.
    ''' </exception>
    ''' <remarks>Use this method to read or write from the digital port.  The device
    ''' digital port values are expressed as "T:UINT16" and converted to INT32 to preserve
    ''' CLS compliance.</remarks>
    ''' <remarks>
    ''' 	[david] 	4/21/2006	Change to Integer.
    ''' </para></remarks>
    Public Property PortValue() As Integer
        Get
            Return PortValueGetter()
        End Get
        Set(ByVal value As Integer)
            Me.PortValueSetter(value)
        End Set
    End Property

    ''' <summary>
    ''' Reads and returns a value from the digial port.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function PortValueGetter() As Integer
        If Me._input Then
            If _digitalDevice.IsDemo Then
                _digitalDevice.DeviceErrorInfo = New MccDaq.ErrorInfo
                Return 0
            Else
                _digitalDevice.DeviceErrorInfo = _digitalDevice.DaqDevice.DIn(Me._portType, Me._portValue)
                If _digitalDevice.DeviceErrorInfo.Value = MccDaq.ErrorInfo.ErrorCode.NoErrors Then
                    ' if no error return value
                    Return Convert.ToInt32(_portValue)
                Else
                    ' throw an exception
                    Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
                                           "{0} failed to get digital input.",
              Me.InstanceName)
                    Throw New isr.IO.Universal.IOException(Me.StatusMessage,
              _digitalDevice.DeviceErrorInfo.Value, _digitalDevice.DeviceErrorInfo.Message)
                End If
            End If
        Else
            ' if not input, raise an error
            Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
        "{0} failed attempting to input from an output port.",
          Me.InstanceName)
            Throw New isr.IO.Universal.IOException(Me.StatusMessage)
        End If
    End Function

    ''' <summary>
    ''' Writes a value to the digital port.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function PortValueSetter(ByVal value As Integer) As Integer
        If Me._input Then
            ' if not input, raise an error
            Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
        "{0} failed attempting to output to an input port.",
          Me.InstanceName)
            Throw New isr.IO.Universal.IOException(Me.StatusMessage)
        Else
            If _digitalDevice.IsDemo Then
                _digitalDevice.DeviceErrorInfo = New MccDaq.ErrorInfo
            Else
                _portValue = Convert.ToUInt16(value)
                _digitalDevice.DeviceErrorInfo = _digitalDevice.DaqDevice.DOut(Me._portType, Me._portValue)
                If _digitalDevice.DeviceErrorInfo.Value = MccDaq.ErrorInfo.ErrorCode.NoErrors Then
                Else
                    ' throw an exception
                    Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture,
              "{0} failed getting digital input.", Me.InstanceName)
                    Throw New isr.IO.Universal.IOException(Me.StatusMessage,
              _digitalDevice.DeviceErrorInfo.Value, _digitalDevice.DeviceErrorInfo.Message)
                End If
            End If
        End If
        Return _portValue
    End Function

#End Region

End Class

