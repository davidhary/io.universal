''' <summary>Handles Input Output exceptions.</summary>
''' <remarks> (c) 2003 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 09/12/03, 1.0.1350. Created </para></remarks>
<Serializable()> Public Class IOException
    Inherits DeviceExceptionBase

    Private Const _defMessage As String = "Universal device reported an IO exception."
    Private Const _messageFormat As String = "{0} The device reported error number {1}:: {2}"

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor specifying message and data to be set.</summary>
    ''' <param name="errorMessage">Specifies the exception message.</param>
    ''' <param name="deviceErrorCode">Specifies the error code from the device.</param>
    ''' <param name="deviceErrorMessage">Specifies the error message from the device.</param>
    Public Sub New(ByVal errorMessage As String,
    ByVal deviceErrorCode As Int64, ByVal deviceErrorMessage As String)
        MyBase.New(_messageFormat, errorMessage, deviceErrorCode, deviceErrorMessage)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo,
      ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class

''' <summary>Handles data conversion exceptions.</summary>
''' <remarks> (c) 2003 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 09/12/03, 1.0.1350. Created </para></remarks>
<Serializable()> Public Class CastException
    Inherits DeviceExceptionBase

    Private Const _defMessage As String = "Universal device failed converting a value to a new type."
    Private Const _messageFormat As String = "{0} The device reported error number {1}:: {2}"

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor specifying message and data to be set.</summary>
    ''' <param name="errorMessage">Specifies the exception message.</param>
    ''' <param name="deviceErrorCode">Specifies the error code from the device.</param>
    ''' <param name="deviceErrorMessage">Specifies the error message from the device.</param>
    Public Sub New(ByVal errorMessage As String,
    ByVal deviceErrorCode As Int64, ByVal deviceErrorMessage As String)
        MyBase.New(_messageFormat, errorMessage, deviceErrorCode, deviceErrorMessage)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo,
      ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class

''' <summary>Handles hardware not found exceptions.</summary>
''' <remarks> (c) 2003 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 05/22/04, 1.0.1603. Created </para></remarks>
<Serializable()> Public Class HardwareNotFoundException
    Inherits DeviceExceptionBase

    Private Const _defMessage As String = "Universal device failed to locate the hardware."
    Private Const _messageFormat As String = "{0} The device reported error number {1}:: {2}"

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor specifying message and data to be set.</summary>
    ''' <param name="errorMessage">Specifies the exception message.</param>
    ''' <param name="deviceErrorCode">Specifies the error code from the device.</param>
    ''' <param name="deviceErrorMessage">Specifies the error message from the device.</param>
    Public Sub New(ByVal errorMessage As String,
    ByVal deviceErrorCode As Int64, ByVal deviceErrorMessage As String)
        MyBase.New(_messageFormat, errorMessage, deviceErrorCode, deviceErrorMessage)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo,
      ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class
