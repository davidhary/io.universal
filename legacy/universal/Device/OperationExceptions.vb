''' <summary>Handles operation exceptions.</summary>
''' <remarks>Use this class to handle exceptions that might be thrown
'''   exercising open, close, hardware access, and other similar operations.(c) 2003 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 07/29/03, 1.0.1305. Created </para></remarks>
<Serializable()> Public Class OperationException
    Inherits isr.Core.ExceptionBase

    Private Const _defMessage As String = "Operation failed."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a new exception using the internal default message.</summary>
    ''' <remarks></remarks>
    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructs a new exception using the given message.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructs a new exception using the given message and inner exception.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Specifies the exception that was trapped for 
    '''   throwing this exception.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ' Protected constructor to de-serialize data
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo,
      ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>Overrides the GetObjectData method to serialize custom values.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    ''' <remarks> David, 08/23/03, 1.0.1333. Inherit operation exception. </para></remarks>
    <Security.Permissions.SecurityPermission(Security.Permissions.SecurityAction.Demand, SerializationFormatter:=True)>
    Public Overrides Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo,
      ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.GetObjectData(info, context)
    End Sub

#End Region

End Class

''' <summary>Handles open operation exceptions.</summary>
''' <remarks>Use this class to handle exceptions that might be thrown
'''   when attempting to open an operation such as a state machine.(c) 2003 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 07/29/03, 1.0.1305. Created
''' David, 08/23/03, 1.0.1333. Inherit operation exception
''' </para></remarks>
<Serializable()> Public Class OperationOpenException
    Inherits OperationException

    Private Const _DefMessage As String = "Operation failed opening."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of this class.
    ''' Uses the default message.
    ''' </summary>

    Public Sub New()
        MyBase.New(_DefMessage)
    End Sub

    ''' <summary>Constructs a new exception using the given message.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructs a new exception using the given message and inner exception.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Specifies the exception that was trapped for 
    '''   throwing this exception.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class

''' <summary>Handles close operation exceptions.</summary>
''' <remarks>Use this class to handle exceptions that might be thrown
'''   when attempting to close an operation such as a state machine.(c) 2003 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 07/29/03", 1.0.1305.x">
''' Created
''' 
''' David, 08/23/03, 1.0.1333. Inherit operation exception</para></remarks>
<Serializable()> Public Class OperationCloseException
    Inherits OperationException

    Private Const _DefMessage As String = "Operation failed closing."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of this class.
    ''' Uses the default message.
    ''' </summary>

    Public Sub New()
        MyBase.New(_DefMessage)
    End Sub

    ''' <summary>Constructs a new exception using the given message.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructs a new exception using the given message and inner exception.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Specifies the exception that was trapped for 
    '''   throwing this exception.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class

''' <summary>Handles initialize operation exception.</summary>
''' <remarks>Use this class to handle exceptions that might be thrown
'''   when attempting to initialize an operation.(c) 2003 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 07/29/03, 1.0.1305.Created 
''' David, 08/23/03, 1.0.1333. Inherit operation exception
''' </para></remarks>
<Serializable()> Public Class OperationInitializeException
    Inherits OperationException
    Private Const _defMessage As String = "Operation failed initializing."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of this class.
    ''' Uses the default message.
    ''' </summary>

    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructs a new exception using the given message.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructs a new exception using the given message and inner exception.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Specifies the exception that was trapped for 
    '''   throwing this exception.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class

''' <summary>Handles log-on operation exception.</summary>
''' <remarks>Use this class to handle log-on that might occur when attempting to login.(c) 2003 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 07/29/03, 1.0.1305. Created 
''' David, 08/23/03, 1.0.1333. Inherit operation exception </para></remarks>
<Serializable()> Public Class LogOnException
    Inherits OperationException

    Private Const _defMessage As String = "Failed to logOn."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of this class.
    ''' Uses the default message.
    ''' </summary>

    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructs a new exception using the given message.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructs a new exception using the given message and inner exception.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Specifies the exception that was trapped for 
    '''   throwing this exception.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class

''' <summary>Handles close operation exceptions.</summary>
''' <remarks>Use this class to throw exceptions for operations that were not
'''   implemented yet.(c) 2003 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 07/29/03, 1.0.1305. Created </para></remarks>
<Serializable()> Public Class NotImplementedException
    Inherits OperationException

    Private Const _defMessage As String = "Operation not implemented."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

      ''' <summary>
    ''' Initializes a new instance of this class.
    ''' Uses the default message.
    ''' </summary>

  Public Sub New()
    MyBase.New(_defMessage)
  End Sub

  ''' <summary>Constructs a new exception using the given message.</summary>
  ''' <param name="message">Specifies the exception message.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal message As String)
    MyBase.New(message)
  End Sub

  ''' <summary>Constructs a new exception using the given message and inner exception.</summary>
  ''' <param name="message">Specifies the exception message.</param>
  ''' <param name="innerException">Specifies the exception that was trapped for 
  '''   throwing this exception.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
    MyBase.New(message, innerException)
  End Sub

  ''' <summary>Constructor used for deserialization of the exception class.</summary>
  ''' <param name="info">Represents the SerializationInfo of the exception.</param>
  ''' <param name="context">Represents the context information of the exception.</param>
  Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
    MyBase.New(info, context)
  End Sub

#End Region

End Class

