## ISR IO Universal Libraries<sub>&trade;</sub>: Wrapper for Measurement Computing Universal Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*2.0.7095 06/05/19*  
Simplifies the assembly information.

*1.2.4232 08/03/11*  
Standardize code elements and documentation.

*1.2.4213 07/15/11*  
Simplifies the assembly information.

*1.2.2961 02/09/08*  
Update to .NET 3.5.

*1.1.2302 04/21/06*  
Upgrade to VS 2005.

*1.0.2219 01/29/06*  
Remove Visual Basic import and replace related
functions.

*1.0.2206 01/15/06*  
New support and exceptions libraries. Use INt32,
Int64, and Int16 instead of Integer, Long, and Short.

*1.0.1937 04/21/05 Rename and restructure. Needs to fix reference to
modified Core.

*1.0.1558 04/07/04*  
New dispose methods.

*1.0.1496 02/05/04*  
Use Core VB Library

*1.0.1350 09/12/03*  
Add Exceptions module and exception classes.

*1.0.1345 09/07/03*  
Recompile and test.

*1.0.1315 08/08/03*  
Add unit tests. Add method to adjust full range.

*1.0.1314 08/07/03*  
Initiate error handling. Add single bit and byte
digital I/O.

*1.0.1313 08/06/03*  
Rename from ULdaqOne and enforce guidelines

*1.0.1145 05/30/03*  
New library

\(C\) 2003 Integrated Scientific Resources, Inc.\
### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:


### Closed software  [](#){name=Closed-software}
Closed software used by this software is described and licensed on
the following sites:  
[Universal Library](https://mccdaq.com)
