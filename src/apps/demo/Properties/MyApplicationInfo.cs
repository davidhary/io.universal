﻿
namespace isr.io.UL.Demo.My
{
    internal partial class MyApplication
    {
        public const string AssemblyTitle = "Universal Library Switchboard";
        public const string AssemblyDescription = "Universal Library Switchboard";
        public const string AssemblyProduct = "Universal.Library.Switchboard";
    }
}