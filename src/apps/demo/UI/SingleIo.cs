using System;
using System.Reflection;
using System.Windows.Forms;

namespace isr.io.UL.Demo
{
    /// <summary>Includes code for form SingleIo, which provide test methods for single I/O.</summary>
    /// <remarks>Use this module to test single I/O operations of the board.(c) 2002 Integrated Scientific Resources, Inc.
    /// Licensed under The MIT License.<para>
    /// David, 12/05/02, 1.0.1069. Created </para><para>
    /// David, 05/30/03, 1.0.1245. Adapt to PMD-1208 </para></remarks>
    internal partial class SingleIo : Form
    {

        #region " CONSTRUCTORS  and  DESTRUCTORS "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks> David, 2022-01-17. </remarks>
        public SingleIo() : base()
        {
            Closed += this.Form_Closed;
            Closing += this.Form_Closing;
            Load += this.Form_Load;
            // This method is required by the Windows Form Designer.
            this.InitializeComponent();
        }

        // Form overrides dispose to clean up the component list.
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " METHODS "

        /// <summary>Displays this module.</summary>
        /// <returns>Returns dialog result.</returns>
        /// <remarks>Use this method to display this form.</remarks>
        internal new DialogResult ShowDialog()
        {

            // show the form
            return base.ShowDialog();
        }

        /// <summary>Initializes the user interface and tool tips.</summary>
        private void InitializeUserInterface()
        {
            this.tipsToolTip.SetToolTip( this.openDeviceCheckBox, "Depress to open the device or release to close." );
        }

        /// <summary>opens access to SignalLink.</summary>
        /// <remarks>Use this method to open the driver in real or demo modes</remarks>
        private void Open()
        {

            // the board number is a constant.  We should later get it from 
            // a configuration file.
            const int boardNumber = 1;
            try
            {

                // instantiate the class
                // Single I/O is only relevant for real mode
                this._DaqDevice = new Device( "Single I/O" ) { IsDemo = false };

                // open the driver
                if ( this.OpenDriver( boardNumber ) )
                {
                    this.analogInputGroupBox.Enabled = true;
                    this.analogOutputGroupBox.Enabled = true;
                    this.counterGroupBox.Enabled = true;
                    this.digitalIoGroupBox.Enabled = true;
                    this.ledCheckBox.Enabled = true;

                    // enable the timer
                    this.actionTimer.Enabled = true;
                }
                else
                {
                    // if failed opening, disable user interface 
                    this.analogInputGroupBox.Enabled = false;
                    this.analogOutputGroupBox.Enabled = false;
                    this.counterGroupBox.Enabled = false;
                    this.digitalIoGroupBox.Enabled = false;
                    this.ledCheckBox.Enabled = false;

                    // disable the timer
                    this.actionTimer.Enabled = false;
                }
            }

            // enable controls
            catch ( ApplicationException exn )
            {

                // add the message to the exception
                this._StatusMessage = "failed instantiating the MCC device driver";
                throw new ApplicationException( this._StatusMessage, exn );
            }
        }

        /// <summary>opens the SignalLink driver.</summary>
        /// <remarks>Use this method to open the driver</remarks>
        private bool OpenDriver( int boardNumber )
        {

            // use in finally to determine if we should complete the open process.
            bool failed = false;
            try
            {

                // open the driver trapping hardware not found exception
                _ = this._DaqDevice.Open( boardNumber );
            }
            catch ( HardwareNotFoundException hardwareExn )
            {

                // if failed opening try to open in demo mode
                if ( this._DaqDevice.IsDemo )
                {

                    // add the message to the exception
                    this._StatusMessage = "Failed opening the data acquisition device.";
                    failed = true;
                }
                else
                {

                    // if not demo, set to demo mode.
                    this._DaqDevice.IsDemo = true;
                    try
                    {

                        // try to open the driver
                        _ = this._DaqDevice.Open( boardNumber );

                        // add the message to message list
                        this._StatusMessage = "Failed opening device for data acquisition.  Reverting to demo mode.  Check if the device is connected.";
                        this.messagesTextBox.Text = this._StatusMessage;
                    }
                    catch ( ApplicationException exn )
                    {

                        // add the message to the exception
                        this._StatusMessage = "Failed opening the data acquisition device";
                        failed = true;
                    }
                }
            }
            finally
            {
                if ( failed )
                {
                }

                // if failed, do nothing.  we shall abort this one.

                else
                {

                    // return the firmware version
                    this.messagesTextBox.Text = "Open.  Software revision: " + Device.SoftwareRevision.ToString( System.Globalization.CultureInfo.CurrentCulture );

                    // instantiate two digital input channels
                    this._PortZero = new DigitalPort( "Port A", this._DaqDevice ) {
                        IsInput = true,
                        PortNumber = 0
                    };
                    this._PortZero.ConfigurePort();
                    this._PortOne = new DigitalPort( "Port B", this._DaqDevice ) {
                        IsInput = true,
                        PortNumber = 1
                    };
                    this._PortOne.ConfigurePort();

                    // instantiate four analog input channels
                    this._InputZero = new AnalogInput( "Channel A", this._DaqDevice );
                    this._InputOne = new AnalogInput( "Channel B", this._DaqDevice );
                    this._InputTwo = new AnalogInput( "Channel C", this._DaqDevice );
                    this._InputThree = new AnalogInput( "Channel D", this._DaqDevice );
                }
            }

            return !failed;
        }

        /// <summary>Closes and releases the data acquisition device driver.</summary>
        /// <remarks>Use this method to close and release the driver</remarks>
        private void CloseSignalIo()
        {
            try
            {

                // disable the timer
                this.actionTimer.Enabled = false;
                Application.DoEvents();

                // check if we have instantiated the class
                if ( this._DaqDevice is null )
                {
                    this.messagesTextBox.Text = "Warning.  Driver already closed.";
                }
                else
                {
                    // close the driver.
                    _ = this._DaqDevice.Close();
                }
            }
            catch ( ApplicationException exn )
            {

                // add the message to the exception
                string usrMessage;
                usrMessage = "Failed closing the data acquisition device driver";
                throw new ApplicationException( usrMessage, exn );
            }
            finally
            {

                // disable all group boxes
                this.analogInputGroupBox.Enabled = false;
                this.analogOutputGroupBox.Enabled = false;
                this.counterGroupBox.Enabled = false;
                this.digitalIoGroupBox.Enabled = false;
                this.ledCheckBox.Enabled = false;
            }
        }

        /// <summary>Initializes the class objects.</summary>
        /// <remarks>Called from the form load method to instantiate 
        ///   module-level objects.</remarks>
        private void InstantiateObjects()
        {

            // instantiate the board
            this._DaqDevice = new Device( "Single I/O" );

            // disable all group boxes
            this.analogInputGroupBox.Enabled = false;
            this.analogOutputGroupBox.Enabled = false;
            this.counterGroupBox.Enabled = false;
            this.digitalIoGroupBox.Enabled = false;
            this.ledCheckBox.Enabled = false;

            // set the channel and gains
            this.channelCheckBoxA.Items.Clear();
            this.channelCheckBoxA.DataSource = AnalogInput.Channels;
            this.channelCheckBoxA.SelectedIndex = 0;
            this.channelCheckBoxB.Items.Clear();
            this.channelCheckBoxB.DataSource = AnalogInput.Channels;
            this.channelCheckBoxB.SelectedIndex = 1;
            this.channelCheckBoxC.Items.Clear();
            this.channelCheckBoxC.DataSource = AnalogInput.Channels;
            this.channelCheckBoxC.SelectedIndex = 2;
            this.channelCheckBoxD.Items.Clear();
            this.channelCheckBoxD.DataSource = AnalogInput.Channels;
            this.channelCheckBoxD.SelectedIndex = 3;
            this.rangeCheckBoxA.Items.Clear();
            this.rangeCheckBoxA.DataSource = AnalogInput.Ranges;
            this.rangeCheckBoxA.SelectedIndex = 0;
            this.rangeCheckBoxB.Items.Clear();
            this.rangeCheckBoxB.DataSource = AnalogInput.Ranges;
            this.rangeCheckBoxB.SelectedIndex = 0;
            this.rangeCheckBoxC.Items.Clear();
            this.rangeCheckBoxC.DataSource = AnalogInput.Ranges;
            this.rangeCheckBoxC.SelectedIndex = 0;
            this.rangeCheckBoxD.Items.Clear();
            this.rangeCheckBoxD.DataSource = AnalogInput.Ranges;
            this.rangeCheckBoxD.SelectedIndex = 0;
        }

        #endregion

        #region " PROPERTIES "

        // module level SignalLink PMD-1208 board
        private Device _DaqDevice;

        // module level digital ports
        private DigitalPort _PortOne;
        private DigitalPort _PortZero;

        // module level analog input channel
        private AnalogInput _InputZero;
        private AnalogInput _InputOne;
        private AnalogInput _InputTwo;
        private AnalogInput _InputThree;
        private string _StatusMessage = string.Empty;

        #endregion

        #region " FORM EVENT HANDLERS "

        /// <summary>Occurs after the form is closed.</summary>
        /// <param name="sender"><see cref="Object"/> instance of this 
        ///   <see cref="System.Windows.Forms.Form"/></param>
        /// <param name="e"><see cref="System.EventArgs"/></param>
        /// <remarks>This event is a notification that the form has already gone away before
        /// control is returned to the calling method (in case of a modal form).  Use this
        /// method to delete any temporary files that were created or dispose of any objects
        /// not disposed with the closing event.
        /// </remarks>
        private void Form_Closed( object sender, EventArgs e )
        {
        }

        /// <summary>Occurs before the form is closed</summary>
        /// <param name="sender"><see cref="Object"/> instance of this 
        ///   <see cref="System.Windows.Forms.Form"/></param>
        /// <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
        /// <remarks>Use this method to optionally cancel the closing of the form.
        /// Because the form is not yet closed at this point, this is also the best 
        /// place to serialize a form's visible properties, such as size and 
        /// location. Finally, dispose of any form level objects especially those that
        /// might needs access to the form and thus should not be terminated after the
        /// form closed.
        /// </remarks>
        private void Form_Closing( object sender, System.ComponentModel.CancelEventArgs e )
        {

            // disable the timer if any
            // actionTimer.Enabled = False
            Application.DoEvents();

            // set module objects that reference other objects to Nothing

            // terminate form objects
            this.TerminateObjects();
        }

        /// <summary>Terminates and disposes of class-level objects.</summary>
        /// <remarks>Called from the form Closing method.</remarks>
        private void TerminateObjects()
        {

            // disable all group boxes
            this.analogInputGroupBox.Enabled = false;
            this.analogOutputGroupBox.Enabled = false;
            this.counterGroupBox.Enabled = false;
            this.digitalIoGroupBox.Enabled = false;
            this.ledCheckBox.Enabled = false;

            // disable the timer
            this.actionTimer.Enabled = false;
            Application.DoEvents();

            // terminate module objects 
            if ( this._PortOne is object )
            {
                this._PortOne.Dispose();
            }

            if ( this._PortZero is object )
            {
                this._PortZero.Dispose();
            }

            if ( this._InputZero is object )
            {
                this._InputZero.Dispose();
            }

            if ( this._InputOne is object )
            {
                this._InputOne.Dispose();
            }

            if ( this._InputTwo is object )
            {
                this._InputTwo.Dispose();
            }

            if ( this._InputThree is object )
            {
                this._InputThree.Dispose();
            }

            if ( this._DaqDevice is object )
            {

                // close the board
                this.CloseSignalIo();
                this._DaqDevice.Dispose();
                this._DaqDevice = null;
            }
        }

        /// <summary>Occurs when the form is loaded.</summary>
        /// <param name="sender"><see cref="Object"/> instance of this 
        ///   <see cref="System.Windows.Forms.Form"/></param>
        /// <param name="e"><see cref="System.EventArgs"/></param>
        /// <remarks>Use this method for doing any final initialization right before 
        ///   the form is shown.  This is a good place to change the Visible and
        ///   ShowInTaskbar properties to start the form as hidden.  
        ///   Starting a form as hidden is useful for forms that need to be running but that
        ///   should not show themselves right away, such as forms with a notify icon in the
        ///   task bar.</remarks>
        private void Form_Load( object sender, EventArgs e )
        {
            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;

                // instantiate form objects
                this.InstantiateObjects();

                // set the form caption
                this.Text = Assembly.GetExecutingAssembly().BuildProductTimeCaption() + ": SINGLE I/O PANEL";

                // set tool tips
                this.InitializeUserInterface();

                // center the form
                this.CenterToScreen();
            }

            // turn on the loaded flag
            // loaded = True

            catch
            {

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        private void OpenDeviceCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( this.openDeviceCheckBox.Checked )
            {
                // open the daq board
                this.Open();
                // set the caption to close
                this.openDeviceCheckBox.Text = "Cl&ose Device";
            }
            else
            {
                // close the daq board
                this.CloseSignalIo();
                // set the caption to open
                this.openDeviceCheckBox.Text = "&Open Device";
            }
        }

        /// <summary>Occurs upon timer events.</summary>
        /// <remarks>Use this method to execute all timer actions.</remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void ActionTimer_Tick( object sender, EventArgs e )
        {
            try
            {
                this._InputZero.ChannelNumber = int.Parse( this.channelCheckBoxA.Text, System.Globalization.CultureInfo.CurrentCulture );
                this._InputZero.Gain = Convert.ToInt32( AnalogInput.Gains[Convert.ToInt16( this.rangeCheckBoxA.SelectedIndex, System.Globalization.CultureInfo.CurrentCulture )] );
                this._InputZero.Acquire();
                this.voltsTextBoxA.Text = this._InputZero.Voltage.ToString( "F4", System.Globalization.CultureInfo.CurrentCulture );
                this._InputOne.ChannelNumber = int.Parse( this.channelCheckBoxB.Text, System.Globalization.CultureInfo.CurrentCulture );
                this._InputOne.Gain = Convert.ToInt32( AnalogInput.Gains[Convert.ToInt16( this.rangeCheckBoxB.SelectedIndex, System.Globalization.CultureInfo.CurrentCulture )] );
                this._InputOne.Acquire();
                this.voltsTextBoxB.Text = this._InputOne.Voltage.ToString( "F4", System.Globalization.CultureInfo.CurrentCulture );
                this._InputTwo.ChannelNumber = int.Parse( this.channelCheckBoxC.Text, System.Globalization.CultureInfo.CurrentCulture );
                this._InputTwo.Gain = Convert.ToInt32( AnalogInput.Gains[Convert.ToInt16( this.rangeCheckBoxC.SelectedIndex, System.Globalization.CultureInfo.CurrentCulture )] );
                this._InputTwo.Acquire();
                this.voltsTextBoxC.Text = this._InputTwo.Voltage.ToString( "F4", System.Globalization.CultureInfo.CurrentCulture );
                this._InputThree.ChannelNumber = int.Parse( this.channelCheckBoxD.Text, System.Globalization.CultureInfo.CurrentCulture );
                this._InputThree.Gain = Convert.ToInt32( AnalogInput.Gains[Convert.ToInt16( this.rangeCheckBoxD.SelectedIndex, System.Globalization.CultureInfo.CurrentCulture )] );
                this._InputThree.Acquire();
                this.voltsTextBoxD.Text = this._InputThree.Voltage.ToString( "F4", System.Globalization.CultureInfo.CurrentCulture );
                if ( this.ledCheckBox.Checked )
                {
                    this.ledCheckBox.Checked = false;
                    this.messagesTextBox.Text = this._DaqDevice.DoIdentify();
                }

                if ( this.portZeroInputCheckBox.Checked ^ this._PortZero.IsInput )
                {
                    this._PortZero.IsInput = this.portZeroInputCheckBox.Checked;
                    this._PortZero.ConfigurePort();
                    this.portZeroSetCheckBox.Checked = false;
                    this.portZeroSetCheckBox.Enabled = !this.portZeroInputCheckBox.Checked;
                    this.portZeroReadCheckBox.Checked = false;
                    this.portZeroReadCheckBox.Enabled = this.portZeroInputCheckBox.Checked;
                }

                if ( this.portZeroSetCheckBox.Checked )
                {
                    this._PortZero.PortValue = ( short ) Math.Round( this.portZeroNumericUpDown.Value );
                }

                if ( this.portZeroReadCheckBox.Checked )
                {
                    this.portZeroNumericUpDown.Value = Convert.ToDecimal( this._PortZero.PortValue, System.Globalization.CultureInfo.CurrentCulture );
                }

                if ( this.portOneInputCheckBox.Checked ^ this._PortOne.IsInput )
                {
                    this._PortOne.IsInput = this.portOneInputCheckBox.Checked;
                    this._PortOne.ConfigurePort();
                    this.portOneSetCheckBox.Checked = false;
                    this.portOneSetCheckBox.Enabled = !this.portOneInputCheckBox.Checked;
                    this.portOneReadCheckBox.Checked = false;
                    this.portOneReadCheckBox.Enabled = this.portOneInputCheckBox.Checked;
                    this.portOneReadCheckBox.Refresh();
                }

                if ( this.portOneSetCheckBox.Checked )
                {
                    this._PortOne.PortValue = ( short ) Math.Round( this.portOneNumericUpDown.Value );
                }

                if ( this.portOneReadCheckBox.Checked )
                {
                    this.portOneNumericUpDown.Value = Convert.ToDecimal( this._PortOne.PortValue, System.Globalization.CultureInfo.CurrentCulture );
                }
            }
            catch ( Exception )
            {
                this.actionTimer.Enabled = false;
                this.messagesTextBox.Text = this._DaqDevice.DeviceErrorMessage;
                this.openDeviceCheckBox.Checked = false;
            }
        }

        #endregion

    }
}
