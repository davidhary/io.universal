using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace isr.io.UL.Demo
{
    public partial class SwitchboardForm : Form
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _ToolTip = new ToolTip(components);
            _OpenButton = new Button();
            _OpenButton.Click += new EventHandler(OpenButton_Click);
            _ExitButton = new Button();
            _ExitButton.Click += new EventHandler(ExitButton_Click);
            _DialogsComboBox = new ComboBox();
            SuspendLayout();
            // 
            // _OpenButton
            // 
            _OpenButton.Location = new Point(385, 12);
            _OpenButton.Name = "_OpenButton";
            _OpenButton.Size = new Size(58, 24);
            _OpenButton.TabIndex = 11;
            _OpenButton.Text = "&Open...";
            _ToolTip.SetToolTip(_OpenButton, "Opens the selected dialog.");
            // 
            // _ExitButton
            // 
            _ExitButton.Location = new Point(381, 52);
            _ExitButton.Name = "_ExitButton";
            _ExitButton.Size = new Size(62, 23);
            _ExitButton.TabIndex = 10;
            _ExitButton.Text = "E&xit";
            // 
            // _DialogsComboBox
            // 
            _DialogsComboBox.BackColor = SystemColors.Window;
            _DialogsComboBox.Cursor = Cursors.Default;
            _DialogsComboBox.Font = new Font("Arial", 12.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _DialogsComboBox.ForeColor = SystemColors.WindowText;
            _DialogsComboBox.Location = new Point(17, 12);
            _DialogsComboBox.Name = "_DialogsComboBox";
            _DialogsComboBox.RightToLeft = RightToLeft.No;
            _DialogsComboBox.Size = new Size(361, 27);
            _DialogsComboBox.TabIndex = 9;
            _DialogsComboBox.Text = "Select Dialog from the list";
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(458, 91);
            Controls.Add(_OpenButton);
            Controls.Add(_ExitButton);
            Controls.Add(_DialogsComboBox);
            Name = "Form1";
            Text = "Form1";
            Load += new EventHandler(Form_Load);
            ResumeLayout(false);
        }

        private ToolTip _ToolTip;
        private Button _OpenButton;
        private Button _ExitButton;
        private ComboBox _DialogsComboBox;
    }
}
