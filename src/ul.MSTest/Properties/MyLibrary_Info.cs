
namespace isr.io.UL.MStest.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    public sealed partial class MyLibrary
    {

        /// <summary> Constructor that prevents a default instance of this class from being created. </summary>
        private MyLibrary() : base()
        {
        }

        internal const string AssemblyTitle = "Universal Library Tests";
        internal const string AssemblyDescription = "Unit Tests for the Universal Library";
        internal const string AssemblyProduct = "isr.io.UL.Library.Tests";
    }
}
