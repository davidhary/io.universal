﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.io.UL.MStest
{
    /// <summary> An analog input tests. </summary>
    /// <remarks> David, 2022-01-17. </remarks>
    [TestClass()]
    public class AnalogInputTests
    {

        #region " TEST CONSTRUCTION "

        /// <summary> My class initialize. </summary>
        /// <remarks> David, 2022-01-17. </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            TestContext = testContext;
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> David, 2022-01-17. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
        }

        /// <summary> My test initialize. </summary>
        /// <remarks> David, 2022-01-17. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            try
            {

                // instantiate the prover bank
                // instantiate the class
                this._Device = new Device( "Single I/O" );

                // open the driver trapping hardware not found exception
                _ = this._Device.Open( this._BoardNumber );

                // instantiate an analog input channel
                this._AnalogInput = new AnalogInput( "Analog Input", this._Device );
            }
            catch ( Exception ex )
            {

                // close to meet strong guarantees
                try
                {
                    this.MyTestCleanup();
                }
                finally
                {
                }

                // throw an exception
                throw new OperationOpenException( $"{nameof( AnalogInputTests )} failed opening", ex );
            }
        }

        /// <summary>Closes the instance.</summary>
        /// <remarks>Use this method to close the instance.  The method is class as the 
        ///   TearDown method of the tester.</remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            try
            {

                // check if we have instantiated the class
                if ( this._Device is object )
                {
                    // close the driver.
                    _ = this._Device.Close();
                }
            }
            catch ( Exception ex )
            {

                // throw an exception
                throw new OperationException( $"{nameof( AnalogInputTests )} failed closing", ex );
            }
        }

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        [CLSCompliant( false )]
        public static TestContext TestContext { get; set; }


        #endregion

        /// <summary> (Unit Test Method) tests analog gain. </summary>
        /// <remarks> David, 2022-01-17. </remarks>
        [TestMethod()]
        public void AnalogGainTest()
        {
            const float gainRangeTwo = 2f;
            const float gainRangeTen = 10f;
            const float gainRangeTwenty = 20f;
            const float deltaRange = 0.2f;
            float maxVoltage;
            maxVoltage = gainRangeTwo;
            _ = this._AnalogInput.AdjustRange( maxVoltage - 0.1f );
            Assert.AreEqual( maxVoltage, this._AnalogInput.AnalogInputRange.Max, deltaRange, "2 Volts Range" );
            maxVoltage = gainRangeTwenty;
            _ = this._AnalogInput.AdjustRange( maxVoltage );
            Assert.AreEqual( maxVoltage, this._AnalogInput.AnalogInputRange.Max, deltaRange, "10 Volts Range" );
            maxVoltage = gainRangeTen;
            _ = this._AnalogInput.AdjustRange( maxVoltage );
            Assert.AreEqual( maxVoltage, this._AnalogInput.AnalogInputRange.Max, deltaRange, "20 Volts Range" );
        }

        /// <summary> (Unit Test Method) tests measure voltage. </summary>
        /// <remarks> David, 2022-01-17. </remarks>
        [TestMethod()]
        public void MeasureVoltageTest()
        {
            const float expectedVoltage = 5.0f;
            const float deltaVoltage = 0.5f;
            _ = this._AnalogInput.AdjustOptimalRange( 2f * expectedVoltage );
            this._AnalogInput.Acquire();
            Assert.AreEqual( expectedVoltage, this._AnalogInput.Voltage, deltaVoltage, "Input Voltage" );
        }

        private Device _Device;
        private AnalogInput _AnalogInput;
        private readonly int _BoardNumber = 1;
    }
}