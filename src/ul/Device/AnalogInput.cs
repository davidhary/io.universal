using System;
using System.Collections;

namespace isr.io.UL
{
    /// <summary>Defines an analog input channel.</summary>
    /// <remarks> (c) 2003 Integrated Scientific Resources, Inc.
    /// Licensed under The MIT License.</para><para>
    /// David, 08/07/03, 1.0.1314. Created </para></remarks>
    public class AnalogInput : IDisposable
    {

        #region " CONSTRUCTORS  and  DESTRUCTORS "

        /// <summary>Constructs this class.</summary>
        /// <param name="instanceName">Specifies the name of the instance.</param>
        /// <remarks>Use this constructor to instantiate this class
        ///   and set the instance name, which is useful in tracing</remarks>

        // instantiate the base class
        public AnalogInput( string instanceName ) : base()
        {
            this._InstanceName = instanceName;
        }

        /// <summary>Constructs this class.</summary>
        /// <param name="instanceName">Specifies the name of the instance.</param>
        /// <param name="deviceLink">
        ///   is an Object expression that specifies the signal link driver for accessing
        ///   the device.</param>
        /// <remarks>Use this constructor to instantiate this class
        ///   and set the instance name, which is useful in tracing</remarks>
        public AnalogInput( string instanceName, Device deviceLink ) : this( instanceName )
        {
            this._AnalogDevice = deviceLink;
        }

        /// <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        /// <remarks>Do not make this method overridable (virtual) because a derived 
        ///   class should not be able to override this method.</remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary>Gets or sets the dispose status sentinel.</summary>
        private bool _Disposed;
        /// <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        /// <param name="disposing">True if this method releases both managed and unmanaged 
        ///   resources; False if this method releases only unmanaged resources.</param>
        /// <remarks>Executes in two distinct scenarios as determined by
        ///   its disposing parameter.  If True, the method has been called directly or 
        ///   indirectly by a user's code--managed and unmanaged resources can be disposed.
        ///   If disposing equals False, the method has been called by the 
        ///   runtime from inside the finalizer and you should not reference 
        ///   other objects--only unmanaged resources can be disposed.</remarks>
        protected virtual void Dispose( bool disposing )
        {
            if ( !this._Disposed )
            {
                if ( disposing )
                {

                    // Free managed resources when explicitly called
                    this.StatusMessage = string.Empty;
                    this._InstanceName = string.Empty;
                    if ( this._AnalogDevice is object )
                    {
                        this._AnalogDevice.Dispose();
                    }
                }

                // Free shared unmanaged resources

            }

            // set the sentinel indicating that the class was disposed.
            this._Disposed = true;
        }

        /// <summary>This destructor will run only if the Dispose method 
        ///   does not get called. It gives the base class the opportunity to 
        ///   finalize. Do not provide destructors in types derived from this class.</summary>
        ~AnalogInput()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary>Overrides ToString returning the instance name if not empty.</summary>
        /// <remarks>Use this method to return the instance name. If instance name is not set, 
        ///   returns the base class ToString value.</remarks>
        public override string ToString()
        {
            return string.IsNullOrEmpty( this._InstanceName ) ? base.ToString() : this._InstanceName;
        }

        private string _InstanceName = string.Empty;
        /// <summary>Gets or sets the name given to an instance of this class.</summary>
        /// <value><c>InstanceName</c> is a String property.</value>
        /// <remarks> David, 11/23/04, 1.0.1788.
        ///   Correct code no to get instance name from .ToString but from MyBase.ToString
        ///   so that calling this method from the child class will not break the rule of
        ///   calling overridable methods from the constructor.
        /// </para></remarks>
        public string InstanceName
        {
            get => !string.IsNullOrEmpty( this._InstanceName ) ? this._InstanceName : base.ToString();

            set => this._InstanceName = value;
        }

        /// <summary>Gets or sets the status message.</summary>
        /// <value>A <see cref="String">String</see>.</value>
        public string StatusMessage { get; set; } = string.Empty;

        #endregion

        #region " METHODS "

        /// <summary>Configures the analog input.</summary>
        /// <exception cref="isr.io.UL.OperationException" guarantee="strong"></exception>
        public void ConfigureInput()
        {
            if ( this._AnalogDevice.IsDemo )
            {
                this._AnalogDevice.DeviceErrorInfo = new MccDaq.ErrorInfo();
            }
            else
            {
                // configure the port.
                // _analogDevice.DeviceErrorInfo = _
                // _analogDevice.DaqDevice.DConfigPort(_portType.FirstPortA, _portDirection)
            }

            if ( this._AnalogDevice.DeviceErrorInfo.Value == MccDaq.ErrorInfo.ErrorCode.NoErrors )
            {
            }
            else
            {
                // throw an exception
                this.StatusMessage = $"{this.InstanceName} failed to configure analog input";
                throw new IOException( this.StatusMessage, ( long ) this._AnalogDevice.DeviceErrorInfo.Value, this._AnalogDevice.DeviceErrorInfo.Message );
            }
        }

        /// <summary>Increments the analog input gain.</summary>
        /// <remarks>Use this method to increment the analog input gains.</remarks>
        public float IncrementGain()
        {
            // select a new gain
            switch ( this._Gain )
            {
                case var @case when @case >= 20:
                    {
                        this._Gain = 20;
                        break;
                    }

                case var case1 when case1 >= 16:
                    {
                        this._Gain = 20;
                        break;
                    }

                case var case2 when case2 >= 10:
                    {
                        this._Gain = 16;
                        break;
                    }

                case var case3 when case3 >= 8:
                    {
                        this._Gain = 10;
                        break;
                    }

                case var case4 when case4 >= 5:
                    {
                        this._Gain = 8;
                        break;
                    }

                case var case5 when case5 >= 4:
                    {
                        this._Gain = 8;
                        break;
                    }

                case var case6 when case6 >= 2:
                    {
                        this._Gain = 4;
                        break;
                    }

                case var case7 when case7 >= 1:
                    {
                        this._Gain = 2;
                        break;
                    }

                default:
                    {
                        this._Gain = 1;
                        break;
                    }
            }
            // set the new gain
            this.Gain = this._Gain;
            return this.Gain;
        }

        /// <summary>Decrements the analog input gain.</summary>
        /// <remarks>Use this method to decrement the analog input gains.</remarks>
        public float DecrementGain()
        {
            // select a new gain
            switch ( this._Gain )
            {
                case var @case when @case >= 20:
                    {
                        this._Gain = 16;
                        break;
                    }

                case var case1 when case1 >= 16:
                    {
                        this._Gain = 10;
                        break;
                    }

                case var case2 when case2 >= 10:
                    {
                        this._Gain = 8;
                        break;
                    }

                case var case3 when case3 >= 8:
                    {
                        this._Gain = 5;
                        break;
                    }

                case var case4 when case4 >= 5:
                    {
                        this._Gain = 4;
                        break;
                    }

                case var case5 when case5 >= 4:
                    {
                        this._Gain = 2;
                        break;
                    }

                case var case6 when case6 >= 2:
                    {
                        this._Gain = 1;
                        break;
                    }

                case var case7 when case7 >= 1:
                    {
                        this._Gain = 1;
                        break;
                    }

                default:
                    {
                        this._Gain = 1;
                        break;
                    }
            }
            // set the new gain
            this.Gain = this._Gain;
            return this.Gain;
        }

        private readonly Random _RandomAnalogInput = new();
        /// <summary>Samples a single channel at a specific gain.</summary>
        /// <exception cref="isr.io.UL.IOException" guarantee="strong"></exception>
        /// <remarks>Use this method to get a single sample from the input channel.</remarks>
        public void Acquire()
        {
            if ( this._AnalogDevice.IsDemo )
            {
                this._Voltage = 0.9f * this._Voltage + 0.01f * Convert.ToSingle( this._RandomAnalogInput.NextDouble() - 0.5d ) * this.AnalogInputRange.Max;
                this._AnalogDevice.DeviceErrorInfo = new MccDaq.ErrorInfo();
            }
            else
            {
                // save previous data value for overflow test
                this._DataValuePrev = this._DataValue;
                // read an analog input
                this._AnalogDevice.DeviceErrorInfo = this._AnalogDevice.DaqDevice.AIn( this.ChannelNumber, this._RangeCode, out this._DataValue );
            }

            if ( this._AnalogDevice.DeviceErrorInfo.Value == MccDaq.ErrorInfo.ErrorCode.NoErrors )
            {
                // if no error convert
                this._AnalogDevice.DeviceErrorInfo = this._AnalogDevice.DaqDevice.ToEngUnits( this._RangeCode, this._DataValue, out this._Voltage );
            }
            else
            {
                // throw an exception
                this.StatusMessage = $"{this.InstanceName} failed getting analog input";
                throw new IOException( this.StatusMessage, ( long ) this._AnalogDevice.DeviceErrorInfo.Value, this._AnalogDevice.DeviceErrorInfo.Message );
            }
        }

        /// <summary>Sets the offset voltage correction to the current voltage.</summary>
        /// <remarks>Use this method to set the internal voltage offset value used to correct
        ///   the analog input voltage for offset.</remarks>
        public void UpdateVoltageOffset()
        {
            this._VoltageOffset = this._Voltage;
        }

        /// <summary>Adjust the gain to fit the voltage within the device range.</summary>
        /// <returns>A Boolean data type</returns>
        /// <param name="Voltage">
        ///   is a Single expression that specifies the input voltage to check for range.</param>
        public bool AdjustRange( float voltage )
        {
            bool rangeChanged = false;
            // set gain to maximum
            this.Gain = this._GainMax;
            while ( !(this.InRangeInternal( voltage ) | this.Gain == this._GainMin) )
            {
                // if not in range, 
                _ = this.DecrementGain();
                rangeChanged = true;
            }

            return rangeChanged;
        }

        /// <summary>updates the range to fit within the optimal ranges.</summary>
        /// <returns>A Boolean data type</returns>
        /// <param name="Voltage">
        ///   is a Single expression that specifies the input voltage to check for range.</param>
        /// <remarks>Call this method to adjust the analog input gain to fit the specified 
        ///   voltage within the optimal gain.</remarks>
        public bool AdjustOptimalRange( float voltage )
        {
            bool rangeChanged = false;
            while ( !this.InOptimalRangeInternal( voltage ) )
            {
                if ( this.InHigherGainRangeInternal( voltage ) )
                {
                    // if in higher gain, increment the gain
                    _ = this.IncrementGain();
                    rangeChanged = true;
                }
                else if ( this.InLowerGainRangeInternal( voltage ) )
                {
                    // if in lower gain range, decrement the gain
                    _ = this.DecrementGain();
                    rangeChanged = true;
                }
            }

            return rangeChanged;
        }

        #endregion

        #region " SHARED PROPERTIES "

        /// <summary>Gets or sets the list Analog Input Channels.</summary>
        /// <value><c>Channels</c> is an ArrayList property.</value>
        public static ArrayList Channels
        {
            get {
                var channelList = new ArrayList();
                channelList.AddRange( new object[] { 0, 1, 2, 3 } );
                return channelList;
            }
        }

        /// <summary>Lists the Analog Input Gains.</summary>
        /// <value><c>Gains</c> is an ArrayList property.</value>
        public static ArrayList Gains
        {
            get {
                var gainList = new ArrayList();
                gainList.AddRange( new object[] { 1, 2, 4, 5, 8, 10, 16, 20 } );
                return gainList;
            }
        }

        /// <summary>Lists the Analog Input Ranges.</summary>
        /// <value><c>Ranges</c> is an ArrayList property.</value>
        public static ArrayList Ranges
        {
            get {
                var rangeList = new ArrayList();
                rangeList.AddRange( new object[] { "±20", "±10", "±5", "±4", "±2.5", "±2", "±1.25", "±1" } );
                return rangeList;
            }
        }

        #endregion

        #region " PROPERTIES "

        private SignalRange _RelativeGainRange = new( ( float ) -9.5d, 9.5f );
        private SignalRange _HigherGainRange = new( ( float ) -2.5d, 2.5f );
        private readonly Device _AnalogDevice;
        /// <summary>Gets or sets the analog input channel code.</summary>
        /// <value><c>ChannelNumber</c> is an Int32 property.</value>
        /// <remarks>Use this property to get or set the analog input channel.</remarks>
        public int ChannelNumber { get; set; }

        private readonly int _GainMax = 20;
        private readonly int _GainMin = 1;
        private int _Gain = 1;
        private int _GainNextHigher = 2;
        private MccDaq.Range _RangeCode = MccDaq.Range.Bip10Volts;
        /// <summary>Gets or sets the analog input gain.</summary>
        /// <value><c>Gain</c> is an Int32 property.</value>
        /// <remarks>Use this property to get or set the analog input gain which to use for single 
        ///   analog inputs when calling the Read method.  This property also sets the
        ///   analog input gain code (GainCode).</remarks>
        public int Gain
        {
            get => this._Gain;

            set {
                // set the gain code based on the gain value
                switch ( value )
                {
                    case var @case when @case >= 20:
                        {
                            this._RangeCode = MccDaq.Range.Bip1Volts;
                            this._Gain = 20;
                            this._GainNextHigher = 20;
                            break;
                        }

                    case var case1 when case1 >= 16:
                        {
                            this._RangeCode = MccDaq.Range.Bip1Pt25Volts;
                            this._Gain = 16;
                            this._GainNextHigher = 20;
                            break;
                        }

                    case var case2 when case2 >= 10:
                        {
                            this._RangeCode = MccDaq.Range.Bip2Volts;
                            this._Gain = 10;
                            this._GainNextHigher = 16;
                            break;
                        }

                    case var case3 when case3 >= 8:
                        {
                            this._RangeCode = MccDaq.Range.Bip2Pt5Volts;
                            this._Gain = 8;
                            this._GainNextHigher = 10;
                            break;
                        }

                    case var case4 when case4 >= 5:
                        {
                            this._RangeCode = MccDaq.Range.Bip4Volts;
                            this._Gain = 5;
                            this._GainNextHigher = 8;
                            break;
                        }

                    case var case5 when case5 >= 4:
                        {
                            this._RangeCode = MccDaq.Range.Bip5Volts;
                            this._Gain = 4;
                            this._GainNextHigher = 8;
                            break;
                        }

                    case var case6 when case6 >= 2:
                        {
                            this._RangeCode = MccDaq.Range.Bip10Volts;
                            this._Gain = 2;
                            this._GainNextHigher = 4;
                            break;
                        }

                    case var case7 when case7 >= 1:
                        {
                            this._RangeCode = MccDaq.Range.Bip20Volts;
                            this._Gain = 1;
                            this._GainNextHigher = 2;
                            break;
                        }

                    default:
                        {
                            this._RangeCode = MccDaq.Range.Bip20Volts;
                            this._Gain = 1;
                            this._GainNextHigher = 2;
                            break;
                        }
                }
                // set the analog input range based on the new range code
                _ = this.SetRangesInternal();
            }
        }

        private readonly int _DataValueMin = 0;
        private readonly int _DataValueMax = 4095;
        private int _DataValuePrev = 1;
        private short _DataValue;
        /// <summary>Gets or sets the analog input data value.</summary>
        /// <value><c>DataValue</c> is a Unsigned 16 Bit Int32 property that can be read from (read only).</value>
        /// <remarks>Use this property to get the analog input data value read using GetAnalogInput.</remarks>
        public int DataValue => this._DataValue;

        private float _Voltage;
        /// <summary>Gets or sets the analog input voltage.</summary>
        /// <value><c>Voltage</c> is a Single-Precision property that can be read from (read only).</value>
        /// <remarks>Use this property to get analog input voltage read using GetAnalogInput.</remarks>
        public float Voltage => this._Voltage;

        private float _VoltageOffset;
        /// <summary>Gets or sets the analog input voltage corrected for offset.</summary>
        /// <value><c>VoltageCorrected</c> is a Single-Precision property that can be read from (read only).</value>
        /// <remarks>Use this property to get analog input voltage read using Read 
        ///   and corrected for offset as set in UpdateVoltageOffset.</remarks>
        public float VoltageCorrected => this.IsInvert ? this._VoltageOffset - this._Voltage : this._Voltage - this._VoltageOffset;

        private SignalRange _AnalogInputRange = new( -10, 10f );
        /// <summary>Gets or sets the analog input range for the prescribed gain.</summary>
        /// <value><c>AnalogInputRange</c> is a SignalRange property that can be read from (read only).</value>
        /// <remarks>Use this property to get the maximum, minimum, or range values of the
        ///   analog input voltage for the range settings for the specified channel and gain.</remarks>
        public SignalRange AnalogInputRange => this._AnalogInputRange;

        /// <summary>Gets or sets the analog input relative range.</summary>
        /// <value><c>AnalogInputAutoRangeLimen</c> is a Single property.</value>
        /// <remarks>Use this property to get or set the range within the analog input 
        ///   range outside of which auto ranging kicks in.</remarks>
        public float AnalogInputAutoRangeLimen { get; set; } = 0.95f;

        /// <summary>Gets or sets the invert status of the analog input.</summary>
        /// <value><c>IsInvert</c> is a Boolean property.</value>
        /// <remarks>Use this property to turn on or off inversion of the input voltage when calculating the corrected
        ///   input voltage</remarks>
        public bool IsInvert { get; set; }

        /// <summary>Gets or sets the over voltage outcome value.</summary>
        /// <value><c>IsOverVoltage</c> is a <see cref="Boolean"/> property that can be read from (read only).</value>
        /// <remarks>Use this property to get over voltage outcome of the last reading.</remarks>
        /// <remarks> David, 05/30/03, 1.0.1245. Adapt for PMD-1208. </para></remarks>
        /// <remarks> David, 05/30/03, 1.0.1123. Use range limits </para></remarks>
        public bool IsOverVoltage => this._Voltage >= this._AnalogInputRange.High | this._Voltage <= this._AnalogInputRange.Low;

        /// <summary>Gets or sets the overflow outcome value.</summary>
        /// <value><c>Overflowed</c> is a <see cref="Boolean"/> property that can be read from (read only).</value>
        /// <remarks>Use this property to test if the last two data values hit the A/D range limits.</remarks>
        public bool Overflowed => this.DataValueOverflowedInternal( this._DataValue ) & this.DataValueOverflowedInternal( this._DataValuePrev );

        /// <summary>Gets or sets the single-ended attribute of the input channel.</summary>
        /// <value><c>SingleEnded</c> is a <see cref="Boolean"/> property that can be read from (read only).</value>
        /// <remarks>Use this property to determine if the specified input channel is single ended.</remarks>
        public bool SingleEnded { get; private set; }

        #endregion

        #region " PRIVATE  and  PROTECTED "

        /// <summary>Sets the analog input ranges.</summary>
        /// <exception cref="isr.io.UL.OperationException" guarantee="strong">
        ///   Failed to set ranges.</exception>
        /// <remarks>Call this method from the class properties setting gain or polarity
        ///   to set the ranges for such values.</remarks>
        private bool SetRangesInternal()
        {
            try
            {
                this._AnalogInputRange = this.SingleEnded ? new SignalRange( 20f, 1f, true ) : new SignalRange( 40f, this._Gain, true );

                // these settings are needed for auto range
                // if unipolar, set ranges for in auto range, set the high and low gain ranges.
                this._RelativeGainRange.Max = this._AnalogInputRange.Max - 0.5f * (1.0f - this.AnalogInputAutoRangeLimen) * this._AnalogInputRange.Range;
                this._RelativeGainRange.Min = this._AnalogInputRange.Min + 0.5f * (1.0f - this.AnalogInputAutoRangeLimen) * this._AnalogInputRange.Range;

                // set the relative range for higher gain making sure that upon using the 
                // higher gain, the signal will be within the AnalogInputAutoRangeLimen
                float higherGainAutoRangeLimen = (this.AnalogInputAutoRangeLimen - 0.01f) * this._Gain / this._GainNextHigher;
                this._HigherGainRange.Max = this._AnalogInputRange.Midrange + 0.5f * higherGainAutoRangeLimen * this._AnalogInputRange.Range;
                this._HigherGainRange.Min = this._AnalogInputRange.Midrange - 0.5f * higherGainAutoRangeLimen * this._AnalogInputRange.Range;
                return true;
            }
            catch ( Exception exn )
            {

                // add the message to the exception
                this.StatusMessage = $"{this.InstanceName} failed to set ranges";
                throw new OperationException( this.StatusMessage, exn );
            }
        }

        private bool DataValueOverflowedInternal( int dataValue )
        {
            return dataValue <= this._DataValueMin | dataValue >= this._DataValueMax;
        }

        /// <summary>Check the analog input ranges.</summary>
        /// <param name="voltage">
        ///   is a Single expression that specifies the input voltage to check for range.</param>
        /// <remarks>Call this method to check if the signal is in full range.</remarks>
        private bool InRangeInternal( float voltage )
        {
            return voltage <= this._AnalogInputRange.Max & voltage >= this._AnalogInputRange.Min;
        }

        /// <summary>Checks the analog input ranges.</summary>
        /// <returns><c>isAinOptimalRange</c> returns true if the input signal is in optional range.</returns>
        /// <param name="voltage">
        ///   is a Single expression that specifies the input voltage to check for range.</param>
        /// <remarks>Call this method to check if the signal is in optimal range. The 
        ///   signal is in optimal range if it is within the analog input relative 
        ///   range and outside the range of higher gain.</remarks>
        private bool InOptimalRangeInternal( float voltage )
        {
            return !(this.InHigherGainRangeInternal( voltage ) | this.InLowerGainRangeInternal( voltage ));
        }

        private bool InHigherGainRangeInternal( float voltage )
        {
            return voltage < this._HigherGainRange.Max & voltage > this._HigherGainRange.Min & !this.InMaxAnalogInputGainRangeInternal();
        }

        private bool InMaxAnalogInputGainRangeInternal()
        {
            return this._Gain >= this._GainMax;
        }

        private bool InLowerGainRangeInternal( float voltage )
        {
            return (voltage > this._RelativeGainRange.Max | voltage < this._RelativeGainRange.Min) & !this.InMinAnalogInputGainRangeInternal();
        }

        private bool InMinAnalogInputGainRangeInternal()
        {
            return this._Gain <= this._GainMin;
        }

        #endregion

    }
}
