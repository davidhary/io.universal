using System;

namespace isr.io.UL
{
    /// <summary>Links to the PMD-1208LS USB data acquisition module.</summary>
    /// <remarks> (c) 2002 Integrated Scientific Resources, Inc.
    /// Licensed under The MIT License.</para><para>
    /// David, 12/05/02, 1.0.997. Created 
    /// David, 05/30/03, 1.0.1245.x. Adapt for PMD-1208
    /// David, 96/03/04, 1.0.1615.x. Adapt for installing without the CBUL system files </para></remarks>
    public class Device : IDisposable
    {

        #region " CONSTRUCTORS  and  DESTRUCTORS "

        /// <summary>Constructs this class.</summary>
        public Device() : this( string.Empty )
        {
        }

        /// <summary>Constructs this class.</summary>
        /// <param name="instanceName">Specifies the name of the instance.</param>
        public Device( string instanceName ) : base()
        {
            this._InstanceName = instanceName;
        }

        /// <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        /// <remarks>Do not make this method overridable (virtual) because a derived 
        ///   class should not be able to override this method.</remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary>Gets or sets the dispose status sentinel.</summary>
        private bool _Disposed;
        /// <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        /// <param name="disposing">True if this method releases both managed and unmanaged 
        ///   resources; False if this method releases only unmanaged resources.</param>
        /// <remarks>Executes in two distinct scenarios as determined by
        ///   its disposing parameter.  If True, the method has been called directly or 
        ///   indirectly by a user's code--managed and unmanaged resources can be disposed.
        ///   If disposing equals False, the method has been called by the 
        ///   runtime from inside the finalizer and you should not reference 
        ///   other objects--only unmanaged resources can be disposed.</remarks>
        protected virtual void Dispose( bool disposing )
        {
            if ( !this._Disposed )
            {
                if ( disposing )
                {

                    // Free managed resources when explicitly called
                    this.StatusMessage = string.Empty;
                    this._InstanceName = string.Empty;
                }

                // Free shared unmanaged resources

            }

            // set the sentinel indicating that the class was disposed.
            this._Disposed = true;
        }

        /// <summary>This destructor will run only if the Dispose method 
        ///   does not get called. It gives the base class the opportunity to 
        ///   finalize. Do not provide destructors in types derived from this class.</summary>
        ~Device()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #region " BASE METHODS AND PROPERTIES "

        /// <summary>Overrides ToString returning the instance name if not empty.</summary>
        /// <remarks>Use this method to return the instance name. If instance name is not set, 
        ///   returns the base class ToString value.</remarks>
        public override string ToString()
        {
            return string.IsNullOrEmpty( this._InstanceName ) ? base.ToString() : this._InstanceName;
        }
        /// <summary>Gets or sets the opened status of the instance.</summary>
        /// <value><c>IsOpen</c> is a <see cref="Boolean"/> property that is True if the 
        ///   instance is open.</value>
        public bool IsOpen { get; set; }

        private string _InstanceName = string.Empty;
        /// <summary>Gets or sets the name given to an instance of this class.</summary>
        /// <value><c>InstanceName</c> is a String property.</value>
        /// <remarks>If instance name is not set, returns .ToString.</remarks>
        /// <remarks> David, 11/23/04", 1.0.1788.x">
        ///   Correct code no to get instance name from .ToString but from MyBase.ToString
        ///   so that calling this method from the child class will not break the rule of
        ///   calling overridable methods from the constructor.
        /// </para></remarks>
        public string InstanceName
        {
            get => !string.IsNullOrEmpty( this._InstanceName ) ? this._InstanceName : base.ToString();

            set => this._InstanceName = value;
        }
        /// <summary>Gets or sets the status message.</summary>
        /// <value>A <see cref="String">String</see>.</value>
        /// <remarks>Use this property to get the status message generated by the object.</remarks>
        public string StatusMessage { get; set; } = string.Empty;

        #endregion

        #region " METHODS "

        /// <summary>opens this instance.</summary>
        /// <exception cref="isr.io.UL.OperationException" guarantee="strong">
        ///   Failed I/O initialize error handling or declaring version level.
        /// </exception>
        /// <remarks>Use this method to open the instance.</remarks>
        /// <remarks> David, 05/30/03", 1.0.1245.x">
        /// Adapt for PMD-1208 </para>
        /// David, 08/07/03, 1.0.1314.
        ///   Declare revision level of library and initialize error reporting.
        /// </para>
        /// David, 06/03/04, 1.0.1615
        ///   Ignore first fail so as not to use CBUL system drivers.
        /// </para></remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public virtual bool Open( int deviceNumber )
        {
            float revisionNumber  = MccDaq.MccService.CurrentRevNum;
            try
            {

                // Declare a private MccBoard _daqDevice object for Device number 
                this.DaqDevice = new MccDaq.MccBoard( deviceNumber );

                // declare revision level of Universal Library
                if ( MccDaq.MccService.DeclareRevision( ref revisionNumber ).Value == MccDaq.ErrorInfo.ErrorCode.NoErrors )
                {

                    // initiate error handling
                    if ( this.InitiateErrorHandling() )
                    {

                        // get the software revision
                        revisionNumber = SoftwareRevision;

                        // trap error on first call to device per instructions from MCC VP of S/w Eng. 
                        // because the first time it tries to access the CBUL32.sys driver
                        try
                        {
                            _ = this.DoIdentify();
                        }
                        catch ( Exception )
                        {
                        }

                        // identify the device
                        _ = this.DoIdentify();
                        this.IsOpen = true;
                    }
                    else
                    {
                        this.StatusMessage = $"{this.InstanceName} failed to initiate error handling";
                        throw new OperationException( this.StatusMessage );
                    }
                }
                else
                {
                    this.StatusMessage = $"{this.InstanceName} failed to declare revision level of Universal Library";
                    throw new OperationException( this.StatusMessage );
                }
            }
            catch ( HardwareNotFoundException exn )
            {

                // if hardware not found, just throw the same exception
                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw exn;
            }
            catch ( Exception exn )
            {

                // close device to meet strong guarantees
                try
                {
                    _ = this.Close();
                }
                finally
                {
                }

                // throw an exception
                this.StatusMessage = $"{this.InstanceName} operation failed opening";
                throw new OperationOpenException( this.StatusMessage, exn );
            }

            if ( this.DeviceErrorInfo.Value != MccDaq.ErrorInfo.ErrorCode.NoErrors )
            {

                // if error, 

                // close device to meet strong guarantees
                try
                {
                    _ = this.Close();
                }
                catch
                {
                }
                finally
                {
                }
                // throw an exception
                this.StatusMessage = $"{this.InstanceName} operation failed opening. PMD-1208 returned error {this.DeviceErrorCode}:: {this.DeviceErrorMessage}";
                throw new OperationOpenException( this.StatusMessage );
            }
            else if ( revisionNumber <= 0f )
            {
                // throw an exception
                this.StatusMessage = $"{this.InstanceName} operation failed opening. PMD-1208 returned an invalid firmware version.";
                throw new HardwareNotFoundException( this.StatusMessage );
            }
            else
            {
                return this.IsOpen;
            }
        }

        /// <summary>Closes the instance.</summary>
        /// <exception cref="isr.io.UL.OperationInitializeException" guarantee="strong"></exception>
        /// <remarks>Use this method to close the instance.</remarks>
        /// <remarks> David, 05/30/03", 1.0.1245.x">
        ///   Adapt for PMD-1208
        /// </para></remarks>
        public virtual bool Close()
        {
            try
            {

                // clear the error code
                this.DeviceErrorInfo = new MccDaq.ErrorInfo();
                this.IsOpen = false;
                return !this.IsOpen;
            }
            catch ( ApplicationException exn )
            {
                // throw an exception
                this.StatusMessage = $"{this.InstanceName} operation failed closing";
                throw new OperationInitializeException( this.StatusMessage, exn );
            }
        }

        /// <summary>Identifies the device.</summary>
        /// <exception cref="isr.io.UL.OperationException" guarantee="strong"></exception>
        /// <remarks>Use this method to flash the LED and return the device name.</remarks>
        public string DoIdentify()
        {

            // flash the led
            this.DeviceErrorInfo = this.IsDemo ? new MccDaq.ErrorInfo() : this.DaqDevice.FlashLED();
            if ( this.DeviceErrorInfo.Value == MccDaq.ErrorInfo.ErrorCode.NoErrors )
            {
                // if no error return the device name
                if ( this.IsDemo )
                {
                    return "demo device";
                }
                else
                {
                    // if no error return the device name
                    return this.DaqDevice.BoardName;
                }
            }
            else if ( this.DeviceErrorInfo.Value == MccDaq.ErrorInfo.ErrorCode.BadBoard )
            {
                // throw an exception
                this.StatusMessage = $"{this.InstanceName} failed to locate the device.";
                throw new HardwareNotFoundException( this.StatusMessage, ( long ) this.DeviceErrorInfo.Value, this.DeviceErrorInfo.Message );
            }
            else
            {
                // throw an exception
                this.StatusMessage = $"{this.StatusMessage} failed to identify the device.";
                throw new IOException( this.StatusMessage, ( long ) this.DeviceErrorInfo.Value, this.DeviceErrorInfo.Message );
            }
        }

        /// <summary>Initiates error handling.</summary>
        /// <returns>A Boolean data type that is true if success</returns>
        /// <remarks>Use this method to initiate error handling so that errors will not 
        ///   generate a message to the screen -- this program checks the returned error 
        ///   code after each library call to determine if an error occurred. The program 
        ///   will always continue executing when an error occurs.
        /// </remarks>
        public bool InitiateErrorHandling()
        {

            // Initiate error handling
            // activating error handling will trap errors like
            // bad channel numbers and non-configured conditions.
            // Parameters:
            // MccDaq.ErrorReporting.PrintAll :all warnings and errors encountered will be printed
            // MccDaq.ErrorHandling.StopAll   :if any error is encountered, the program will stop
            this.DeviceErrorInfo = MccDaq.MccService.ErrHandling( MccDaq.ErrorReporting.DontPrint, MccDaq.ErrorHandling.DontStop );
            return this.DeviceErrorInfo.Value == MccDaq.ErrorInfo.ErrorCode.NoErrors;
        }

        #endregion

        #region " DEVICE PROPERTIES "

        /// <summary>Gets or sets the Device serial number..</summary>
        /// <value><c>SerialNumber</c> is an Int32 property.</value>
        /// <remarks>Use this property to get the Device number as it is identified by the CB.CFG 
        ///   configuration of the Universal Library.</remarks>
        public int DeviceNumber { get; private set; }

        /// <summary>Gets or sets the Device board.</summary>
        /// <value><c>DaqDevice</c> is an Object MccDaq.MccBoard property.</value>
        /// <remarks>Use this property to get project-level reference to the MccDaq board.</remarks>
        internal MccDaq.MccBoard DaqDevice { get; private set; }

        internal MccDaq.ErrorInfo DeviceErrorInfo { get; set; }

        /// <summary>Returns the current Device error.</summary>
        /// <value><c>DeviceErrorMessage</c> is a String property.</value>
        public string DeviceErrorMessage => this.DeviceErrorInfo.Message;

        /// <summary>Returns the current Device error code.</summary>
        /// <value><c>DeviceErrorCode</c> is a String property.</value>
        public int DeviceErrorCode => ( int ) this.DeviceErrorInfo.Value;
        /// <summary>Gets or sets the demo mode.</summary>
        /// <value><c>IsDemo</c> is a Boolean property.</value>
        /// <remarks>Use this property to get or set the demo mode of the Device.
        ///   When in demo mode, Device returns demo values.</remarks>
        public bool IsDemo { get; set; }

        /// <summary>Gets or sets the Device Firmware Version.</summary>
        /// <value><c>SoftwareRevision</c> is a Single Precision property.</value>
        /// <remarks>Use this property to get the firmware version of the Device.</remarks>
        public static float SoftwareRevision
        {
            get => MccDaq.MccService.CurrentRevNum;

            set => _ = MccDaq.MccService.DeclareRevision( ref value );
        }

        #endregion

    }

    /// <summary>This structure defines a scalar range for A/D converters.</summary>
    /// <remarks>The SignalRange provides the basic structure for handling the range of a scalar.(c) 2003 Integrated Scientific Resources, Inc.
    /// Licensed under The MIT License.</para><para>
    /// David, 01/13/03", 1.0.1122.x">
    /// Created
    /// </para></remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1815:Override equals and operator equals on value types", Justification = "<Pending>" )]
    public struct SignalRange
    {
        public float Min { get; set; }
        public float Max { get; set; }
        public float Resolution { get; set; }
        public int Bits { get; set; }
        public float Low { get; set; }
        public float High { get; set; }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2022-01-17. </remarks>
        /// <param name="minValue"> The minimum value. </param>
        /// <param name="maxValue"> The maximum value. </param>
        public SignalRange( float minValue, float maxValue )
        {
            this.Min = minValue;
            this.Max = maxValue;
            this.Bits = 12;
            this.Resolution = Convert.ToSingle( (maxValue - minValue) / Math.Pow( 2d, this.Bits ), System.Globalization.CultureInfo.CurrentCulture );
            this.Low = this.Min + this.Resolution;
            this.High = this.Max - this.Resolution - this.Resolution;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2022-01-17. </remarks>
        /// <param name="rangeValue">   The range value. </param>
        /// <param name="gainValue">    The gain value. </param>
        /// <param name="bipolarValue"> True to bipolar value. </param>
        public SignalRange( float rangeValue, float gainValue, bool bipolarValue )
        {
            float minValue, maxValue;
            if ( bipolarValue )
            {
                maxValue = 0.5f * rangeValue / gainValue;
                minValue = -maxValue;
            }
            else
            {
                maxValue = rangeValue / gainValue;
                minValue = 0f;
            }

            this.Min = minValue;
            this.Max = maxValue;
            this.Bits = 12;
            this.Resolution = Convert.ToSingle( (maxValue - minValue) / Math.Pow( 2d, this.Bits ), System.Globalization.CultureInfo.CurrentCulture );
            this.Low = this.Min + this.Resolution;
            this.High = this.Max - this.Resolution - this.Resolution;
        }


        /// <summary>   Sets a range. </summary>
        /// <remarks>   David, 2022-01-17. </remarks>
        /// <param name="minValue"> The minimum value. </param>
        /// <param name="maxValue"> The maximum value. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void SetRange( float minValue, float maxValue )
        {
            this.Min = minValue;
            this.Max = maxValue;
            this.Bits = 12;
            this.Resolution = Convert.ToSingle( this.Range / Math.Pow( 2d, this.Bits ), System.Globalization.CultureInfo.CurrentCulture );
            this.Low = this.Min + this.Resolution;
            this.High = this.Max - this.Resolution - this.Resolution;
        }

        public bool Equals( SignalRange rangeCompared )
        {
            return Math.Ceiling( this.Min / this.Resolution ) == Math.Ceiling( rangeCompared.Min / rangeCompared.Resolution ) & Math.Ceiling( this.Max / this.Resolution ) == Math.Ceiling( rangeCompared.Max / rangeCompared.Resolution ) & this.Resolution == rangeCompared.Resolution;

        }

        public override int GetHashCode()
        {
            return Convert.ToInt32( Math.Ceiling( this.Min / this.Resolution ), System.Globalization.CultureInfo.CurrentCulture ) ^ Convert.ToInt32( Math.Ceiling( this.Max / this.Resolution ), System.Globalization.CultureInfo.CurrentCulture );
        }

        public override string ToString()
        {
            return $"({this.Min},{this.Max})";
        }

        public float Range => this.Max - this.Min;

        public float Midrange => 0.5f * (this.Max + this.Min);
    }
}
