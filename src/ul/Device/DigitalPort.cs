﻿using System;

namespace isr.io.UL
{
    /// <summary>Defines a digital port for I/O..</summary>
    /// <remarks> (c) 2003 Integrated Scientific Resources, Inc.
    /// Licensed under The MIT License.</para><para>
    /// David, 08/07/03, 1.0.1314. Created. </para></remarks>

    // based on the BaseClass inheritable class
    public class DigitalPort : IDisposable
    {

        #region " CONSTRUCTORS  and  DESTRUCTORS "

        /// <summary>Constructs this class.</summary>
        /// <param name="instanceName">Specifies the name of the instance.</param>
        /// <param name="deviceLink">
        ///   is an Object expression that specifies the signal link driver for accessing
        ///   the device.</param>
        /// <remarks>Use this constructor to instantiate this class
        ///   and set the instance name, which is useful in tracing</remarks>
        public DigitalPort( string instanceName, Device deviceLink ) : base()
        {
            this._InstanceName = instanceName;
            this._DigitalDevice = deviceLink;
        }

        /// <summary>Constructs this class.</summary>
        /// <param name="deviceLink">
        ///   is an Object expression that specifies the signal link driver for accessing
        ///   the device.</param>
        /// <remarks>Use this constructor to instantiate this class
        ///   and set the instance name, which is useful in tracing</remarks>
        public DigitalPort( Device deviceLink ) : this( deviceLink.ToString(), deviceLink )
        {
        }

        /// <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        /// <remarks>Do not make this method overridable (virtual) because a derived 
        ///   class should not be able to override this method.</remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary>Gets or sets the dispose status sentinel.</summary>
        private bool _Disposed;
        /// <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        /// <param name="disposing">True if this method releases both managed and unmanaged 
        ///   resources; False if this method releases only unmanaged resources.</param>
        /// <remarks>Executes in two distinct scenarios as determined by
        ///   its disposing parameter.  If True, the method has been called directly or 
        ///   indirectly by a user's code--managed and unmanaged resources can be disposed.
        ///   If disposing equals False, the method has been called by the 
        ///   runtime from inside the finalizer and you should not reference 
        ///   other objects--only unmanaged resources can be disposed.</remarks>
        protected virtual void Dispose( bool disposing )
        {
            if ( !this._Disposed )
            {
                if ( disposing )
                {

                    // Free managed resources when explicitly called
                    this.StatusMessage = string.Empty;
                    this._InstanceName = string.Empty;
                    if ( this._DigitalDevice is object )
                    {
                        this._DigitalDevice.Dispose();
                    }
                }

                // Free shared unmanaged resources

            }

            // set the sentinel indicating that the class was disposed.
            this._Disposed = true;
        }

        /// <summary>This destructor will run only if the Dispose method 
        ///   does not get called. It gives the base class the opportunity to 
        ///   finalize. Do not provide destructors in types derived from this class.</summary>
        ~DigitalPort()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary>Overrides ToString returning the instance name if not empty.</summary>
        /// <remarks>Use this method to return the instance name. If instance name is not set, 
        ///   returns the base class ToString value.</remarks>
        public override string ToString()
        {
            return string.IsNullOrEmpty( this._InstanceName ) ? base.ToString() : this._InstanceName;
        }

        private string _InstanceName = string.Empty;
        /// <summary>Gets or sets the name given to an instance of this class.</summary>
        /// <value><c>InstanceName</c> is a String property.</value>
        /// <remarks> David, 11/23/04, 1.0.1788.
        ///   Correct code no to get instance name from .ToString but from MyBase.ToString
        ///   so that calling this method from the child class will not break the rule of
        ///   calling overridable methods from the constructor.
        /// </para></remarks>
        public string InstanceName
        {
            get => !string.IsNullOrEmpty( this._InstanceName ) ? this._InstanceName : base.ToString();

            set => this._InstanceName = value;
        }
        /// <summary>Gets or sets the status message.</summary>
        /// <value>A <see cref="String">String</see>.</value>
        public string StatusMessage { get; set; } = string.Empty;

        #endregion

        #region " METHODS "

        /// <summary>Configures the digital port.</summary>
        /// <exception cref="isr.io.UL.OperationException" guarantee="strong"></exception>
        public void ConfigurePort()
        {
            if ( this._DigitalDevice.IsDemo )
            {
                this._DigitalDevice.DeviceErrorInfo = new MccDaq.ErrorInfo();
            }
            else
            {
                // configure the port.
                this._DigitalDevice.DeviceErrorInfo = this._DigitalDevice.DaqDevice.DConfigPort( this._PortType, this._PortDirection );
            }

            if ( this._DigitalDevice.DeviceErrorInfo.Value == MccDaq.ErrorInfo.ErrorCode.NoErrors )
            {
            }
            else
            {
                // throw an exception
                this.StatusMessage = $"{this.InstanceName} failed to configure digital port.";
                throw new IOException( this.StatusMessage, ( long ) this._DigitalDevice.DeviceErrorInfo.Value, this._DigitalDevice.DeviceErrorInfo.Message );
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>Gets or sets the reference to the parent device.</summary>
        private readonly Device _DigitalDevice;
        private bool _Input = true;
        private MccDaq.DigitalPortDirection _PortDirection = MccDaq.DigitalPortDirection.DigitalIn;
        /// <summary>Gets or sets the condition for this is an input port.</summary>
        public bool IsInput
        {
            get => this._Input;

            set {
                this._Input = value;
                this._PortDirection = this._Input ? MccDaq.DigitalPortDirection.DigitalIn : MccDaq.DigitalPortDirection.DigitalOut;
            }
        }

        private int _PortNumber;
        private MccDaq.DigitalPortType _PortType = MccDaq.DigitalPortType.FirstPortA;
        /// <summary>Specifies the port number.</summary>
        /// <value></value>
        /// <remarks>Port numbers can be either 0 or 1 for ports A and B respectively.</remarks>
        public int PortNumber
        {
            get => this._PortNumber;

            set {
                this._PortNumber = value;
                switch ( this._PortNumber )
                {
                    case 1:
                        {
                            this._PortType = MccDaq.DigitalPortType.FirstPortB;
                            break;
                        }

                    default:
                        {
                            this._PortType = MccDaq.DigitalPortType.FirstPortA;
                            break;
                        }
                }
            }
        }

        private ushort _PortValue;
        /// <summary>Reads or writes a digital port value.</summary>
        /// <value><c>PortValue</c> is an unsigned Int32 (16 bits) property</value>
        /// <exception cref="isr.io.UL.IOException" guarantee="strong">
        ///   Failed attempting to output to an input port or input from and output port
        ///   or to set or get digital port.
        /// </exception>
        /// <remarks>Use this method to read or write from the digital port.  The device
        /// digital port values are expressed as "T:UINT16" and converted to INT32 to preserve
        /// CLS compliance. <para>
        /// <remarks> 4/21/2006	Change to Integer. </para></remarks>
        public int PortValue
        {
            get => this.PortValueGetter();

            set => _ = this.PortValueSetter( value );
        }

        /// <summary>
        /// Reads and returns a value from the digital port.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public int PortValueGetter()
        {
            if ( this._Input )
            {
                if ( this._DigitalDevice.IsDemo )
                {
                    this._DigitalDevice.DeviceErrorInfo = new MccDaq.ErrorInfo();
                    return 0;
                }
                else
                {
                    this._DigitalDevice.DeviceErrorInfo = this._DigitalDevice.DaqDevice.DIn( this._PortType, out this._PortValue );
                    if ( this._DigitalDevice.DeviceErrorInfo.Value == MccDaq.ErrorInfo.ErrorCode.NoErrors )
                    {
                        // if no error return value
                        return Convert.ToInt32( this._PortValue );
                    }
                    else
                    {
                        // throw an exception
                        this.StatusMessage = $"{this.InstanceName} failed to get digital input.";
                        throw new IOException( this.StatusMessage, ( long ) this._DigitalDevice.DeviceErrorInfo.Value, this._DigitalDevice.DeviceErrorInfo.Message );
                    }
                }
            }
            else
            {
                // if not input, raise an error
                this.StatusMessage = $"{this.InstanceName} failed attempting to input from an output port.";
                throw new IOException( this.StatusMessage );
            }
        }

        /// <summary>
        /// Writes a value to the digital port.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public int PortValueSetter( int value )
        {
            if ( this._Input )
            {
                // if not input, raise an error
                this.StatusMessage = $"{this.InstanceName} failed attempting to output to an input port.";
                throw new IOException( this.StatusMessage );
            }
            else if ( this._DigitalDevice.IsDemo )
            {
                this._DigitalDevice.DeviceErrorInfo = new MccDaq.ErrorInfo();
            }
            else
            {
                this._PortValue = Convert.ToUInt16( value );
                this._DigitalDevice.DeviceErrorInfo = this._DigitalDevice.DaqDevice.DOut( this._PortType, this._PortValue );
                if ( this._DigitalDevice.DeviceErrorInfo.Value == MccDaq.ErrorInfo.ErrorCode.NoErrors )
                {
                }
                else
                {
                    // throw an exception
                    this.StatusMessage = $"{this.InstanceName} failed getting digital input.";
                    throw new IOException( this.StatusMessage, ( long ) this._DigitalDevice.DeviceErrorInfo.Value, this._DigitalDevice.DeviceErrorInfo.Message );
                }
            }

            return this._PortValue;
        }

        #endregion

    }
}