﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.io.UL.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.io.UL.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.io.UL.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
