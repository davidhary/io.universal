﻿
namespace isr.io.UL.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    public sealed partial class MyLibrary
    {

        /// <summary> Constructor that prevents a default instance of this class from being created. </summary>
        private MyLibrary() : base()
        {
        }

        public const string AssemblyTitle = "Universal Library";
        public const string AssemblyDescription = "Universal Library";
        public const string AssemblyProduct = "isr.io.UL";
    }
}