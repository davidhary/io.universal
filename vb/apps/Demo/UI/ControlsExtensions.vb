Imports System.Reflection
Imports System.Runtime.CompilerServices

''' <summary>   The controls extensions. </summary>
''' <remarks>   David, 2022-01-12. </remarks>
Public Module ControlsExtensions

#Region " PRODUCT PROCESS CAPTION "

    ''' <summary> Prefix process name. </summary>
    ''' <remarks> David, 2022-01-17. </remarks>
    ''' <param name="assembly"> Represents an assembly, which is a reusable, versionable, and self-
    '''                         describing building block of a common language runtime application. 
    ''' </param>
    ''' <returns> A String. </returns>
    <Extension()>
    Public Function PrefixProcessName(ByVal assembly As Assembly) As String
        If assembly Is Nothing Then
            Throw New ArgumentNullException(NameOf(assembly))
        End If

        Dim productName As String = Enumerable.FirstOrDefault(assembly.GetCustomAttributes(GetType(AssemblyProductAttribute)).OfType(Of AssemblyProductAttribute)()).Product
        Dim processName As String = Process.GetCurrentProcess().ProcessName

        If Not productName.StartsWith(processName, StringComparison.OrdinalIgnoreCase) Then
            productName = $"{processName}.{productName}"
        End If

        Return productName
    End Function

    ''' <summary> Builds local time caption. </summary>
    ''' <remarks> David, 2022-01-17. </remarks>
    ''' <param name="timeCaptionFormat"> The time caption format. </param>
    ''' <param name="kindFormat">        The kind format. </param>
    ''' <returns> A String. </returns>
    Public Function BuildLocalTimeCaption(ByVal timeCaptionFormat As String, ByVal kindFormat As String) As String
        Dim result As String = If(String.IsNullOrWhiteSpace(timeCaptionFormat), $"{DateTimeOffset.Now}",
                                  If(String.IsNullOrWhiteSpace(kindFormat), $"{DateTimeOffset.Now.ToString(timeCaptionFormat)}",
                                                                            $"{DateTimeOffset.Now.ToString(timeCaptionFormat)}{DateTimeOffset.Now.ToString(kindFormat)}"))
        Return result
    End Function

    ''' <summary>   Builds product time caption. </summary>
    ''' <remarks>
    ''' <list type="bullet">Use the following format options: <item>
    ''' u - UTC - 2019-09-10 19:27:04Z</item><item>
    ''' r - GMT - Tue, 10 May 2019 19:26:42 GMT</item><item>
    ''' o - ISO - 2019-09-10T12:12:29.7552627-07:00</item><item>
    ''' s - ISO - 2019-09-10T12:24:47</item><item>
    ''' empty   - 2019-09-10 16:57:24 -07:00</item><item>
    ''' s + zzz - 2019-09-10T12:24:47-07:00</item></list>
    ''' </remarks>
    <Extension()>
    Public Function BuildProductTimeCaption(ByVal assembly As Assembly, ByVal versionElements As Integer, ByVal timeCaptionFormat As String, ByVal kindFormat As String) As String
        If assembly Is Nothing Then
            Throw New ArgumentNullException(NameOf(assembly))
        Else
            Return $"{assembly.PrefixProcessName()}.r.{assembly.GetName().Version.ToString(versionElements)} {BuildLocalTimeCaption(timeCaptionFormat, kindFormat)}"
        End If
    End Function

    ''' <summary>
    ''' Builds product time caption using full version and local time plus kind format.
    ''' </summary>
    ''' <remarks>   David, 2020-09-15. </remarks>
    ''' <param name="assembly"> Represents an assembly, which is a reusable, versionable, and self-
    '''                         describing building block of a common language runtime application. </param>
    ''' <returns>   A String. </returns>
    <Extension()>
    Public Function BuildProductTimeCaption(ByVal assembly As Assembly) As String
        Return assembly.BuildProductTimeCaption(4, String.Empty, String.Empty)
    End Function

#End Region

End Module
