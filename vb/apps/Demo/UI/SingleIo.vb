Imports System.Reflection

Imports Microsoft.VisualBasic.ApplicationServices
''' <summary>Includes code for form SingleIo, which provide test methods for single I/O.</summary>
''' <remarks>Use this module to test single I/O operations of the board.(c) 2002 Integrated Scientific Resources, Inc.
''' Licensed under The MIT License.<para>
''' David, 12/05/02, 1.0.1069. Created </para><para>
''' David, 05/30/03, 1.0.1245. Adapt to PMD-1208 </para></remarks>
Partial Friend Class SingleIo
    Inherits System.Windows.Forms.Form

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
    ''' </summary>
    ''' <remarks> David, 2022-01-17. </remarks>
    Public Sub New()
        MyBase.New()
        ' This method is required by the Windows Form Designer.
        Me.InitializeComponent()
    End Sub

    ' Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                Me.components?.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)

        End Try
    End Sub

#End Region

#Region " METHODS "

    ''' <summary>Displays this module.</summary>
    ''' <returns>Returns dialog result.</returns>
    ''' <remarks>Use this method to display this form.</remarks>
    Friend Overloads Function ShowDialog() As System.Windows.Forms.DialogResult

        ' show the form
        Return MyBase.ShowDialog()

    End Function

    ''' <summary>Initializes the user interface and tool tips.</summary>
    Private Sub InitializeUserInterface()
        Me.tipsToolTip.SetToolTip(Me.openDeviceCheckBox, "Depress to open the device or release to close.")
    End Sub

    ''' <summary>opens access to SignalLink.</summary>
    ''' <remarks>Use this method to open the driver in real or demo modes</remarks>
    Private Sub Open()

        ' the board number is a constant.  We should later get it from 
        ' a configuration file.
        Const boardNumber As Integer = 1

        Try

            ' instantiate the class
            ' Single I/O is only relevant for real mode
            Me._DaqDevice = New isr.io.UL.Device("Single I/O") With {
                .IsDemo = False
            }

            ' open the driver
            If Me.OpenDriver(boardNumber) Then

                Me.analogInputGroupBox.Enabled = True
                Me.analogOutputGroupBox.Enabled = True
                Me.counterGroupBox.Enabled = True
                Me.digitalIoGroupBox.Enabled = True
                Me.ledCheckBox.Enabled = True

                ' enable the timer
                Me.actionTimer.Enabled = True

            Else
                ' if failed opening, disable user interface 
                Me.analogInputGroupBox.Enabled = False
                Me.analogOutputGroupBox.Enabled = False
                Me.counterGroupBox.Enabled = False
                Me.digitalIoGroupBox.Enabled = False
                Me.ledCheckBox.Enabled = False

                ' disable the timer
                Me.actionTimer.Enabled = False
            End If

            ' enable controls
        Catch exn As System.ApplicationException

            ' add the message to the exception
            Me._StatusMessage = "failed instantiating the MCC device driver"
            Throw New ApplicationException(Me._StatusMessage, exn)

        End Try

    End Sub

    ''' <summary>opens the SignalLink driver.</summary>
    ''' <remarks>Use this method to open the driver</remarks>
    Private Function OpenDriver(ByVal boardNumber As Integer) As Boolean

        ' use in finally to determine if we should complete the open process.
        Dim failed As Boolean = False

        Try

            ' open the driver trapping hardware not found exception
            Me._DaqDevice.Open(boardNumber)

        Catch hardwareExn As isr.io.UL.HardwareNotFoundException

            ' if failed opening try to open in demo mode
            If Me._DaqDevice.IsDemo Then

                ' add the message to the exception
                Me._StatusMessage = "Failed opening the data acquisition device."
                failed = True

            Else

                ' if not demo, set to demo mode.
                Me._DaqDevice.IsDemo = True

                Try

                    ' try to open the driver
                    Me._DaqDevice.Open(boardNumber)

                    ' add the message to message list
                    Me._StatusMessage = "Failed opening device for data acquisition.  Reverting to demo mode.  Check if the device is connected."
                    Me.messagesTextBox.Text = Me._StatusMessage

                Catch exn As System.ApplicationException

                    ' add the message to the exception
                    Me._StatusMessage = "Failed opening the data acquisition device"
                    failed = True

                End Try

            End If

        Finally

            If failed Then

                ' if failed, do nothing.  we shall abort this one.

            Else

                ' return the firmware version
                Me.messagesTextBox.Text = "Open.  Software revision: " & isr.io.UL.Device.SoftwareRevision.ToString(Globalization.CultureInfo.CurrentCulture)

                ' instantiate two digital input channels
                Me._PortZero = New isr.io.UL.DigitalPort("Port A", Me._DaqDevice) With {
                    .IsInput = True,
                    .PortNumber = 0
                }
                Me._PortZero.ConfigurePort()

                Me._PortOne = New isr.io.UL.DigitalPort("Port B", Me._DaqDevice) With {
                    .IsInput = True,
                    .PortNumber = 1
                }
                Me._PortOne.ConfigurePort()

                ' instantiate four analog input channels
                Me._InputZero = New isr.io.UL.AnalogInput("Channel A", Me._DaqDevice)
                Me._InputOne = New isr.io.UL.AnalogInput("Channel B", Me._DaqDevice)
                Me._InputTwo = New isr.io.UL.AnalogInput("Channel C", Me._DaqDevice)
                Me._InputThree = New isr.io.UL.AnalogInput("Channel D", Me._DaqDevice)

            End If

        End Try

        Return Not failed

    End Function

    ''' <summary>Closes and releases the data acquisition device driver.</summary>
    ''' <remarks>Use this method to close and release the driver</remarks>
    Private Sub CloseSignalIo()

        Try

            ' disable the timer
            Me.actionTimer.Enabled = False
            System.Windows.Forms.Application.DoEvents()

            ' check if we have instantiated the class
            If Me._DaqDevice Is Nothing Then
                Me.messagesTextBox.Text = "Warning.  Driver already closed."
            Else
                ' close the driver.
                Me._DaqDevice.Close()
            End If

        Catch exn As System.ApplicationException

            ' add the message to the exception
            Dim usrMessage As String
            usrMessage = "Failed closing the data acquisition device driver"
            Throw New ApplicationException(usrMessage, exn)

        Finally

            ' disable all group boxes
            Me.analogInputGroupBox.Enabled = False
            Me.analogOutputGroupBox.Enabled = False
            Me.counterGroupBox.Enabled = False
            Me.digitalIoGroupBox.Enabled = False
            Me.ledCheckBox.Enabled = False

        End Try

    End Sub

    ''' <summary>Initializes the class objects.</summary>
    ''' <remarks>Called from the form load method to instantiate 
    '''   module-level objects.</remarks>
    Private Sub InstantiateObjects()

        ' instantiate the board
        Me._DaqDevice = New isr.io.UL.Device("Single I/O")

        ' disable all group boxes
        Me.analogInputGroupBox.Enabled = False
        Me.analogOutputGroupBox.Enabled = False
        Me.counterGroupBox.Enabled = False
        Me.digitalIoGroupBox.Enabled = False
        Me.ledCheckBox.Enabled = False

        ' set the channel and gains
        Me.channelCheckBoxA.Items.Clear()
        Me.channelCheckBoxA.DataSource = isr.io.UL.AnalogInput.Channels
        Me.channelCheckBoxA.SelectedIndex = 0

        Me.channelCheckBoxB.Items.Clear()
        Me.channelCheckBoxB.DataSource = isr.io.UL.AnalogInput.Channels
        Me.channelCheckBoxB.SelectedIndex = 1

        Me.channelCheckBoxC.Items.Clear()
        Me.channelCheckBoxC.DataSource = isr.io.UL.AnalogInput.Channels
        Me.channelCheckBoxC.SelectedIndex = 2

        Me.channelCheckBoxD.Items.Clear()
        Me.channelCheckBoxD.DataSource = isr.io.UL.AnalogInput.Channels
        Me.channelCheckBoxD.SelectedIndex = 3

        Me.rangeCheckBoxA.Items.Clear()
        Me.rangeCheckBoxA.DataSource = isr.io.UL.AnalogInput.Ranges
        Me.rangeCheckBoxA.SelectedIndex = 0

        Me.rangeCheckBoxB.Items.Clear()
        Me.rangeCheckBoxB.DataSource = isr.io.UL.AnalogInput.Ranges
        Me.rangeCheckBoxB.SelectedIndex = 0

        Me.rangeCheckBoxC.Items.Clear()
        Me.rangeCheckBoxC.DataSource = isr.io.UL.AnalogInput.Ranges
        Me.rangeCheckBoxC.SelectedIndex = 0

        Me.rangeCheckBoxD.Items.Clear()
        Me.rangeCheckBoxD.DataSource = isr.io.UL.AnalogInput.Ranges
        Me.rangeCheckBoxD.SelectedIndex = 0

    End Sub

#End Region

#Region " PROPERTIES "

    ' module level SignalLink PMD-1208 board
    Private _DaqDevice As isr.io.UL.Device

    ' module level digital ports
    Private _PortOne As isr.io.UL.DigitalPort
    Private _PortZero As isr.io.UL.DigitalPort

    ' module level analog input channel
    Private _InputZero As isr.io.UL.AnalogInput
    Private _InputOne As isr.io.UL.AnalogInput
    Private _InputTwo As isr.io.UL.AnalogInput
    Private _InputThree As isr.io.UL.AnalogInput
    Private _StatusMessage As String = String.Empty

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs after the form is closed.</summary>
    ''' <param name="sender"><see cref="Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>This event is a notification that the form has already gone away before
    ''' control is returned to the calling method (in case of a modal form).  Use this
    ''' method to delete any temporary files that were created or dispose of any objects
    ''' not disposed with the closing event.
    ''' </remarks>
    Private Sub Form_Closed(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles MyBase.Closed

    End Sub

    ''' <summary>Occurs before the form is closed</summary>
    ''' <param name="sender"><see cref="Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
    ''' <remarks>Use this method to optionally cancel the closing of the form.
    ''' Because the form is not yet closed at this point, this is also the best 
    ''' place to serialize a form's visible properties, such as size and 
    ''' location. Finally, dispose of any form level objects especially those that
    ''' might needs access to the form and thus should not be terminated after the
    ''' form closed.
    ''' </remarks>
    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) _
    Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        System.Windows.Forms.Application.DoEvents()

        ' set module objects that reference other objects to Nothing

        ' terminate form objects
        Me.TerminateObjects()

    End Sub

    ''' <summary>Terminates and disposes of class-level objects.</summary>
    ''' <remarks>Called from the form Closing method.</remarks>
    Private Sub TerminateObjects()

        ' disable all group boxes
        Me.analogInputGroupBox.Enabled = False
        Me.analogOutputGroupBox.Enabled = False
        Me.counterGroupBox.Enabled = False
        Me.digitalIoGroupBox.Enabled = False
        Me.ledCheckBox.Enabled = False

        ' disable the timer
        Me.actionTimer.Enabled = False
        System.Windows.Forms.Application.DoEvents()

        ' terminate module objects 
        If Me._PortOne IsNot Nothing Then
            Me._PortOne.Dispose()
        End If

        If Me._PortZero IsNot Nothing Then
            Me._PortZero.Dispose()
        End If

        If Me._InputZero IsNot Nothing Then
            Me._InputZero.Dispose()
        End If
        If Me._InputOne IsNot Nothing Then
            Me._InputOne.Dispose()
        End If
        If Me._InputTwo IsNot Nothing Then
            Me._InputTwo.Dispose()
        End If
        If Me._InputThree IsNot Nothing Then
            Me._InputThree.Dispose()
        End If

        If Me._DaqDevice IsNot Nothing Then

            ' close the board
            Me.CloseSignalIo()

            Me._DaqDevice.Dispose()
            Me._DaqDevice = Nothing
        End If

    End Sub

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            Me.InstantiateObjects()

            ' set the form caption
            Me.Text = ControlsExtensions.BuildProductTimeCaption(Assembly.GetExecutingAssembly()) & ": SINGLE I/O PANEL"

            ' set tool tips
            Me.InitializeUserInterface()

            ' center the form
            Me.CenterToScreen()

            ' turn on the loaded flag
            'loaded = True

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    Private Sub OpenDeviceCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles openDeviceCheckBox.CheckedChanged
        If Me.openDeviceCheckBox.Checked Then
            ' open the daq board
            Me.Open()
            ' set the caption to close
            Me.openDeviceCheckBox.Text = "Cl&ose Device"
        Else
            ' close the daq board
            Me.CloseSignalIo()
            ' set the caption to open
            Me.openDeviceCheckBox.Text = "&Open Device"
        End If
    End Sub

    ''' <summary>Occurs upon timer events.</summary>
    ''' <remarks>Use this method to execute all timer actions.</remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub ActionTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles actionTimer.Tick

        Try
            Me._InputZero.ChannelNumber = Integer.Parse(Me.channelCheckBoxA.Text, Globalization.CultureInfo.CurrentCulture)
            Me._InputZero.Gain = CType(isr.io.UL.AnalogInput.Gains(Convert.ToInt16(Me.rangeCheckBoxA.SelectedIndex, Globalization.CultureInfo.CurrentCulture)), Integer)
            Me._InputZero.Acquire()
            Me.voltsTextBoxA.Text = Me._InputZero.Voltage.ToString("F4", Globalization.CultureInfo.CurrentCulture)

            Me._InputOne.ChannelNumber = Integer.Parse(Me.channelCheckBoxB.Text, Globalization.CultureInfo.CurrentCulture)
            Me._InputOne.Gain = CType(isr.io.UL.AnalogInput.Gains(Convert.ToInt16(Me.rangeCheckBoxB.SelectedIndex, Globalization.CultureInfo.CurrentCulture)), Integer)
            Me._InputOne.Acquire()
            Me.voltsTextBoxB.Text = Me._InputOne.Voltage.ToString("F4", Globalization.CultureInfo.CurrentCulture)

            Me._InputTwo.ChannelNumber = Integer.Parse(Me.channelCheckBoxC.Text, Globalization.CultureInfo.CurrentCulture)
            Me._InputTwo.Gain = CType(isr.io.UL.AnalogInput.Gains(Convert.ToInt16(Me.rangeCheckBoxC.SelectedIndex, Globalization.CultureInfo.CurrentCulture)), Integer)
            Me._InputTwo.Acquire()
            Me.voltsTextBoxC.Text = Me._InputTwo.Voltage.ToString("F4", Globalization.CultureInfo.CurrentCulture)

            Me._InputThree.ChannelNumber = Integer.Parse(Me.channelCheckBoxD.Text, Globalization.CultureInfo.CurrentCulture)
            Me._InputThree.Gain = CType(isr.io.UL.AnalogInput.Gains(Convert.ToInt16(Me.rangeCheckBoxD.SelectedIndex, Globalization.CultureInfo.CurrentCulture)), Integer)
            Me._InputThree.Acquire()
            Me.voltsTextBoxD.Text = Me._InputThree.Voltage.ToString("F4", Globalization.CultureInfo.CurrentCulture)

            If Me.ledCheckBox.Checked Then
                Me.ledCheckBox.Checked = False
                Me.messagesTextBox.Text = Me._DaqDevice.DoIdentify()
            End If

            If Me.portZeroInputCheckBox.Checked Xor Me._PortZero.IsInput Then
                Me._PortZero.IsInput = Me.portZeroInputCheckBox.Checked
                Me._PortZero.ConfigurePort()

                Me.portZeroSetCheckBox.Checked = False
                Me.portZeroSetCheckBox.Enabled = Not Me.portZeroInputCheckBox.Checked

                Me.portZeroReadCheckBox.Checked = False
                Me.portZeroReadCheckBox.Enabled = Me.portZeroInputCheckBox.Checked
            End If

            If Me.portZeroSetCheckBox.Checked Then
                Me._PortZero.PortValue = CShort(Me.portZeroNumericUpDown.Value)
            End If
            If Me.portZeroReadCheckBox.Checked Then
                Me.portZeroNumericUpDown.Value = Convert.ToDecimal(Me._PortZero.PortValue, Globalization.CultureInfo.CurrentCulture)
            End If

            If Me.portOneInputCheckBox.Checked Xor Me._PortOne.IsInput Then

                Me._PortOne.IsInput = Me.portOneInputCheckBox.Checked
                Me._PortOne.ConfigurePort()

                Me.portOneSetCheckBox.Checked = False
                Me.portOneSetCheckBox.Enabled = Not Me.portOneInputCheckBox.Checked

                Me.portOneReadCheckBox.Checked = False
                Me.portOneReadCheckBox.Enabled = Me.portOneInputCheckBox.Checked
                Me.portOneReadCheckBox.Refresh()

            End If

            If Me.portOneSetCheckBox.Checked Then
                Me._PortOne.PortValue = CShort(Me.portOneNumericUpDown.Value)
            End If
            If Me.portOneReadCheckBox.Checked Then
                Me.portOneNumericUpDown.Value = Convert.ToDecimal(Me._PortOne.PortValue, Globalization.CultureInfo.CurrentCulture)
            End If

        Catch ex As System.Exception
            Me.actionTimer.Enabled = False
            Me.messagesTextBox.Text = Me._DaqDevice.DeviceErrorMessage
            Me.openDeviceCheckBox.Checked = False
        End Try

    End Sub

#End Region

End Class
