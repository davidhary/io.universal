﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        Public Const AssemblyTitle As String = "Universal Library Tests"
        Public Const AssemblyDescription As String = "Unit Tests for the Universal Library"
        Public Const AssemblyProduct As String = "isr.io.UL.Library.Tests"

    End Class

End Namespace
