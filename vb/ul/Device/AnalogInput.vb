Imports System.Collections
''' <summary>Defines an analog input channel.</summary>
''' <remarks> (c) 2003 Integrated Scientific Resources, Inc.
''' Licensed under The MIT License.</para><para>
''' David, 08/07/03, 1.0.1314. Created </para></remarks>
Public Class AnalogInput

    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="instanceName">Specifies the name of the instance.</param>
    ''' <remarks>Use this constructor to instantiate this class
    '''   and set the instance name, which is useful in tracing</remarks>
    Public Sub New(
        ByVal instanceName As String)

        ' instantiate the base class
        MyBase.New()
        Me._InstanceName = instanceName

    End Sub

    ''' <summary>Constructs this class.</summary>
    ''' <param name="instanceName">Specifies the name of the instance.</param>
    ''' <param name="deviceLink">
    '''   is an Object expression that specifies the signal link driver for accessing
    '''   the device.</param>
    ''' <remarks>Use this constructor to instantiate this class
    '''   and set the instance name, which is useful in tracing</remarks>
    Public Sub New(
        ByVal instanceName As String,
        ByVal deviceLink As Device)

        Me.New(instanceName)

        Me._AnalogDevice = deviceLink

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>Gets or sets the dispose status sentinel.</summary>
    Private _Disposed As Boolean
    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me._Disposed Then

            If disposing Then

                ' Free managed resources when explicitly called
                Me.StatusMessage = String.Empty
                Me._InstanceName = String.Empty

                If Me._AnalogDevice IsNot Nothing Then
                    Me._AnalogDevice.Dispose()
                End If

            End If

            ' Free shared unmanaged resources

        End If

        ' set the sentinel indicating that the class was disposed.
        Me._Disposed = True
    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>Overrides ToString returning the instance name if not empty.</summary>
    ''' <remarks>Use this method to return the instance name. If instance name is not set, 
    '''   returns the base class ToString value.</remarks>
    Public Overrides Function ToString() As String
        Return If(String.IsNullOrEmpty(Me._InstanceName), MyBase.ToString, Me._InstanceName)
    End Function

    Private _InstanceName As String = String.Empty
    ''' <summary>Gets or sets the name given to an instance of this class.</summary>
    ''' <value><c>InstanceName</c> is a String property.</value>
    ''' <remarks> David, 11/23/04, 1.0.1788.
    '''   Correct code no to get instance name from .ToString but from MyBase.ToString
    '''   so that calling this method from the child class will not break the rule of
    '''   calling overridable methods from the constructor.
    ''' </para></remarks>
    Public Property InstanceName() As String
        Get
            Return If(Not String.IsNullOrEmpty(Me._InstanceName), Me._InstanceName, MyBase.ToString)
        End Get
        Set(ByVal value As String)
            Me._InstanceName = value
        End Set
    End Property

    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A <see cref="String">String</see>.</value>
    Public Property StatusMessage() As String = String.Empty

#End Region

#Region " METHODS "

    ''' <summary>Configures the analog input.</summary>
    ''' <exception cref="isr.io.UL.OperationException" guarantee="strong"></exception>
    Public Sub ConfigureInput()

        If Me._AnalogDevice.IsDemo Then
            Me._AnalogDevice.DeviceErrorInfo = New MccDaq.ErrorInfo
        Else
            ' configure the port.
            '      _analogDevice.DeviceErrorInfo = _
            '        _analogDevice.DaqDevice.DConfigPort(_portType.FirstPortA, _portDirection)
        End If

        If Me._AnalogDevice.DeviceErrorInfo.Value = MccDaq.ErrorInfo.ErrorCode.NoErrors Then
        Else
            ' throw an exception
            Me.StatusMessage = $"{Me.InstanceName} failed to configure analog input"
            Throw New isr.io.UL.IOException(Me.StatusMessage, Me._AnalogDevice.DeviceErrorInfo.Value, Me._AnalogDevice.DeviceErrorInfo.Message)
        End If

    End Sub

    ''' <summary>Increments the analog input gain.</summary>
    ''' <remarks>Use this method to increment the analog input gains.</remarks>
    Public Function IncrementGain() As Single
        ' select a new gain
        Select Case Me._Gain
            Case Is >= 20
                Me._Gain = 20
            Case Is >= 16
                Me._Gain = 20
            Case Is >= 10
                Me._Gain = 16
            Case Is >= 8
                Me._Gain = 10
            Case Is >= 5
                Me._Gain = 8
            Case Is >= 4
                Me._Gain = 8
            Case Is >= 2
                Me._Gain = 4
            Case Is >= 1
                Me._Gain = 2
            Case Else
                Me._Gain = 1
        End Select
        ' set the new gain
        Me.Gain = Me._Gain
        Return Me.Gain
    End Function

    ''' <summary>Decrements the analog input gain.</summary>
    ''' <remarks>Use this method to decrement the analog input gains.</remarks>
    Public Function DecrementGain() As Single
        ' select a new gain
        Select Case Me._Gain
            Case Is >= 20
                Me._Gain = 16
            Case Is >= 16
                Me._Gain = 10
            Case Is >= 10
                Me._Gain = 8
            Case Is >= 8
                Me._Gain = 5
            Case Is >= 5
                Me._Gain = 4
            Case Is >= 4
                Me._Gain = 2
            Case Is >= 2
                Me._Gain = 1
            Case Is >= 1
                Me._Gain = 1
            Case Else
                Me._Gain = 1
        End Select
        ' set the new gain
        Me.Gain = Me._Gain
        Return Me.Gain
    End Function

    Private ReadOnly _RandomAnalogInput As New Random
    ''' <summary>Samples a single channel at a specific gain.</summary>
    ''' <exception cref="isr.io.UL.IOException" guarantee="strong"></exception>
    ''' <remarks>Use this method to get a single sample from the input channel.</remarks>
    Public Sub Acquire()

        If Me._AnalogDevice.IsDemo Then
            Me._Voltage = 0.9F * Me._Voltage +
        0.01F * Convert.ToSingle(Me._RandomAnalogInput.NextDouble - 0.5) * Me.AnalogInputRange.Max
            Me._AnalogDevice.DeviceErrorInfo = New MccDaq.ErrorInfo
        Else
            ' save previous data value for overflow test
            Me._DataValuePrev = Me._DataValue
            ' read an analog input
            Me._AnalogDevice.DeviceErrorInfo = Me._AnalogDevice.DaqDevice.AIn(Me.ChannelNumber, Me._RangeCode, Me._DataValue)
        End If

        If Me._AnalogDevice.DeviceErrorInfo.Value = MccDaq.ErrorInfo.ErrorCode.NoErrors Then
            ' if no error convert
            Me._AnalogDevice.DeviceErrorInfo = Me._AnalogDevice.DaqDevice.ToEngUnits(Me._RangeCode, Me._DataValue, Me._Voltage)
        Else
            ' throw an exception
            Me.StatusMessage = $"{Me.InstanceName} failed getting analog input"
            Throw New isr.io.UL.IOException(Me.StatusMessage,
          Me._AnalogDevice.DeviceErrorInfo.Value, Me._AnalogDevice.DeviceErrorInfo.Message)
        End If

    End Sub

    ''' <summary>Sets the offset voltage correction to the current voltage.</summary>
    ''' <remarks>Use this method to set the internal voltage offset value used to correct
    '''   the analog input voltage for offset.</remarks>
    Public Sub UpdateVoltageOffset()

        Me._VoltageOffset = Me._Voltage

    End Sub

    ''' <summary>Adjust the gain to fit the voltage within the device range.</summary>
    ''' <returns>A Boolean data type</returns>
    ''' <param name="Voltage">
    '''   is a Single expression that specifies the input voltage to check for range.</param>
    Public Function AdjustRange(ByVal voltage As Single) As Boolean

        Dim rangeChanged As Boolean = False
        ' set gain to maximum
        Me.Gain = Me._GainMax
        Do Until Me.InRangeInternal(voltage) Or (Me.Gain = Me._GainMin)
            ' if not in range, 
            Me.DecrementGain()
            rangeChanged = True
        Loop
        Return rangeChanged

    End Function

    ''' <summary>updates the range to fit within the optimal ranges.</summary>
    ''' <returns>A Boolean data type</returns>
    ''' <param name="Voltage">
    '''   is a Single expression that specifies the input voltage to check for range.</param>
    ''' <remarks>Call this method to adjust the analog input gain to fit the specified 
    '''   voltage within the optimal gain.</remarks>
    Public Function AdjustOptimalRange(ByVal voltage As Single) As Boolean

        Dim rangeChanged As Boolean = False
        Do Until Me.InOptimalRangeInternal(voltage)

            If Me.InHigherGainRangeInternal(voltage) Then
                ' if in higher gain, increment the gain
                Me.IncrementGain()
                rangeChanged = True
            ElseIf Me.InLowerGainRangeInternal(voltage) Then
                ' if in lower gain range, decrement the gain
                Me.DecrementGain()
                rangeChanged = True
            End If

        Loop
        Return rangeChanged

    End Function

#End Region

#Region " SHARED PROPERTIES "

    ''' <summary>Gets or sets the list Analog Input Channels.</summary>
    ''' <value><c>Channels</c> is an ArrayList property.</value>
    Public Shared ReadOnly Property Channels() As ArrayList
        Get
            Dim channelList As New ArrayList
            channelList.AddRange(New Object() {0, 1, 2, 3})
            Return channelList
        End Get
    End Property

    ''' <summary>Lists the Analog Input Gains.</summary>
    ''' <value><c>Gains</c> is an ArrayList property.</value>
    Public Shared ReadOnly Property Gains() As ArrayList
        Get
            Dim gainList As New ArrayList
            gainList.AddRange(New Object() {1, 2, 4, 5, 8, 10, 16, 20})
            Return gainList
        End Get
    End Property

    ''' <summary>Lists the Analog Input Ranges.</summary>
    ''' <value><c>Ranges</c> is an ArrayList property.</value>
    Public Shared ReadOnly Property Ranges() As ArrayList
        Get
            Dim rangeList As New ArrayList
            rangeList.AddRange(New Object() {"±20", "±10", "±5", "±4", "±2.5", "±2", "±1.25", "±1"})
            Return rangeList
        End Get
    End Property

#End Region

#Region " PROPERTIES "

    Private _RelativeGainRange As New SignalRange(-9.5, 9.5)
    Private _HigherGainRange As New SignalRange(-2.5, 2.5)

    Private ReadOnly _AnalogDevice As Device
    ''' <summary>Gets or sets the analog input channel code.</summary>
    ''' <value><c>ChannelNumber</c> is an Int32 property.</value>
    ''' <remarks>Use this property to get or set the analog input channel.</remarks>
    Public Property ChannelNumber() As Integer

    Private ReadOnly _GainMax As Integer = 20
    Private ReadOnly _GainMin As Integer = 1
    Private _Gain As Integer = 1
    Private _GainNextHigher As Integer = 2
    Private _RangeCode As MccDaq.Range = MccDaq.Range.Bip10Volts
    ''' <summary>Gets or sets the analog input gain.</summary>
    ''' <value><c>Gain</c> is an Int32 property.</value>
    ''' <remarks>Use this property to get or set the analog input gain which to use for single 
    '''   analog inputs when calling the Read method.  This property also sets the
    '''   analog input gain code (GainCode).</remarks>
    Public Property Gain() As Integer
        Get
            Return Me._Gain
        End Get
        Set(ByVal value As Integer)
            ' set the gain code based on the gain value
            Select Case value
                Case Is >= 20
                    Me._RangeCode = MccDaq.Range.Bip1Volts
                    Me._Gain = 20
                    Me._GainNextHigher = 20
                Case Is >= 16
                    Me._RangeCode = MccDaq.Range.Bip1Pt25Volts
                    Me._Gain = 16
                    Me._GainNextHigher = 20
                Case Is >= 10
                    Me._RangeCode = MccDaq.Range.Bip2Volts
                    Me._Gain = 10
                    Me._GainNextHigher = 16
                Case Is >= 8
                    Me._RangeCode = MccDaq.Range.Bip2Pt5Volts
                    Me._Gain = 8
                    Me._GainNextHigher = 10
                Case Is >= 5
                    Me._RangeCode = MccDaq.Range.Bip4Volts
                    Me._Gain = 5
                    Me._GainNextHigher = 8
                Case Is >= 4
                    Me._RangeCode = MccDaq.Range.Bip5Volts
                    Me._Gain = 4
                    Me._GainNextHigher = 8
                Case Is >= 2
                    Me._RangeCode = MccDaq.Range.Bip10Volts
                    Me._Gain = 2
                    Me._GainNextHigher = 4
                Case Is >= 1
                    Me._RangeCode = MccDaq.Range.Bip20Volts
                    Me._Gain = 1
                    Me._GainNextHigher = 2
                Case Else
                    Me._RangeCode = MccDaq.Range.Bip20Volts
                    Me._Gain = 1
                    Me._GainNextHigher = 2
            End Select
            ' set the analog input range based on the new range code
            Me.SetRangesInternal()
        End Set
    End Property

    Private ReadOnly _DataValueMin As Integer
    Private ReadOnly _DataValueMax As Integer = 4095
    Private _DataValuePrev As Integer = 1
    Private _DataValue As Short
    ''' <summary>Gets or sets the analog input data value.</summary>
    ''' <value><c>DataValue</c> is a Unsigned 16 Bit Int32 property that can be read from (read only).</value>
    ''' <remarks>Use this property to get the analog input data value read using GetAnalogInput.</remarks>
    Public ReadOnly Property DataValue() As Integer
        Get
            Return Me._DataValue
        End Get
    End Property

    Private _Voltage As Single
    ''' <summary>Gets or sets the analog input voltage.</summary>
    ''' <value><c>Voltage</c> is a Single-Precision property that can be read from (read only).</value>
    ''' <remarks>Use this property to get analog input voltage read using GetAnalogInput.</remarks>
    Public ReadOnly Property Voltage() As Single
        Get
            Return Me._Voltage
        End Get
    End Property

    Private _VoltageOffset As Single
    ''' <summary>Gets or sets the analog input voltage corrected for offset.</summary>
    ''' <value><c>VoltageCorrected</c> is a Single-Precision property that can be read from (read only).</value>
    ''' <remarks>Use this property to get analog input voltage read using Read 
    '''   and corrected for offset as set in UpdateVoltageOffset.</remarks>
    Public ReadOnly Property VoltageCorrected() As Single
        Get
            Return If(Me.IsInvert, Me._VoltageOffset - Me._Voltage, Me._Voltage - Me._VoltageOffset)
        End Get
    End Property

    Private _AnalogInputRange As New SignalRange(-10, 10)
    ''' <summary>Gets or sets the analog input range for the prescribed gain.</summary>
    ''' <value><c>AnalogInputRange</c> is a SignalRange property that can be read from (read only).</value>
    ''' <remarks>Use this property to get the maximum, minimum, or range values of the
    '''   analog input voltage for the range settings for the specified channel and gain.</remarks>
    Public ReadOnly Property AnalogInputRange() As SignalRange
        Get
            Return Me._AnalogInputRange
        End Get
    End Property
    ''' <summary>Gets or sets the analog input relative range.</summary>
    ''' <value><c>AnalogInputAutoRangeLimen</c> is a Single property.</value>
    ''' <remarks>Use this property to get or set the range within the analog input 
    '''   range outside of which auto ranging kicks in.</remarks>
    Public Property AnalogInputAutoRangeLimen() As Single = 0.95
    ''' <summary>Gets or sets the invert status of the analog input.</summary>
    ''' <value><c>IsInvert</c> is a Boolean property.</value>
    ''' <remarks>Use this property to turn on or off inversion of the input voltage when calculating the corrected
    '''   input voltage</remarks>
    Public Property IsInvert() As Boolean

    ''' <summary>Gets or sets the over voltage outcome value.</summary>
    ''' <value><c>IsOverVoltage</c> is a <see cref="Boolean"/> property that can be read from (read only).</value>
    ''' <remarks>Use this property to get over voltage outcome of the last reading.</remarks>
    ''' <remarks> David, 05/30/03, 1.0.1245. Adapt for PMD-1208. </para></remarks>
    ''' <remarks> David, 05/30/03, 1.0.1123. Use range limits </para></remarks>
    Public ReadOnly Property IsOverVoltage() As Boolean
        Get
            Return (Me._Voltage >= Me._AnalogInputRange.High) Or (Me._Voltage <= Me._AnalogInputRange.Low)
        End Get
    End Property

    ''' <summary>Gets or sets the overflow outcome value.</summary>
    ''' <value><c>Overflowed</c> is a <see cref="Boolean"/> property that can be read from (read only).</value>
    ''' <remarks>Use this property to test if the last two data values hit the A/D range limits.</remarks>
    Public ReadOnly Property Overflowed() As Boolean
        Get
            Return Me.DataValueOverflowedInternal(Me._DataValue) And Me.DataValueOverflowedInternal(Me._DataValuePrev)
        End Get
    End Property
    ''' <summary>Gets or sets the single-ended attribute of the input channel.</summary>
    ''' <value><c>SingleEnded</c> is a <see cref="Boolean"/> property that can be read from (read only).</value>
    ''' <remarks>Use this property to determine if the specified input channel is single ended.</remarks>
    Public ReadOnly Property SingleEnded() As Boolean

#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary>Sets the analog input ranges.</summary>
    ''' <exception cref="isr.io.UL.OperationException" guarantee="strong">
    '''   Failed to set ranges.</exception>
    ''' <remarks>Call this method from the class properties setting gain or polarity
    '''   to set the ranges for such values.</remarks>
    Private Function SetRangesInternal() As Boolean

        Try

            Me._AnalogInputRange = If(Me.SingleEnded, New SignalRange(20, 1, True), New SignalRange(40, Me._Gain, True))

            ' these settings are needed for auto range
            ' if unipolar, set ranges for in auto range, set the high and low gain ranges.
            Me._RelativeGainRange.Max = Me._AnalogInputRange.Max - 0.5F * (1.0F - Me.AnalogInputAutoRangeLimen) * Me._AnalogInputRange.Range
            Me._RelativeGainRange.Min = Me._AnalogInputRange.Min + 0.5F * (1.0F - Me.AnalogInputAutoRangeLimen) * Me._AnalogInputRange.Range

            ' set the relative range for higher gain making sure that upon using the 
            ' higher gain, the signal will be within the AnalogInputAutoRangeLimen
            Dim higherGainAutoRangeLimen As Single = (Me.AnalogInputAutoRangeLimen - 0.01F) * Me._Gain / Me._GainNextHigher
            Me._HigherGainRange.Max = Me._AnalogInputRange.Midrange + 0.5F * higherGainAutoRangeLimen * Me._AnalogInputRange.Range
            Me._HigherGainRange.Min = Me._AnalogInputRange.Midrange - 0.5F * higherGainAutoRangeLimen * Me._AnalogInputRange.Range
            Return True
        Catch exn As System.Exception

            ' add the message to the exception
            Me.StatusMessage = $"{Me.InstanceName} failed to set ranges"
            Throw New isr.io.UL.OperationException(Me.StatusMessage, exn)

        End Try

    End Function

    Private Function DataValueOverflowedInternal(ByVal dataValue As Integer) As Boolean
        Return (dataValue <= Me._DataValueMin) Or (dataValue >= Me._DataValueMax)
    End Function

    ''' <summary>Check the analog input ranges.</summary>
    ''' <param name="voltage">
    '''   is a Single expression that specifies the input voltage to check for range.</param>
    ''' <remarks>Call this method to check if the signal is in full range.</remarks>
    Private Function InRangeInternal(ByVal voltage As Single) As Boolean
        Return ((voltage <= Me._AnalogInputRange.Max) And (voltage >= Me._AnalogInputRange.Min))
    End Function

    ''' <summary>Checks the analog input ranges.</summary>
    ''' <returns><c>isAinOptimalRange</c> returns true if the input signal is in optional range.</returns>
    ''' <param name="voltage">
    '''   is a Single expression that specifies the input voltage to check for range.</param>
    ''' <remarks>Call this method to check if the signal is in optimal range. The 
    '''   signal is in optimal range if it is within the analog input relative 
    '''   range and outside the range of higher gain.</remarks>
    Private Function InOptimalRangeInternal(ByVal voltage As Single) As Boolean
        Return Not (Me.InHigherGainRangeInternal(voltage) Or Me.InLowerGainRangeInternal(voltage))
    End Function

    Private Function InHigherGainRangeInternal(ByVal voltage As Single) As Boolean
        Return ((voltage < Me._HigherGainRange.Max) And (voltage > Me._HigherGainRange.Min)) And (Not Me.InMaxAnalogInputGainRangeInternal)
    End Function

    Private Function InMaxAnalogInputGainRangeInternal() As Boolean
        Return (Me._Gain >= Me._GainMax)
    End Function

    Private Function InLowerGainRangeInternal(ByVal voltage As Single) As Boolean
        Return ((voltage > Me._RelativeGainRange.Max) Or (voltage < Me._RelativeGainRange.Min)) And (Not Me.InMinAnalogInputGainRangeInternal)
    End Function

    Private Function InMinAnalogInputGainRangeInternal() As Boolean
        Return (Me._Gain <= Me._GainMin)
    End Function

#End Region

End Class

