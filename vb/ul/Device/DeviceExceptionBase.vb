''' <summary> A device exception base. </summary>
<Serializable>
Public MustInherit Class DeviceExceptionBase
    Inherits ExceptionBase

    Private Const _DefMessage As String = "General Universal Library failure."
    Private Const _MessageFormat As String = "{0} The device reported error number {1}:: {2}"

#Region " CUSTOM CONSTRUCTORS "

    ''' <summary> Constructor specifying message and data to be set. </summary>
    ''' <param name="errorMessage">       Specifies the exception message. </param>
    ''' <param name="deviceErrorCode">    Specifies the error code from the device. </param>
    ''' <param name="deviceErrorMessage"> Specifies the error message from the device. </param>
    Protected Sub New(ByVal errorMessage As String, ByVal deviceErrorCode As Long, ByVal deviceErrorMessage As String)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, _MessageFormat, errorMessage, deviceErrorCode, deviceErrorMessage))
        Me._DeviceErrorCode = deviceErrorCode
        Me._DeviceErrorMessage = deviceErrorMessage
    End Sub

    ''' <summary> Constructor specifying message format, message, and data to be set. </summary>
    ''' <param name="messageFormat">      Specifies the message format. </param>
    ''' <param name="errorMessage">       Specifies the exception message. </param>
    ''' <param name="deviceErrorCode">    Specifies the error code from the device. </param>
    ''' <param name="deviceErrorMessage"> Specifies the error message from the device. </param>
    Protected Sub New(ByVal messageFormat As String, ByVal errorMessage As String, ByVal deviceErrorCode As Long, ByVal deviceErrorMessage As String)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, messageFormat, errorMessage, deviceErrorCode, deviceErrorMessage))
        Me._DeviceErrorCode = deviceErrorCode
        Me._DeviceErrorMessage = deviceErrorMessage
    End Sub

    ''' <summary>Constructor with no parameters.</summary>
    Protected Sub New()
        MyBase.New(_DefMessage)
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    Protected Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <para
    ''' m name="innerException">Sets a reference to the InnerException.</param>
    Protected Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo,
      ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets the device error code. </summary>
    ''' <value> The device error code. </value>
    Public ReadOnly Property DeviceErrorCode() As Long

    ''' <summary> Gets a message describing the device error. </summary>
    ''' <value> A message describing the device error. </value>
    Public ReadOnly Property DeviceErrorMessage() As String

#End Region


End Class

