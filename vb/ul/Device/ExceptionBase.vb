''' <summary> An inheritable exception for use by framework classes and applications. </summary>
''' <remarks>
''' Inherits from System.Exception per design rule CA1958 which specifies that "Types do not
''' extend inheritance vulnerable types" and further explains that "This [Application Exception]
''' base exception type does not provide any additional value for framework classes.  <para>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para><para>  
''' David, 01/21/2014, x.x.5134 Based on legacy base exception. </para><para>  
''' David, 01/15/2005, 1.0.1841. </para>
''' </remarks>
<Serializable()>
Public MustInherit Class ExceptionBase
    Inherits System.Exception

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="ExceptionBase" /> class. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Protected Sub New()
        MyBase.New()
        Me.ObtainEnvironmentInformation()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="ExceptionBase" /> class. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="message"> The message. </param>
    Protected Sub New(ByVal message As String)
        MyBase.New(message)
        Me.ObtainEnvironmentInformation()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="ExceptionBase" /> class. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="message">        The message. </param>
    ''' <param name="innerException"> The inner exception. </param>
    Protected Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
        Me.ObtainEnvironmentInformation()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="ExceptionBase" /> class. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="format"> The format. </param>
    ''' <param name="args">   The arguments. </param>
    Protected Sub New(ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        Me.ObtainEnvironmentInformation()
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="ExceptionBase" /> class. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="innerException"> Specifies the InnerException. </param>
    ''' <param name="format">         Specifies the exception formatting. </param>
    ''' <param name="args">           Specifies the message arguments. </param>
    Protected Sub New(ByVal innerException As System.Exception, ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, format, args), innerException)
    End Sub

#End Region

#Region " SERIALIZATION "

    ''' <summary> Initializes a new instance of the class with serialized data. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
    '''                        that holds the serialized object data about the exception being
    '''                        thrown. </param>
    ''' <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
    '''                        that contains contextual information about the source or destination. 
    ''' </param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
        If info Is Nothing Then
            Return
        End If
        Me._MachineName = info.GetString(NameOf(ExceptionBase.MachineName))
        Me._CreatedDateTime = info.GetDateTime(NameOf(ExceptionBase.CreatedDateTime))
        Me._AppDomainName = info.GetString(NameOf(ExceptionBase.AppDomainName))
        Me._ThreadIdentityName = info.GetString(NameOf(ExceptionBase.ThreadIdentityName))
        Me._WindowsIdentityName = info.GetString(NameOf(ExceptionBase.WindowsIdentityName))
        Me._OSVersion = info.GetString(NameOf(ExceptionBase.OSVersion))
        Me._AdditionalInformation = CType(info.GetValue(NameOf(ExceptionBase.AdditionalInformation),
                                                        GetType(System.Collections.Specialized.NameValueCollection)),
                                                    System.Collections.Specialized.NameValueCollection)
    End Sub

    ''' <summary>
    ''' Overrides the <see cref="GetObjectData" /> method to serialize custom values.
    ''' </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    ''' <param name="info">    The <see cref="Runtime.Serialization.SerializationInfo">serialization
    '''                        information</see>. </param>
    ''' <param name="context"> The <see cref="Runtime.Serialization.StreamingContext">streaming
    '''                        context</see> for the exception. </param>
    <Security.Permissions.SecurityPermission(Security.Permissions.SecurityAction.Demand, SerializationFormatter:=True)>
    Public Overrides Sub GetObjectData(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)

        If info Is Nothing Then
            Return
        End If
        info.AddValue(NameOf(ExceptionBase.MachineName), Me.MachineName, GetType(String))
        info.AddValue(NameOf(ExceptionBase.CreatedDateTime), Me.CreatedDateTime)
        info.AddValue(NameOf(ExceptionBase.AppDomainName), Me.AppDomainName, GetType(String))
        info.AddValue(NameOf(ExceptionBase.ThreadIdentityName), Me.ThreadIdentityName, GetType(String))
        info.AddValue(NameOf(ExceptionBase.WindowsIdentityName), Me.WindowsIdentityName, GetType(String))
        info.AddValue(NameOf(ExceptionBase.OSVersion), Me.OSVersion, GetType(String))
        info.AddValue(NameOf(ExceptionBase.AdditionalInformation), Me.AdditionalInformation, GetType(System.Collections.Specialized.NameValueCollection))
        MyBase.GetObjectData(info, context)
    End Sub

#End Region

#Region " ADDITIONAL INFORMATION "

    ''' <summary> Information describing the additional. </summary>
    Private _AdditionalInformation As System.Collections.Specialized.NameValueCollection

    ''' <summary> Collection allowing additional information to be added to the exception. </summary>
    ''' <value> The additional information. </value>
    Public ReadOnly Property AdditionalInformation() As System.Collections.Specialized.NameValueCollection
        Get
            If Me._AdditionalInformation Is Nothing Then
                Me._AdditionalInformation = New System.Collections.Specialized.NameValueCollection
            End If
            Return Me._AdditionalInformation
        End Get
    End Property

    ''' <summary> Name of the application domain. </summary>
    Private _AppDomainName As String

    ''' <summary> AppDomain name where the Exception occurred. </summary>
    ''' <value> The name of the application domain. </value>
    Public ReadOnly Property AppDomainName() As String
        Get
            Return Me._AppDomainName
        End Get
    End Property

    ''' <summary> The created date time. </summary>
    Private _CreatedDateTime As DateTimeOffset = DateTimeOffset.Now

    ''' <summary> Date and Time <see cref="DateTimeOffset"/> the exception was created. </summary>
    ''' <value> The created date time. </value>
    Public ReadOnly Property CreatedDateTime() As DateTimeOffset
        Get
            Return Me._CreatedDateTime
        End Get
    End Property

    ''' <summary> Name of the machine. </summary>
    Private _MachineName As String

    ''' <summary> Machine name where the Exception occurred. </summary>
    ''' <value> The name of the machine. </value>
    Public ReadOnly Property MachineName() As String
        Get
            Return Me._MachineName
        End Get
    End Property

    ''' <summary> The operating system version. </summary>
    Private _OSVersion As String

    ''' <summary> Gets the OS version. </summary>
    ''' <value> The OS version. </value>
    Public ReadOnly Property OSVersion() As String
        Get
            Return Me._OSVersion
        End Get
    End Property

    ''' <summary> Name of the thread identity. </summary>
    Private _ThreadIdentityName As String

    ''' <summary> Identity of the executing thread on which the exception was created. </summary>
    ''' <value> The name of the thread identity. </value>
    Public ReadOnly Property ThreadIdentityName() As String
        Get
            Return Me._ThreadIdentityName
        End Get
    End Property

    ''' <summary> Name of the windows identity. </summary>
    Private _WindowsIdentityName As String

    ''' <summary> Windows identity under which the code was running. </summary>
    ''' <value> The name of the windows identity. </value>
    Public ReadOnly Property WindowsIdentityName() As String
        Get
            Return Me._WindowsIdentityName
        End Get
    End Property

    ''' <summary> Gathers environment information safely. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ObtainEnvironmentInformation()

        Const unknown As String = "N/A"
        Me._CreatedDateTime = DateTimeOffset.Now

        Me._MachineName = Environment.MachineName
        If Me._MachineName.Length = 0 Then
            Me._MachineName = unknown
        End If

        Me._ThreadIdentityName = Threading.Thread.CurrentPrincipal.Identity.Name
        If Me._ThreadIdentityName.Length = 0 Then
            Me._ThreadIdentityName = unknown
        End If

        Me._WindowsIdentityName = Security.Principal.WindowsIdentity.GetCurrent().Name
        If Me._WindowsIdentityName.Length = 0 Then
            Me._WindowsIdentityName = unknown
        End If

        Me._AppDomainName = AppDomain.CurrentDomain.FriendlyName
        If Me._AppDomainName.Length = 0 Then
            Me._AppDomainName = unknown
        End If

        Me._OSVersion = Environment.OSVersion.ToString
        If Me._OSVersion.Length = 0 Then
            Me._OSVersion = unknown
        End If

        Me._AdditionalInformation = New System.Collections.Specialized.NameValueCollection From {
            {AdditionalInfoItem.MachineName.ToString, My.Computer.Name},
            {AdditionalInfoItem.Timestamp.ToString, DateTimeOffset.Now.ToString(Globalization.CultureInfo.CurrentCulture)},
            {AdditionalInfoItem.FullName.ToString, Reflection.Assembly.GetExecutingAssembly().FullName},
            {AdditionalInfoItem.AppDomainName.ToString, AppDomain.CurrentDomain.FriendlyName},
            {AdditionalInfoItem.ThreadIdentity.ToString, My.User.Name} ' Thread.CurrentPrincipal.Identity.Name
        }

        Try
            ' WMI may not be installed on the computer
            Me._AdditionalInformation.Add(AdditionalInfoItem.WindowsIdentity.ToString, My.Computer.Info.OSFullName)
            Me._AdditionalInformation.Add(AdditionalInfoItem.OSVersion.ToString, My.Computer.Info.OSVersion)
        Catch
        End Try

    End Sub

#End Region

    ''' <summary> Specifies the contents of the additional information. </summary>
    ''' <remarks> David, 2020-09-15. </remarks>
    Private Enum AdditionalInfoItem
        ''' <summary>
        ''' The none
        ''' </summary>
        None
        ''' <summary>
        ''' The machine name
        ''' </summary>
        MachineName
        ''' <summary>
        ''' The timestamp
        ''' </summary>
        Timestamp
        ''' <summary>
        ''' The full name
        ''' </summary>
        FullName
        ''' <summary>
        ''' The application domain name
        ''' </summary>
        AppDomainName
        ''' <summary>
        ''' The thread identity
        ''' </summary>
        ThreadIdentity
        ''' <summary>
        ''' The windows identity
        ''' </summary>
        WindowsIdentity
        ''' <summary>
        ''' The OS version
        ''' </summary>
        OSVersion
    End Enum

End Class
