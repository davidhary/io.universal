Namespace ExceptionExtensions

    ''' <summary> Adds exception data for building the exception full blown report. </summary>
    ''' <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary> Adds exception data from the specified exception. </summary>
        ''' <param name="value">     The value. </param>
        ''' <param name="exception"> The exception. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Private Function AddExceptionData(ByVal value As System.Exception, ByVal exception As DeviceExceptionBase) As Boolean
            If exception IsNot Nothing Then
                Dim count As Integer = value.Data.Count
                value.Data.Add($"{count}-DeviceErrorCode", exception.DeviceErrorCode)
                value.Data.Add($"{count}-DeviceMessage", exception.DeviceErrorCode)
            End If
            Return exception IsNot Nothing
        End Function

        ''' <summary> Adds an exception data to 'Exception'. </summary>
        ''' <remarks> David, 2020-09-12. </remarks>
        ''' <param name="exception"> The exception. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Public Function AddExceptionData(ByVal exception As System.Exception) As Boolean
            Dim affirmative As Boolean = False
            affirmative = affirmative OrElse Methods.AddExceptionData(exception, TryCast(exception, DeviceExceptionBase))
            affirmative = affirmative OrElse Methods.AddExceptionData(exception, TryCast(exception, ArgumentOutOfRangeException))
            affirmative = affirmative OrElse Methods.AddExceptionData(exception, TryCast(exception, ArgumentException))
            affirmative = affirmative OrElse Methods.AddExceptionData(exception, TryCast(exception, Runtime.InteropServices.ExternalException))
            Return affirmative
        End Function

        ''' <summary> Adds an exception data. </summary>
        ''' <param name="exception"> The exception. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Private Function AddExceptionDataThis(ByVal exception As System.Exception) As Boolean
            Return Methods.AddExceptionData(exception)
        End Function

        ''' <summary> Converts a value to a full blown string. </summary>
        ''' <param name="value"> The value. </param>
        ''' <returns> Value as a String. </returns>
        <System.Runtime.CompilerServices.Extension>
        Friend Function ToFullBlownString(ByVal value As System.Exception) As String
            Return Methods.ToFullBlownString(value, Integer.MaxValue)
        End Function

        ''' <summary> Converts this object to a full blown string. </summary>
        ''' <param name="value"> The value. </param>
        ''' <param name="level"> The level. </param>
        ''' <returns> The given data converted to a String. </returns>
        <System.Runtime.CompilerServices.Extension>
        Friend Function ToFullBlownString(ByVal value As System.Exception, ByVal level As Integer) As String
            Return Methods.ToFullBlownString(value, level, AddressOf AddExceptionDataThis)
        End Function

    End Module

End Namespace

